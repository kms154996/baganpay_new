<?php

return [

    /**
     * @Wave
     * @client_id
     * @client_secret
     * @hash_code
     */
     'wave' => [
        'callback_url' => env('APP_URL') . 'api/wave/callback',
//        'callback_url' => 'http://pay.baganit.com/api/wave/callback',
//        'callback_url' => 'http://api.mintheinkha.com/wavepay/redirect',
        'url' => 'https://api.wavemoney.io:8100/wmt-mfs-merchant-exp/pay-with-wave',
        'mtk' => [
            'client_id' => '3426eb197dcd488d8cc92eb84b124e4d',
            'client_secret' => 'A0d1A8Ce3B3E477cb8C22395E42bF739',
            'hash_code' => '910f82f145a16e7b533573146d5fdefdb0d4d0ee',

        ],
        'wunzinn' => [
            'client_id' => '3426eb197dcd488d8cc92eb84b124e4d',
            'client_secret' => 'A0d1A8Ce3B3E477cb8C22395E42bF739',
            'hash_code' => '910f82f145a16e7b533573146d5fdefdb0d4d0ee',
        ],
        'series' => [
            'client_id' => '3426eb197dcd488d8cc92eb84b124e4d',
            'client_secret' => 'A0d1A8Ce3B3E477cb8C22395E42bF739',
            'hash_code' => '910f82f145a16e7b533573146d5fdefdb0d4d0ee',

        ],
    ],

    /**
     * @KBZ
     * @app_id
     * @merch_code
     * @key
     */
    'kbz' => [
        'static_url' => 'https://wap.kbzpay.com/pgw/pwa/#/?',
        'precreate__url' => 'https://api.kbzpay.com/payment/gateway/precreate',
        'pre_create_method' => 'kbz.payment.precreate',
        'sign_type' => 'SHA256',
        'trade_type_web' => 'PWAAPP',
        'trade_type_app' => 'APP',
        'trade_type_h5' => 'APPH5',
        'trans_currency' => 'MMK',
        'timeout_express' => '10m',
        'notify_url' => env('APP_URL') . 'api/kbz/callback',
        'refer_url' => env('APP_URL') . 'pay/confirm',

        'mtk' => [
            'app_id' => 'kpae0c44ac8da14a3f980bc0ee363da8',
            'merch_code' => '200047',
            'key' => 'a5446c8bdaa7cde1f5b70939e725a625',
            'call_back_url' => 'https://api.mintheinkha.com/kbzpay/callback/'
        ],

        'wunzinn' => [
            'app_id' => 'kpae0c44ac8da14a3f980bc0ee363da8',
            'merch_code' => '200047',
            'key' => 'a5446c8bdaa7cde1f5b70939e725a625',
            'call_back_url' => 'wunzinn_url'
        ],


        'series' => [
//            'app_id' => 'kp92f57aaee9d24242b96e03987f55a8',
//            'merch_code' => '200101',
//            'key' => 'b0d020a10c23286c328486b9d2131d39',
//            'call_back_url' => 'wunzinn_url'
            'app_id' => 'kp6555be7c8bba4e27b4c4a85a62e148',
            'merch_code' => '200094',
            'key' => '7c50e020f04288a3643529006647fb3e',
            'call_back_url' => ''
        ],

        'H5' => [
            'sign_type' => 'SHA256',
            'method' => 'kbz.payment.queryCustInfo',
            'query_cust_info_url' => 'https://api.kbzpay.com/web/gateway/queryCustInfo',
            'mtk' => [
                // 'app_id' => 'kp86e5fd99177840879729c12bab4654',
                // 'merch_code' => '200042',
                // 'key' => 'bit123456',
                'app_id' => 'kpae0c44ac8da14a3f980bc0ee363da8',
                'merch_code' => '200047',
                'key' => 'a5446c8bdaa7cde1f5b70939e725a625',
                'redirect_url' => '',
                'form_url' => 'https://agent_api.mintheinkha.com/kbzpay/request',
                // 'form_url' => 'https://agent_api.mintheinkha.com/kbzpay/demo/request',

                'agent' => 'kbzpay',
                'agent_code' => 'KBZP1763',
                'agent_secret' => '10b28cf368534bc55c471595e73ad8dd',
            ]
        ],

        'h5' => [
            'static_url' => 'https://wap.kbzpay.com/pgw/pwa/#/?',
            'precreate__url' => 'https://api.kbzpay.com/payment/gateway/precreate',
            'query_cust_info_url' => 'https://api.kbzpay.com/web/gateway/queryCustInfo',
            'sign_type' => 'SHA256',
            'method' => 'kbz.payment.queryCustInfo',
            'mtk' => [
                'app_id' => 'kpae0c44ac8da14a3f980bc0ee363da8',
                'merch_code' => '200047',
                'key' => 'a5446c8bdaa7cde1f5b70939e725a625',
                'redirect_url' => '',
                'form_url' => 'https://agent_api.mintheinkha.com/kbzpay/request',
                'agent' => 'kbzpay',
                'agent_code' => 'KBZP1763',
                'agent_secret' => '10b28cf368534bc55c471595e73ad8dd',
            ],
            'sns' => [
                'app_id' => 'kpae0c44ac8da14a3f980bc0ee363da8',
                'merch_code' => '200047',
                'key' => 'a5446c8bdaa7cde1f5b70939e725a625',
                'redirect_url' => '',
            ]
        ]
    ],

    /**
     * @One Pay
     * @var channel
     * @var user_id
     * @var secret_key
     */
    'onepay' => [
        'verify_phone_url' => 'https://onepay.mobi/API/Ver01/Wallet/Wallet_VerifyPhoneNumber',
        'direct_payment_url' => 'https://onepay.mobi/API/Ver02/Wallet/Wallet_DirectAPIV2',
        'call_back_url' => env('APP_URL') . 'api/onepay/callback',
        'version' => '02',
        'expire_second' => '180',
        'remark' => '',
        'mtk' => [
            'channel' => 'BAGANIT',
            'user_id' => 'MM202048230948007021137',
            'secret_key' => '0D987B847FF9',
            'app_call_back_url' => 'https://api.mintheinkha.com/baganpay/callback'
        ],
        'wunzinn' => [
            'channel' => 'test',
            'user_id' => 'test',
            'secret_key' => '',
            'app_call_back_url' => 'https://api.mintheinkha.com/baganpay/callback'
        ],
        'series' => [
            'channel' => 'MIN THEIN KHA',
            'user_id' => '',
            'secret_key' => '',
            'app_call_back_url' => 'https://api.mintheinkha.com/baganpay/callback'
        ],
    ],

    /**
     * @AyaPay
     * @var key
     * @var secret
     * @var token_url
     * @var login_url
     * @var push_payment_url
     */
    'aya' => [
        'token_url' => 'https://api.ayapay.com/token',
        'login_url' => 'https://api.ayapay.com/merchant/1.0.0/thirdparty/merchant/login',
        'push_payment_url' => 'https://api.ayapay.com/merchant/1.0.0/thirdparty/merchant/requestPushPayment',
        'mtk' => [
            'key' => 'Z38ZNrD4fl9UIUXxYxJAIlbfDmsa',
            'secret' => 'Fvw_q3jr7j5mt9m3DJiDY1vh1l4a',
            'token_url' => 'https://api.ayapay.com/token',
            'login_url' => 'https://api.ayapay.com/merchant/1.0.0/thirdparty/merchant/login',
            'push_payment_url' => 'https://api.ayapay.com/merchant/1.0.0/thirdparty/merchant/requestPushPayment'
        ],
        'wunzinn' => [
            'key' => '',
            'secret' => '',
            'token_url' => '',
            'login_url' => '',
            'push_payment_url' => ''
        ],
        'series' => [
            'key' => 'Z38ZNrD4fl9UIUXxYxJAIlbfDmsa',
            'secret' => 'Fvw_q3jr7j5mt9m3DJiDY1vh1l4a',
            'token_url' => 'https://api.ayapay.com/token',
            'login_url' => 'https://api.ayapay.com/merchant/1.0.0/thirdparty/merchant/login',
            'push_payment_url' => 'https://api.ayapay.com/merchant/1.0.0/thirdparty/merchant/requestPushPayment'
        ]
    ],

    /**
     * @UabPay (SaiSai Pay)
     * @var Channel
     * @var MerchantUserId
     * @var secretKey
     */
    'saisai' => [
        'check_phone' => 'https://www.uabpaybusiness.com/API/Ver01/Wallet/Wallet_CheckPhoneNoAPI',
        'payment_enquiry_url' => 'https://www.uabpaybusiness.com/API/Ver01/Wallet/Wallet_PaymentAPI',
        'callback_url' => env('APP_URL') . 'api/saisai/callback',
        "mtk" => [
            "Channel" => "MINTHEINKHA",
            "MerchantUserId" => "UABMM202005031205299803308",
            "secretKey" => "DA49E21DD875"
        ],
        "series" => [
            "Channel" => "WUNZINN",
            "MerchantUserId" => "UABMM202002031202559832204",
            "secretKey" => "768CEFCE8FCC"
        ],
        /** @var H5
         * @var agent
         * @var agent_code
         * @var agent_secret
         */
        "h5" => [
            'form_url' => 'https://agent_api.mintheinkha.com/kbzpay/request',
            'mtk' => [
                'agent' => 'saipayh5',
                'agent_code' => 'SAIP1049',
                'agent_secret' => '104vwqqtd0h41cxjho9fyb417xp8wu4n',

                "Channel" => "MINTHEINKHA",
                "MerchantUserId" => "UABMM202005031205299803308",
                "secretKey" => "DA49E21DD875"
            ]
        ]
    ],

    /**
     * @Cb Pay
     * @var ecommerceId
     * @var subMerId
     * @var authToken
     * @var merchantId
     */
    "cb" => [
        'request_url' => 'https://122.248.120.187:4443/',
        "callback_url" => env('APP_URL') . 'cb/callback',
        'mtk' => [
            "ecommerceId" => "M010102",
            "subMerId" => "0000000000500001",
            "authenToken" => "56d73b99c37defb0178e989f4354f60f1d0870d94237b3ee56133d452129067380801f790b691cc5ffc23041e2961c0f37540ae179dd73b8489f909ba65295bf",
            "merchantId" => "412100000000005"
        ],
        'series' => [
            "ecommerceId" => "M010102",
            "subMerId" => "0000000000500001",
            "authenToken" => "56d73b99c37defb0178e989f4354f60f1d0870d94237b3ee56133d452129067380801f790b691cc5ffc23041e2961c0f37540ae179dd73b8489f909ba65295bf",
            "merchantId" => "412100000000005"
        ]
    ],

    'mytel' => [
        "redirect_url" => "https://pay.mytel.com.mm/paymentGateway/?",
        'mtk' => [
            "service_id" => "PGW_BIT",
            "service_code" => "BIT_SERVICE",
            "secret_key" => "PLZQUUTKDJ58JPN33TNJ7RQXVWCRCEEY",
            "public_key" => "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAptpd4uLByeNpVOX5CsEi1aEldDXmxKOlarUrVSvep707QMKzBd01voyj19QobudoEc3oThA1VvZGSJPC/6D7/BDQ+uyO83tL0/zNcfLsE0xFbbNlKgoadGLZR59EHxaSD0MadiNfK4l4e2RiCrqAGQeGKv5rBd6GChtI5Yugih+CiM5k65QidF2nLxikg1212v6vrV+oxILB0wrWPLHvHjicIi7OaVRIeChitP3G90L1Tu6zwAZaw2up7vy5HHn1Uz+vlUxJFQth7S2945n82oZ3JkFKEIJJmbHEZzqWbznTdvctQ7ZXMXExZXLAme29I+Ez899Hu9zzkfb4oLA8ZVC+vYzg8Bfx+bp0f20M0ZlJQxYujI924iiCkjGr6soL+98LDZ4cXfKKzdNOiXH5WrAcmFzXz+JVBN/Tk+1YnKRQkzjUz3JZnlKSHRDft2FXI8YUlNgbPb/HEPd2cL8S61GUdYIIHIWvkhiIxNRY6svvHfuB1T7MNl4/xPLE3ilK7peu/Z/NLoSLYs8i7OD4FPConBlo9RPnWuPjf0zET2pITXq9a7OSk5yeHzoBRTfV8cuD11LzIJEvt17cqx4w0Ah8y13/UT115fj6usu3/ZJCmFlH56ng3eHYilsfwPajFsrO34JF+zimwnXAOeqzcFX5mmp2aQ65uZ51L1/Fx2MCAwEAAQ=="

        ]
    ],

    /** @Payment Confirm Message For App */
    'payment_confirm_message' => [
        'kbz' => '<b>\'ငွေပေးချေမည်\'</b> ကိုနှိပ်ပါ။ <span class="eng_font">KBZPay Mobile application</span> ပွင့်လာပါမည်။ <span class="eng_font">KBZPay mobile Application</span> ထဲတွင် သင်၏ <span class="eng_font">PIN</span> နံပါတ်ကို ရိုက်ထည့်၍ ငွေပေးချေနိုင်ပါသည်။',
        'wave' => '<b>\'ငွေပေးချေမည်\'</b> ကိုနှိပ်ပါ။သင်၏ <span class="eng_font">WavePay</span> အကောင့်သို့ <span class="eng_font">Notification</span> စာတစ်စောင် ရောက်ရှိလာပါမည်။ ထို <span class="eng_font">Notification</span> ကိုနှိပ်၍ ငွေပေးချေပါ။',
        'one' => '<b>\'ငွေပေးချေမည်\'</b> ကိုနှိပ်ပါ။သင်၏ <span class="eng_font">OnePay</span> အကောင့်သို့ <span class="eng_font">Notification</span> စာတစ်စောင် ရောက်ရှိလာပါမည်။ ထို <span class="eng_font">Notification</span> ကိုနှိပ်၍ ငွေပေးချေပါ။',
        'aya' => '<b>\'ငွေပေးချေမည်\'</b> ကိုနှိပ်ပါ။သင်၏ <span class="eng_font">AyaPay</span> အကောင့်သို့ <span class="eng_font">Notification</span> စာတစ်စောင် ရောက်ရှိလာပါမည်။ ထို <span class="eng_font">Notification</span> ကိုနှိပ်၍ ငွေပေးချေပါ။',

    ],

    /** @App
     * @Text For App
     * @ImageUrl For App
     */
    'app_view_config' => [
        env('APP_MTK') => [
            'text' => 'မင်းသိင်္ခ',
            'image_url' => 'https://www.mintheinkha.com//assets/images/mintheinkha_logo.png'
        ],
        env('APP_WUNZINN') => [
            'text' => 'Wun Zinn',
            'image_url' => 'https://www.wunzinn.com/assets/css/images/logo.png'
        ],
        env('APP_SERIES') => [
            'text' => 'SERIES',
            'image_url' => 'https://osp.wunzinn.com/images/osp_darkblue.png'
        ],
        env('APP_SNS') => [
            'text' => 'SHWE NAR SIN',
            'image_url' => 'http://shwenarsin.com/image/SNS_GOLD.png'
        ]
    ],

    /** @Success Message */
    'success_msg' => [
        'wave' => '<span class="eng_font">Notification</span> တစ်စောင်ရောက်လာပါက ထို <span class="eng_font">Notification</span> ကိုနှိပ်၍ ငွေပေးချေပါ (သို့မဟုတ်) <span class="eng_font">WavePay mobile application</span> ၏ <span class="eng_font">Inbox</span> တွင် ရောက်လာသော <span class="eng_font">Notification</span> ကိုနှိပ်၍လည်း ငွေပေးချေနိုင်ပါသည်။',
        'kbz' => 'ငွေပေးချေမှု အောင်မြင်ပါသည်။ ကျေးဇူးတင်ပါသည်။',
        'one' => '<span class="eng_font">Notification</span> တစ်စောင်ရောက်လာပါက ထို <span class="eng_font">Notification</span> ကိုနှိပ်၍ ငွေပေးချေပါ (သို့မဟုတ်) <span class="eng_font">OnePay mobile application</span> ၏ <span class="eng_font">Inbox</span> တွင် ရောက်လာသော <span class="eng_font">Notification</span> ကိုနှိပ်၍လည်း ငွေပေးချေနိုင်ပါသည်။',
        'aya' => '<span class="eng_font">Notification</span> တစ်စောင်ရောက်လာပါက ထို <span class="eng_font">Notification</span> ကိုနှိပ်၍ ငွေပေးချေပါ (သို့မဟုတ်) <span class="eng_font">AyaPay mobile application</span> ၏ <span class="eng_font">Inbox</span> တွင် ရောက်လာသော <span class="eng_font">Notification</span> ကိုနှိပ်၍လည်း ငွေပေးချေနိုင်ပါသည်။',
        'saisai' => '<span class="eng_font">Notification</span> တစ်စောင်ရောက်လာပါက ထို <span class="eng_font">Notification</span> ကိုနှိပ်၍ ငွေပေးချေပါ ',

    ],

    /** @Fail Message */
    'fail_msg' => [
        'wave' => '<span class="mm-font">ငွေပေးချေမှု မအောင်မြင်ပါ။</span>',
        'kbz' => '',
        'aya_no_phone' => '<span class="mm-font">သင်၏ ဖုန်းနံပါတ်မှာ AYA Pay တွင် register ပြုလုပ်ထားခြင်းမရှိပါ။</span>',
        'saisai' => '<span class="mm-font">သင်၏ ဖုန်းနံပါတ်မှာ SaiSai Pay တွင် register ပြုလုပ်ထားခြင်းမရှိပါ။</span>'
    ],

    'app' => [
        env('APP_MTK'),
        env('APP_WUNZINN')
    ],

    /** @Key For Both Encryption and Decryption */
    'key_for_trans_id' => 'bRuD5WYw5wd0rdHR9yLlM6wt2vteuiniQBqE70nAuhU=',

    /** @CallBack Url To Application Server */
    'callback_url' => [
        'WUZ' => 'https://api.mintheinkha.com/baganpay/callback',
        'MTK' => 'https://api.mintheinkha.com/baganpay/callback',
        'SER' => 'https://series_dev.wunzinn.com/api/baganpay/callback',
        'SNS' => 'https://apiv2.shwenarsin.com/api/kbz/baganpay/callback',
        'OLP' => 'https://opapi.wunzinn.com/api/credit-coin/fill/callback'
    ],

    'code_credential' => [
        'kbz' => [
            'app_id' => 'kbz123',
            'key' => 'bRuD5WYw5wd0rdHR9yLlM6wt2vteuiniQBqE70nAuhU'
        ],
        'saisai' => [
            'app_id' => 'saisaipay',
            'key' => 'bRuD5WYw5wd0rdHR9yLlM6wt2vteuiniQBqE70nAuhU'
        ],
        'mpitesan' => [
            'app_id' => 'mpitesan',
            'key' => 'bRuD5WYw5wd0rdHR9yLlM6wt2vteuiniQBqE70nAuhU'
        ]
    ],

    'credit_code' => [
        env('APP_MTK') => [
            'url' => 'mtkurl'
        ]
    ]
];
