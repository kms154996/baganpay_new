<?php
return [
    'request_otp' => 'https://smsgw.baganit.com/api/otp/generate',
    'verify_otp' => 'https://smsgw.baganit.com/api/otp/verify',

    env('TELENOR') => [
        "access_token_url" => "http://sandbox-apigw.mytelenor.com.mm/oauth/v1/token",
        "request_url" => "http://sandbox-apigw.mytelenor.com.mm/v1/mm/en/customers/products/vas",
        'mtk' => [
            "clientId" => "t2j8uzWAdLuAC2wVZxkAjLND1jMiFzlZ",
            "clientSecret" => "DrK8oLeGxVNndKwl",
            "CP_ID" => 39,
            "CP_NAME" => "BIT",
            "USERNAME" => "bit",
            "PASSWORD" => "bit123"
        ]
    ],

    'appByProduct' => [
        'MIN_99S' => env('APP_MTK'),
        'MIN_525S' => env('APP_MTK'),
        'MIN_2100S' => env('APP_MTK'),
    ],

    'callback' => [
        env('APP_MTK') => 'http://api.mintheinkha.com/mtktelenor/callback'
    ]

];