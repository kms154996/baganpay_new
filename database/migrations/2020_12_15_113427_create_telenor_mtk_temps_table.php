<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTelenorMtkTempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telenor_mtk_temps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('msisdn');
            $table->string('clientTransactionId')->unique();
            $table->string('productCode');
            $table->string('nextRenewDate')->nullable();
            $table->string('ProcessedTime')->nullable();
            $table->string('chargingType')->nullable();
            $table->string('billingId');
            $table->string('fee')->nullable();
            $table->string('lifeCycle')->nullable();
            $table->string('reason')->nullable();
            $table->text('raw_request');
            $table->text('raw_response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('telenor_mtk_temps');
    }
}
