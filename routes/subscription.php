<?php

use Illuminate\Http\Request;

/** Otp Request And Verify */
Route::group(['prefix' => 'telenor/otp'], function () {
    /** Request */
    Route::post('/request', 'OtpController@requestOtp');
    /** Verify */
    Route::post('/verify', 'OtpController@verifyOtp');

});


/** Telenor */
Route::group(['prefix' => 'telenor'], function () {
    /** For Test Purpose */
    Route::post('/', 'Telenor\SubscriptionController@requestTelenor');

    /** Callback */
    Route::get('/callback', 'Telenor\CallBackController@callBack');
    /** Unsubscribe */
    Route::post('/unsubscribe', 'Telenor\SubscriptionController@unsubscribeTelenor');
});
