<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*********** Index Url ********/

use Illuminate\Http\Request;

Route::get('/payment/{app}', 'UiController@getUi');


/********** Path To Specific Payment *******/
Route::post('/payment/{payment}', 'UiController@postData');

/************ Start Payment *********/
Route::post('/payment', 'PaymentController@startPayment');

/******* Confirm Payment *********/
Route::get('/confirm', 'UiController@comfirmUi');


/********* Confirm Ok Payment *******/
Route::post('/pay/confirm', 'PaymentController@comfirmPayment');

/********** Success Page *******/
Route::get('/kbz/success', 'PaymentController@successKBZ');


/********** Cancel Payment *******/
Route::get('/pay/cancel', 'PaymentController@cancelPayment');

Route::get('/onepay', function (\App\Payment\OnePay $onePay) {
    dd($onePay->configData(env('APP_WUNZINN')));
});

/********* Get Confirm Message ******/
Route::get('/confirm_message/{payment_method}', function ($payment_method) {
    $_data['message'] = \Config::get('constants.payment_confirm_message.' . $payment_method);
    return \Response::json($_data);
});


Route::get('/test/abort', function () {
    abort_if(true, \Symfony\Component\HttpFoundation\Response::HTTP_FORBIDDEN, '403 Forbidden');
});


/** H5 MTK */
Route::get('/mtk/h5/uat/kbz', 'KBZ\H5\IndexController@index');
Route::get('/mtk/h5/prod/kbz', 'KBZ\H5\IndexController@index');

Route::get('/h5/confirm', 'KBZ\H5\IndexController@h5ConfirmUi');

/** SaiSai H5 */
Route::get('/saisaiuat/mtk','SaiSaiPay\H5Controller@index');
Route::get('/saisai/mtk','SaiSaiPay\H5Controller@index');

Route::get('/mpitesan/mtk',function(){
    return 'mpitesan';
});

/** Onepay H5 */
Route::get('/onepay/uat/mtk','Onepay\H5Controller@index');

/*************/


/** MyTel Success Page */
Route::post('/mytel/success', 'MyTel\CallBackController@success');

/** CB */
Route::get('/cb','CbPay\IndexController@ui');

/** Time */
Route::get('/time','SaiSaiPay\IndexController@testTime');

/** Time Out */
Route::get('/timeout','SaiSaiPay\IndexController@testTimeout');

//    $data['auto_open']=$request->get('scheme'). "://pay?keyreference=".$request->get('data')."&app=/openPWA?appid=kpae0c44ac8da14a3f980bc0ee363da8&token=8dd7a9030103cbee38792a7036c5f8e9";


//        cbuat://pay?keyreference=218aaa57-b466-42b4-810c-dd555cb9d09e&app=/openPWA?appid=kpae0c44ac8da14a3f980bc0ee363da8&token=8dd7a9030103cbee38792a7036c5f8e9';

