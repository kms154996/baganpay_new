<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/** All Payment */
Route::post('/pay/confirm', 'PaymentController@comfirmPayment');


/************* Wave Pay ***********/
Route::group(['prefix' => 'wave'], function () {

    /******** Wave Request ************/
    Route::post('/request', 'Wave\WaveController@index');

    /********* Wave Redirect ***********/
    Route::post('/callback', 'Wave\WaveController@redirect');

});

/************ KBZ *************/
Route::group(['prefix' => 'kbz'], function () {
    /********** Web Request *******/
    Route::post('/web', 'KBZ\WebController@preCreate');

    /********** App Request *********/
    Route::post('/app', 'KBZ\AppController@preCreate');

    /********* Call Back ********/
    Route::post('/callback', 'KBZ\CallBackController@callBack');


    /********* H5  *******/
    Route::post('/h5', 'KBZ\H5\IndexController@h5');

    Route::post('/callback/test', 'KBZ\CallBackController@testCallback');
});


/******** One Pay ******/
Route::group(['prefix' => 'onepay'], function () {
    /****** Verify Phone *******/
    Route::post('/verify/phone', 'Onepay\OnePayController@verifyPhone');

    /*
     * Direct Pay
     */
    Route::post('/direct/pay', 'Onepay\OnePayController@directPay');

    /*
     * Callback
     */
    Route::post('/callback', 'Onepay\CallBackController@index');

});

/** Aya */
Route::group(['prefix' => 'aya'], function () {

    /** GET Access Token */
    Route::post('/access/token', 'Aya\TokenController@getAccessToken');

    /** Get User Token */
    Route::post('/token', 'Aya\TokenController@loginUser');

    /** Start Payment */
    Route::post('/start/payment', 'Aya\IndexController@startPay');

    /** Callback */
    Route::post('/demo/callback', 'Aya\CallBackController@index');
    Route::post('/production/callback', 'Aya\CallBackController@index');

});


/** MyTel */
Route::group(['prefix' => 'mytel'], function () {
    Route::post('/', 'MyTel\IndexController@index');

    Route::post('/callback', 'MyTel\CallBackController@index');

});


/** MpiteSan */
Route::group(['prefix' => 'mpitesan'], function () {
    /** CallBack */
    Route::get('/callback', function () {
        return 'mpitesan/callback';
    });
});

/** Wave2 */
Route::group(['prefix' => 'wave2'], function () {
    /** Request */
    Route::post('/request', 'Wave\Wave2Controller@requestWave');

    /** Callback */
    Route::post('/callback', 'Wave\Wave2CallBackController@index');
});


/** SaiSai Pay */
Route::group(['prefix' => 'saisai'], function () {
    /** Merchant Login */
    Route::post('/checkphone', 'SaiSaiPay\IndexController@checkPhone');

    /** Start Payment */
    Route::post('/start/pay', 'SaiSaiPay\IndexController@startPay');

    /** Call Back  */
    ROute::post('/callback', 'SaiSaiPay\CallBackController@index');

});

/** Callback Update */
Route::post('/saisaipay/callback/update', 'SaiSaiPay\CallBackController@updateCallBack');
Route::post('/cbpay/callback/update', 'CbPay\CallBackController@updateCallBack');
Route::post('/mytelpay/callback/update', 'MyTel\CallBackController@updateCallBack');
Route::post('/wave2pay/callback/update', 'Wave\Wave2CallBackController@updateCallBack');


Route::post('/baganpay', 'Api\BaganPayController@index');


/** Telenor Callback Test */
Route::get('/subscription/telenor/callback', function (Request $request) {
    DB::select('insert into callbacks (content) values (?)', [json_encode($request->all())]);

    return \Response::json(['status' => 'success']);
});

Route::get('/test/format', function () {
    $string = str_split(222, 1);

    (array_walk($string, 'test_print'));

});

function test_print($value, $key)
{

    $_i = $value .= '.';
    echo $_i;
}


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/** All Payment */
Route::post('/pay/confirm', 'PaymentController@comfirmPayment');


/************* Wave Pay ***********/
Route::group(['prefix' => 'wave'], function () {

    /******** Wave Request ************/
    Route::post('/request', 'Wave\WaveController@index');

    /********* Wave Redirect ***********/
    Route::post('/callback', 'Wave\WaveController@redirect');

});

/************ KBZ *************/
Route::group(['prefix' => 'kbz'], function () {
    /********** Web Request *******/
    Route::post('/web', 'KBZ\WebController@preCreate');

    /********** App Request *********/
    Route::post('/app', 'KBZ\AppController@preCreate');

    /********* Call Back ********/
    Route::post('/callback', 'KBZ\CallBackController@callBack');


    /********* H5  *******/
    Route::post('/h5', 'KBZ\H5\IndexController@h5');

    Route::post('/callback/test', 'KBZ\CallBackController@testCallback');
});


/******** One Pay ******/
Route::group(['prefix' => 'onepay'], function () {
    /****** Verify Phone *******/
    Route::post('/verify/phone', 'Onepay\OnePayController@verifyPhone');

    /*
     * Direct Pay
     */
    Route::post('/direct/pay', 'Onepay\OnePayController@directPay');

    /*
     * Callback
     */
    Route::post('/callback', 'Onepay\CallBackController@index');

});

/** Aya */
Route::group(['prefix' => 'aya'], function () {

    /** GET Access Token */
    Route::post('/access/token', 'Aya\TokenController@getAccessToken');

    /** Get User Token */
    Route::post('/token', 'Aya\TokenController@loginUser');

    /** Start Payment */
    Route::post('/start/payment', 'Aya\IndexController@startPay');

    /** Callback */
    Route::post('/demo/callback', 'Aya\CallBackController@index');
    Route::post('/production/callback', 'Aya\CallBackController@index');

});


/** MyTel */
Route::group(['prefix' => 'mytel'], function () {
    Route::post('/', 'MyTel\IndexController@index');

    Route::post('/callback', 'MyTel\CallBackController@index');

});


/** MpiteSan */
Route::group(['prefix' => 'mpitesan'], function () {
    /** CallBack */
    Route::get('/callback', function () {
        return 'mpitesan/callback';
    });
});

/** Wave2 */
Route::group(['prefix' => 'wave2'], function () {
    /** Request */
    Route::post('/request', 'Wave\Wave2Controller@requestWave');

    /** Callback */
    Route::post('/callback', 'Wave\Wave2CallBackController@index');
});


/** SaiSai Pay */
Route::group(['prefix' => 'saisai'], function () {
    /** Merchant Login */
    Route::post('/checkphone', 'SaiSaiPay\IndexController@checkPhone');

    /** Start Payment */
    Route::post('/start/pay', 'SaiSaiPay\IndexController@startPay');

    /** Call Back  */
    ROute::post('/callback', 'SaiSaiPay\CallBackController@index');

});

/** Callback Update */
Route::post('/saisaipay/callback/update', 'SaiSaiPay\CallBackController@updateCallBack');
Route::post('/cbpay/callback/update', 'CbPay\CallBackController@updateCallBack');
Route::post('/mytelpay/callback/update', 'MyTel\CallBackController@updateCallBack');
Route::post('/wave2pay/callback/update', 'Wave\Wave2CallBackController@updateCallBack');


Route::post('/baganpay', 'Api\BaganPayController@index');


/** Telenor Callback Test */
Route::get('/subscription/telenor/callback', function (Request $request) {
    DB::select('insert into callbacks (content) values (?)', [json_encode($request->all())]);

    return \Response::json(['status' => 'success']);
});



//http://paydev.baganit.com/api/subscription/telenor/callback
//http://paydev.baganit.com/api/subscription/telenor/redirect