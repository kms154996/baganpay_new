<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'BaganPay') }} | @yield('app')</title>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href='https://mmwebfonts.comquas.com/fonts/?font=myanmar3'/>
    <style>
        .mm-font {
            font-family: Myanmar3, Yunghkio;
        }

        .eng_font {
            font-family: Arial !important;
        }
    </style>

    <SCRIPT type="text/javascript">
        window.history.forward();

        function noBack() {
            window.history.forward();
        }
    </SCRIPT>

</head>
<body onload="noBack();"
      onpageshow="if (event.persisted) noBack();" onunload="">


<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#"> {{ config('app.name', 'Laravel') }}</a>
</nav>

<div id="app">

    <div class="container">
        <div class="card">
            <div class="card-body">

                <table>
                    <tr>
                        <td style="width: 30%">
                            <img src="{{ $image_url }}" alt="" class="img-fluid rounded-circle">
                        </td>
                        <td class="" style="padding-left: 10px">
                            <ul style="list-style: none !important;">
                                <li>Order Number - **** {!! substr($trans_id,-4) !!} </li>
                                <li>Amount - {{ number_format($amount) }} MMK</li>
                            </ul>
                        </td>
                    </tr>
                </table>

                <payment_format></payment_format>

                <confirm_message></confirm_message>


                <router-view payment_method='{!!  json_encode($payment_method)  !!}'
                             app="{{ $app }}"
                             amount="{{ $amount }}"
                             trans_id="{{ $trans_id }}">
                </router-view>

                <v-overlay :value="overlay">
                    <v-progress-circular indeterminate size="64"></v-progress-circular>
                </v-overlay>

            </div>


        </div>
    </div>


</div>

<!-- Scripts -->
<script>

    window.app_url = "{{ env('APP_URL') }}";
    console.log(window.app_url)

</script>
<script src="{{ mix('js/app.js') }}"></script>
@yield('js')
</body>
</html>

