@extends('layouts.app')

@section('app',$app)


@section('content')

    <style>
        .form-check:hover {
            cursor: pointer !important;
        }
    </style>

    <div class="container">

        <div class="panel">
            <div class="panel-body">
                <h3>{{ $app }}</h3>

                <hr>


                <p>
                    <b>
                        {{ $payment_confirm_message }}
                    </b>
                </p>

                <hr>

                <form method="post" action="{{ url('/pay/confirm') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="app" value="{{ $app }}">
                    <input type="hidden" name="trans_id" value="{{ $trans_id }}">
                    <input type="hidden" name="payment_method" value="{{ $payment_method }}">


                    @isset($phone)
                        <div class="form-group">
                            <label for="exampleInputEmail1">Phone</label>
                            <input type="number" class="form-control" name="phone" value="{{ $phone }}" readonly>
                        </div>
                    @endisset

                    <div class="form-group">
                        <label for="exampleInputEmail1">Amount</label>
                        <input type="number" class="form-control" name="amount" value="{{ $amount }}" readonly>
                    </div>

                    <hr>

                    <div class="form-group">
                        <label style="display: inline">Payment - </label>

                        <h3 style="display: inline">
                            @include('pay.payment_format',['pay'=>$payment_method])
                        </h3>
                    </div>

                    <hr>


                    <div class="form-group">
                        <button class="btn btn-primary " type="submit" id="payBtn">
                            Ok
                        </button>
                        <a href="{{ url('/pay/cancel?info='.$trans_id) }}" role="button" class="btn btn-danger">
                            Cancel
                        </a>
                    </div>

                </form>
            </div>
        </div>
    </div>

@stop