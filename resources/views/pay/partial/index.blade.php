@extends('layouts.app')

@section('app',$app)


@section('content')

    <style>
        .form-check:hover {
            cursor: pointer !important;
        }
    </style>

    <div class="container">

        <div class="panel">
            <div class="panel-body">
                <h3>{{ $app }}</h3>

                <hr>

                <form id="payStart">

                    <div class="form-group">
                        <label for="exampleInputEmail1">Amount</label>
                        <input type="number" class="form-control" name="amount" value="{{ $amount }}" readonly>
                    </div>

                    <hr>

                    <div class="form-group">
                        <label style="display: inline">Payment - </label>

                        <h3 style="display: inline">
                            @include('pay.payment_format',['pay'=>$payment_method])
                        </h3>

                    </div>

                    <hr>

                    @include('pay.partial.'.$payment_method.'_pay')

                    <hr>
                    <div class="form-group">
                        <button class="btn btn-primary btn-lg" id="payBtn">
                            Pay
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>

@stop

@section('js')
    <script>
        $(document).ready(() => {
            $('#alert').hide();
        });

        $(document)
            .ajaxStart(function () {
                $('#payBtn').text('Processing')
                $('#payBtn').disabled = true
            })
            .ajaxStop(function () {
                $('#payBtn').text('Pay')
            });


        $('#phone').on('input', () => {
            $('#payBtn').text('Pay')
            $('#alert').hide();


        });

        $('#payBtn').on('click', (e) => {

            if ($('#phone').val() === '') {
                $('#alert').show();
                $('#alert_msg').text('Phone Number Need To Fail')
                return
            }

            e.preventDefault();
            $('#alert').hide();
            $.ajax({
                url: '{{ url('/payment') }}',
                method: 'post',
                data: {
                    _token: "{{ csrf_token() }}",
                    app: '{{ $app }}',
                    payment_method: '{{ $payment_method }}',
                    amount: '{{ $amount }}',
                    trans_id: '{{ $trans_id }}',
                    phone: $('#phone').val()
                }
            }).then((resp) => {

                let {status, error_msg} = resp;
                if (status === 0) {
                    $('#payBtn').text('Try Again');
                    $('#alert').show();
                    $('#alert_msg').text(error_msg)
                } else {
                    let url = resp.url;
                    window.location.replace(url)
                }
            });
        })
    </script>
@stop


