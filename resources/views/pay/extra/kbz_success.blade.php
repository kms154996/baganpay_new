@extends('layouts.layout')

@section('meta')

    <meta property="al:android:url" content="mintheinkha://home/home">
    <meta property="al:android:package" content="com.bit.mintheinkha">
    <meta property="al:android:app_name" content="MinTheinKha">
    <meta property="al:web:should_fallback" content="false">
    <meta property="og:url" content="https://pay.baganit.com/kbz/success"/>
    <meta property="fb:app_id" content="306118420176326"/>

    <!-- <meta name="og:title" property="title" content="" /> -->
    <!-- <meta name="og:description" property="description" content="Articles" />        -->
    <meta name="og:title" property="title" content="မင္းသိခၤ ေဗဒင္ Application"/>
    <meta name="og:type" property="type" content="website"/>

@stop

@section('app','')


@section('content')


    <div class="container " style="margin-top: 20px !important;">
        <div class="card">
            <div class="card-body text-center">
                <div class="card-text">
                    <h5 class="mm-font">{{ Config::get('constants.success_msg.kbz') }}</h5>
                    <a href="{{ $_url }}" class="btn btn-success " role="button">
                       {{ $_btn_text}}
                    </a>
                </div>
            </div>
        </div>
    </div>

@stop

@section('js')
@stop
