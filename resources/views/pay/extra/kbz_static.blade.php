@extends('layouts.app')

@section('app','')


@section('content')

    <div id="static">
        <div class="container">
            <div class="panel">
                <div class="panel-body text-center">
                    @if($_show_btn)

                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid eaque explicabo in laborum
                            mollitia nulla quae reiciendis rerum temporibus ullam. Aliquid distinctio perspiciatis
                            similique
                            ut velit! Doloremque expedita nihil porro!
                        </p>

                        <hr>

                        <button class="btn btn-success" id="pay_success">
                            <b> Continue Pay</b>
                        </button>

                    @else

                        <button class="btn btn-danger">
                            Pay Fail
                        </button>

                    @endif
                </div>
            </div>
        </div>
    </div>

@stop

@section('js')

    <script>

        {{--new Vue({--}}
        {{--    el:'#static',--}}
        {{--    created:function(){--}}
        {{--            window.location .href= '{!!   $_url !!}'--}}

        {{--        console.log('{!! $_url !!}}')--}}
        {{--    }--}}
        {{--});--}}

        $('#pay_success').on('click', () => {
            window.location = '{!!   $_url !!}'
        })

        {{--$(document).ready(() => {--}}
        {{--    window.location.replace('{!!   $_url !!}')--}}

        {{--});--}}
    </script>

@stop