@extends('layouts.layout')

@section('app','ERROR')

@section('content')


    <div class="container " style="margin-top: 20px !important;">
        <div class="card">
            <div class="card-body">
                <div class="card-text">
                    <h5>Something Went Wrong !!!</h5>
                </div>
            </div>
        </div>
    </div>


@stop
