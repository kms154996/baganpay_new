<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Min Thein Kha</title>
    <script
        src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
        crossorigin="anonymous"></script>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <style>
        .modal {
            box-shadow: none !important;
            background-color: gray !important;
            height: 100% !important;

        }
    </style>
</head>
<body>


<!-- Modal Structure -->
<div id="modal1" class="modal">
    <div class="modal-content center-align" style="vertical-align: center">
        <div class="preloader-wrapper big active" style="vertical-align: middle">
            <div class="spinner-layer spinner-blue-only">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/kbzpay2.js') }}"></script>

<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {
        let Modalelem = document.querySelector('.modal');
        let instance = M.Modal.init(Modalelem);
        instance.open();
    });


    let url = '{{ $url }}';


    function callback_f(data) {
        // alert(url)
        // alert(JSON.stringify(data));
        location.replace(url)
    }

    function error_f(data) {
        alert(data.code, data.msg)
    }

    let prepayId = "{{ $prepay_id }}";

    let orderInfo = "{!! $order_info !!} ";

    let sign = "{{ $sign }}";

    $('document').ready(function () {
        kbzpay.startPay(prepayId, orderInfo, sign, callback_f, error_f);
    });

</script>


<script>

</script>
</body>
</html>
