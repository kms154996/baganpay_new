/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import vuetify from "./plugin/vuetify";
import routes from "./routes";

import pay_index from './components/pay/index'
import confirm_message from "./components/pay/confirm_message";
import payment_format from "./components/pay/payment_format";
import {pay_btn, back_btn} from "./constant";

import store from "./store";

Vue.component('pay_index', pay_index)
Vue.component('confirm_message', confirm_message)
Vue.component('payment_format', payment_format)


import VueRouter from 'vue-router';

Vue.config.devtools = true;

const router = new VueRouter({
    routes
});

Vue.router = router;

let globalData = new Vue({
    data: {
        overlay: true,
        pay_btn: pay_btn,
        back_btn: back_btn
    }
});

Vue.mixin({
    computed: {
        overlay: {
            get: function () {
                return globalData.$data.overlay
            },
            set: function (newState) {
                globalData.$data.overlay = newState;
            }
        },
        pay_btn: {
            get: function () {
                return globalData.$data.pay_btn
            },
            set: function (newState) {
                globalData.$data.pay_btn = newState
            }
        },
        back_btn: {
            get: function () {
                return globalData.$data.back_btn
            },
            set: function (newState) {
                globalData.$data.back_btn = newState
            }
        }
    }
})


Vue.use(VueRouter);

new Vue({
    el: '#app',
    router: router,
    store,
    vuetify,
}).$mount('#app')
