export const SET_PAYMENT = "SET_PAYMENT"
export const SET_APP_DATA = "SET_APP_DATA"
export const SET_CONFIRM_MESSAGE = "SET_CONFIRM_MESSAGE"
export const NEED_CHECK = [
    'wave',
    'one'
];

export const pay_btn = 'ငွေပေးချေမည်';
export const back_btn = 'နောက်သို့';
