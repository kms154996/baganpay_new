export const toggleOverlay = (state) => {
    return !state;
}

export const changeComponent = (payment) => {
    switch (payment) {
        case 'kbz':
            return 'confirm';
        case 'one':
            return 'one';
        case 'wave':
            return 'wave';
        case 'aya':
            return 'aya';
        default:
            return '';
    }
}

export const changePayFormat = (payment) => {
    switch (payment) {
        case 'kbz':
            return payment.toUpperCase() + 'Pay';
        case 'wave':
            return payment[0].toUpperCase() + payment.substr(1) + ' Pay';
        case 'one':
            return payment[0].toUpperCase() + payment.substr(1) + 'Pay';
        case 'aya':
            return payment[0].toUpperCase() + payment.substr(1) + 'Pay';

    }
}

