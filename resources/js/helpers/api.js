import axios from 'axios';

const api_request = (url, data) => {
    return axios.post(url, data);
}

export default api_request
