import Vue from 'vue';
import Vuex from 'vuex';
import {SET_PAYMENT, SET_APP_DATA, SET_CONFIRM_MESSAGE} from './constant';

Vue.use(Vuex);

const initialState = {
    app_data: {
        app: '',
        amount: '',
        trans_id: '',
    },
    confirm_message: '',
    payment_method: '',
};

const store = new Vuex.Store({
    state: initialState,
    getters: {
        getPayment: state => {
            return state.payment_method
        }
    },
    mutations: {
        [SET_PAYMENT]: (state, data) => {
            state.payment_method = data
        },
        [SET_APP_DATA]: (state, data) => {
            state.app_data = Object.assign(state.app_data, data);
        },
        [SET_CONFIRM_MESSAGE]: (state, data) => {
            state.confirm_message = data
        }
    },
    actions: {
        setPayment({commit}, data) {
            commit(SET_PAYMENT, data)
        },
        setAppData({commit}, data) {
            commit(SET_APP_DATA, data);
        },
        setConfirmMessage({commit}, data) {
            commit(SET_CONFIRM_MESSAGE, data)
        }
    }
});

export default store;
