import PayIndex from './components/pay/index';

import One from "./components/pay/partial/one";
import Wave from "./components/pay/partial/wave";
import Kbz from "./components/pay/partial/kbz"
import Aya from './components/pay/partial/aya'
import CheckSuccess from './components/pay/checksuccess'
import NotiSend from './components/pay/success/noti_send'
import NotiFail from './components/pay/fail/noti_fail'


const routes = [
    {
        path: '/',
        component: PayIndex,
        name: 'Index',
    },
    {
        path: '/kbz',
        component: Kbz,
    },
    {
        path: '/one',
        component: One
    },
    {
        path: '/wave',
        component: Wave
    },
    {
        path: '/aya',
        component: Aya
    },
    {
        path: '/checksuccess',
        component: CheckSuccess
    },
    {
        path: '/notisuccess',
        component: NotiSend
    },
    {
        path: '/notifail',
        component: NotiFail
    }
];

export default routes;
