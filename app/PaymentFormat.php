<?php


namespace App;


class PaymentFormat
{
    /**
     * @param $_pay
     * @return string
     */
    public function paymentFormat($_pay): string
    {
        switch ($_pay) {
            case 'kbz.vue':
                $_pay_msg = strtoupper($_pay) . ' Pay';
                break;
            case 'wave':
                $_pay_msg = ucfirst($_pay) . 'Pay';
                break;
            case 'one':
                $_pay_msg = ucfirst($_pay) . 'pay';
                break;
            default:
                $_pay_msg = $_pay;
        }

        return $_pay_msg;
    }
}
