<?php

namespace App\Http\Middleware\Code;

use App\Exceptions\BasicAuth;
use Closure;
use DB;

class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (isset($_SERVER['HTTP_AUTHORIZATION'])) {
            if (strpos(strtolower(trim($_SERVER['HTTP_AUTHORIZATION'])), 'basic') === 0) {
                list($username, $password) = explode(':', base64_decode(substr(trim($_SERVER['HTTP_AUTHORIZATION']), 6)));

                $_check_ = DB::select("select * from basic_auth_codes
                                              where username=?
                                              and code=?
                                               and BINARY(username) = BINARY(?)
                                               and BINARY(code) = BINARY(?) limit 1", [
                    $username,
                    $password,
                    $username,
                    $password]);


                $_check_status = count($_check_);

                if ($_check_status === 1) {
                    $request['app'] = $_check_[0]->app;
                    return $next($request);
                } else {
                    throw new \App\Exceptions\Code\BasicAuth('Check UserName and Password');
                }

            } else {
                throw new \App\Exceptions\Code\BasicAuth('Check Header Has Necessary Data');
            }
        } else {
            throw new \App\Exceptions\Code\BasicAuth('Check Header Authorization Key');
        }
    }

}
