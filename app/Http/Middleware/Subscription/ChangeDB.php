<?php

namespace App\Http\Middleware\Subscription;

use Closure;
use Config;

class ChangeDB
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Config::set('database.default','subscription');
        return $next($request);
    }
}
