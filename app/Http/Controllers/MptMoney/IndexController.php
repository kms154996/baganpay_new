<?php

namespace App\Http\Controllers\MptMoney;

use App\Model\MptMoney\MptMoneyRequest;
use App\Repository\Config\BaseConfigInterface;
use App\Repository\MptMoney\ServiceInterface;
use App\Traits\helperTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;

class IndexController extends Controller
{
    use helperTrait;

    protected $_baseConfig, $_service, $_payment;

    public function __construct(BaseConfigInterface $baseConfig, ServiceInterface $service)
    {
        $this->_baseConfig = $baseConfig;
        $this->_service = $service;
        $this->_payment = 'mptmoney';
    }

    public function index(Request $request)
    {

//        return json_encode($request);

        $_send_data = json_encode($request->all());
        $_send_data_array = json_decode($_send_data, true);

        $_app = $_send_data_array['app'];

        $_h5 = $this->_baseConfig->checkH5($_send_data_array);

        $_configData = $this->_baseConfig->getConfig($_app, $this->_payment, $_h5);

        list($_generate_trans_id, $_request_arr) = $this->_baseConfig->getBpId($_send_data_array);

        $_send_data_array['bp_trans_id'] = $_generate_trans_id;
        $_send_data_array['h5'] = $_h5;
        $_send_data_array['trans_id'] = $this->my_decrypt($_send_data_array['trans_id']);
        unset($_send_data_array['payment_method']);

        MptMoneyRequest::create($_send_data_array);

        $_mptRequest = $this->_service->createArray($_configData, $_request_arr);

        $_temp = $this->_service->getTemp($_app);
//
        $_encrypt = $this->_service->rsaEncrypt($_mptRequest, $_configData['publicKey']);
//
        $_temp::create([
            'trans_id' => $_send_data_array['trans_id'],
            'bp_trans_id' => $_send_data_array['bp_trans_id'],
            'partnerRefNo' => $_send_data_array['bp_trans_id'],
            'amount' => $_send_data_array['amount'],
            'encrypted' => $_encrypt
        ]);

        return Response::json([
            'url'=> $_configData['redirect_url'] . urlencode($_encrypt),
            'status'=>1,
            'message'=>'Success'
        ]);

    }
}
