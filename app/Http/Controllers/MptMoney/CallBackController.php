<?php

namespace App\Http\Controllers\MptMoney;

use App\Model\MptMoney\MptMoneyCallBack;
use App\Repository\Config\BaseConfigInterface;
use App\Repository\MptMoney\ServiceInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class CallBackController extends Controller
{

    protected $_publicKeyArr, $_baseConfig, $_service, $_callBackUpdateUrl, $_payment;

    public function __construct(BaseConfigInterface $baseConfig, ServiceInterface $service)
    {
        $this->_baseConfig = $baseConfig;
        $this->_service = $service;
        $this->_publicKeyArr = [
            'mtk' => "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCzw1Ib0qlD3ynCPLMUAqE2fkzeXuV5YLAyHtd/Gw8gPMeRWea9caentQpK5k4VFHjCP1LPmWlK7tsyi/9KYbRtK9/Gpasja0qSC1M/ZH6pJZZp81DWiGzQ8gAZ8YVp9+sW6eaH4CvDqCBeMSOTw3VS5yq7ArENbgIcSzCC05fZDwIDAQAB"
        ];
        $this->_payment = 'mptmoney';
        $this->_callBackUpdateUrl = env('APP_URL') . 'api/mptmoney/callbackupdate';
    }

    public function index(Request $request, $_app)
    {
        $_publicKey = $this->getPublicKey($_app);

        $_raw = $this->_service->decrypt($_publicKey, $request->get('result'));

        $_result = json_decode($_raw, true);

        $_bp_trans_id = $_result['partnerRefNo'];

        [
            'app' => $_result['app'],
            'trans_id' => $_result['trans_id'],
            'bp_trans_id' => $_result['bp_trans_id'],
            'amount' => $_result['amount']
        ] = (array)$this->_service->getApp($_bp_trans_id);


        $_result['raw'] = $_raw;

        MptMoneyCallBack::create($_result);

        $curl_command = "curl --data '" . http_build_query($_result) . "' -X POST " . $this->_callBackUpdateUrl . " > /dev/null 2>/dev/null &";

        exec($curl_command);


    }

    public function callBackUpdate(Request $request)
    {
        $_app = $request->get('app');
        $_bp_trans_id = $request->get('bp_trans_id');
        $_trans_id = $request->get('trans_id');
        $_amount = $request->get('amount');
        $_statusCode = $request->get('statusCode');

        if ($_statusCode == 'transaction_confirmed') {

            $_temp = $this->_service->getTemp($_app);

            $_updateData = $_temp::where('partnerRefNo', $request->get('partnerRefNo'))->first();
            $_updateData->status = 1;
            $_updateData->save();

            $_redirect_json_arr['amount'] = $_amount;
            $_redirect_json_arr['trans_id'] = $_trans_id;
            $_redirect_json_arr['payment_method'] = $this->_payment;
            $_redirect_json_arr['status'] = 1;
            $_redirect_json_arr['message'] = 'success';
            $_redirect_json_arr['bp_trans_id'] = $_bp_trans_id;

            $_call_back_url = $this->_baseConfig->getCallBackUrlUpdate($_app);

            $this->_baseConfig->createCallBackLog($_redirect_json_arr, $_app, $this->_payment);

            $this->_baseConfig->callToApp($_call_back_url, $_redirect_json_arr);
        }
    }

    public function getPublicKey($_app)
    {

        $_app_ = $this->_baseConfig->changeAppName($_app);

        $_app_env = env($_app_['app_upper'] . '_MPTMONEY_ENV');

        $_app_short = $_app_['app'];

        $_public_key = '';

        switch ($_app_env) {
            case 'DEV':
                $_public_key = $this->_publicKeyArr[$_app_short];
                break;
            case 'PROD':
                $_public_key = \Config::get('constants.mptmoney.' . $_app_short . '.publicKey');
                break;
        }
        return $_public_key;
    }
}
