<?php

namespace App\Http\Controllers\Api;

use App\Model\PaymentRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\helperTrait;
use Response;
use DB;

class BaganPayController extends Controller
{
    use helperTrait;

    public function index(Request $request)
    {
        $_request_data = $request->getContent();
        $_request_arr = json_decode($_request_data, true);

        $_app = $_request_arr['app'];
        $_trans_id = $_request_arr['trans_id'];
        $_amount = $_request_arr['amount'];

//        $_amount = "5";

        if (isset($_request_arr['type'])) {
            if ($_request_arr['type'] === 'DEV') {
                $_dev = true;
            } else {
                $_dev = false;
            }
        } else {
            $_dev = false;
        }


        if (isset($_request_arr['payment_method'])) {
            $_payment = $_request_arr['payment_method'];
        } else {
            $_payment = false;
        }


        DB::beginTransaction();

        try {

            $_query = 'insert into
                        payment_requests (
                            trans_id,app,amount,timetick,created_at,updated_at
                                        )
                             values (?,?,?,?,?,?)';

            $_current_time = date('Y-m-d H:i:s');

            DB::insert($_query, [$_trans_id, $_app, $_amount, time(), $_current_time, $_current_time]);

            DB::commit();

        } catch (\Exception $e) {

            DB::rollback();

            $_data['status'] = 0;
            $_data['url'] = '';
            $_data['encrypt'] = '';
            $_data['message'] = 'Something Went Wrong!';

            return Response::json($_data);


        }
        $_result = $this->getUrl($_app, $_trans_id, $_amount, $_dev, $_payment);

        return Response::json($_result);

    }


    public function getUrl($_app, $_trans_id, $_amount, $_dev, $_payment)
    {
        $_encrypt_trans_id = $this->my_encrypt($_trans_id);

        if ($_dev) {

            $_url = env('APP_URL') . 'payment/' . $_app . '?info=' . $_encrypt_trans_id . '&amount=' . $_amount . '&dev=' . $_dev;

        } else {
            $_url = env('APP_URL') . 'payment/' . $_app . '?info=' . $_encrypt_trans_id . '&amount=' . $_amount;

        }

        if ($_payment !== false) {
            $_url .= '#/' . $_payment;
        }

        $_data['status'] = 1;
        $_data['url'] = $_url;
        $_data['encrypt'] = $_encrypt_trans_id;
        $_data['message'] = 'success';

        return $_data;
    }
}
