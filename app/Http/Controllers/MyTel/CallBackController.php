<?php

namespace App\Http\Controllers\MyTel;

use App\Model\MyTel\CallBack;
use App\Repository\Config\BaseConfigInterface;
use App\Repository\MYTEL\PayServiceInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use Config;

class CallBackController extends Controller
{
    protected $_service, $_baseConfig, $_pay_method, $_payment, $_callBackUpdateUrl;

    public function __construct(PayServiceInterface $service, BaseConfigInterface $baseConfig)
    {
        $this->_service = $service;
        $this->_baseConfig = $baseConfig;
        $this->_pay_method = 'mytel';
        $this->_payment = 'mytel';
        $this->_callBackUpdateUrl = env('APP_URL') . 'api/mytelpay/callback/update';
    }

    public function index(Request $request)
    {

        $_getContent = $request->getContent();

        $_callBackArr = json_decode($_getContent, true);

        ['app' => $_app, 'trans_id' => $_trans_id, 'bp_trans_id' => $_bp_trans_id, 'amount' => $_amount] = (array)$this->_service->getAppData($_callBackArr['orderId']);

        $_callBackArr['trans_id'] = $_trans_id;
        $_callBackArr['bp_trans_id'] = $_bp_trans_id;
        $_callBackArr['app'] = $_app;
        $_callBackArr['rawData'] = $_getContent;
        $_callBackArr['amount'] = $_amount;


        CallBack::create($_callBackArr);


        $curl_command = "curl --data '" . http_build_query($_callBackArr) . "' -X POST " . $this->_callBackUpdateUrl . " > /dev/null 2>/dev/null &";

        exec($curl_command);

        return Response::json([
            'status' => 1,
            'message' => 'Success'
        ]);


    }

    public function success(Request $request)
    {
        $orderId = $request->get('orderId');

        ['app' => $_app, 'trans_id' => $_trans_id, 'bp_trans_id' => $_bp_trans_id] = (array)$this->_service->getAppData($request->get('orderId'));

        $_url = Config::get('constants.app_redirect_url.' . $_app);

        switch ($_app) {
            case env('APP_MTK'):
                $_url .= "?trans_id=" . $_trans_id;
                break;
            default:
        }

        return redirect($_url);

    }

    /** Update Callback
     * @param Request $request
     */
    public function updateCallBack(Request $request)
    {
        $_app = $request->get('app');
        $_bp_trans_id = $request->get('bp_trans_id');
        $_amount = $request->get('amount');
        $_trans_id = $request->get('trans_id');
        $_callBackArr = json_decode($request->get('rawData'), true);

        $_temp = $this->_service->tempMOdel($_app);

        if ($_callBackArr['statusCode'] == 1) {
            $_pay_status = 1;
            $_pay_message = 'success';
        } else {
            $_pay_status = 0;
            $_pay_message = 'fail';
        }

        $_call_back_url = $this->_baseConfig->getCallBackUrlUpdate($_app);


        $this->_service->updateCallBackTemp($_temp, $_pay_status, $_bp_trans_id, json_encode($_callBackArr));

        $_redirect_json_arr['amount'] = $_amount;
        $_redirect_json_arr['trans_id'] = $_trans_id;
        $_redirect_json_arr['payment_method'] = $this->_payment;
        $_redirect_json_arr['status'] = $_pay_status;
        $_redirect_json_arr['message'] = $_pay_message;
        $_redirect_json_arr['bp_trans_id'] = $_bp_trans_id;


        $this->_baseConfig->createCallBackLog($_redirect_json_arr, $_app, $this->_payment);

//        $this->_baseConfig->callToApp($_call_back_url, $_redirect_json_arr);

        $this->_baseConfig->curlFunc($_call_back_url, $_redirect_json_arr);


    }
}
