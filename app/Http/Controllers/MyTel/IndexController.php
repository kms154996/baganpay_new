<?php

namespace App\Http\Controllers\MyTel;

use App\Repository\Config\BaseConfigInterface;
use App\Repository\MYTEL\PayService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use phpseclib\Crypt\RSA;
use Response;
use Config;
use App\Traits\helperTrait;
use App\TransID;

class IndexController extends Controller
{
    use helperTrait;

    protected $baseConfig;
    protected $_mytel;

    public function __construct(BaseConfigInterface $baseConfig, PayService $service)
    {
        $this->baseConfig = $baseConfig;
        $this->_mytel = $service;
    }

    public function index(Request $request)
    {
        $_send_data = json_encode($request->all());

        $_send_data_array = json_decode($_send_data, true);

        $_app = $_send_data_array['app'];

        $_trans_id = $this->my_decrypt($_send_data_array['trans_id']);

        list($_generate_trans_id, $_request_arr) = $this->getBpId($_send_data_array);
dd($this->baseConfig->getConfig($_app, $_send_data_array['payment_method'], $this->baseConfig->checkH5($_send_data_array)));
        [
            'service_id' => $_service_id,
            'service_code' => $_service_code,
            'secret_key' => $_secret_key,
            'redirect_url' => $_redirect_url,
            'public_key' => $_public_key
        ] = $this->baseConfig->getConfig($_app, $_send_data_array['payment_method'], $this->baseConfig->checkH5($_send_data_array));


        $_my_tel_request_arr = $this->_mytel->requestArr($_send_data_array, $_generate_trans_id, $_service_id, $_service_code);

        $_hash_value = $this->_mytel->hashArr($_my_tel_request_arr, $_secret_key);

        $_rsa_encrypt = $this->_mytel->rsaEncrypt($_my_tel_request_arr, $_public_key);

        $_mytel_data = "\"message\"%3A\"" . $_rsa_encrypt . "\"%2C";
        $_mytel_data .= "\"hashValue\"%3A\"" . $_hash_value . "\"%7D";

        $this->_mytel->storeRequest($_request_arr);

        $_temp_model = $this->_mytel->tempModel($_request_arr['app']);

        unset($_request_arr['h5']);

        $_url = $_redirect_url . $_mytel_data;

        $_request_arr['encrypted'] = $_rsa_encrypt;
        $_request_arr['hashValue'] = $_hash_value;
        $_request_arr['url'] = $_url;

        $this->_mytel->storeTemp($_temp_model, $_request_arr);

         $_url = str_replace('"', '%22', $_url);

        return Response::json([
            'url'=>$_url,
            'status'=>1,
            'message'=>'success'
        ]);


    }

    function encodeURIComponent($str)
    {
        $revert = array('%21' => '!', '%2A' => '*', '%27' => "'", '%28' => '(', '%29' => ')');
        return strtr(rawurlencode($str), $revert);
    }

    /**
     * @param $_request_arr
     * @return array
     */
    private function getBpId($_request_arr): array
    {
        $_trans_class = new TransID();
        $_generate_trans_id = $_trans_class->generatedID($_request_arr['app']);
        $_request_arr['bp_trans_id'] = $_generate_trans_id;
        return array($_generate_trans_id, $_request_arr);
    }

}
