<?php

namespace App\Http\Controllers\MyTel;

use App\Repository\Config\BaseConfigInterface;
use App\TransID;
use DateTimeImmutable;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use \Firebase\JWT\JWT;
use App\Traits\helperTrait;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key;
use Config;

//use Emarref\Jwt\Claim;
//use Emarref\Jwt\Token;
//use Emarref\Jwt;
//use Emarref\Jwt\Algorithm;
//use Emarref\Jwt\Serialization;


class H5Controller extends Controller
{
    use helperTrait;

    protected $_baseConfig;

    public function __construct(BaseConfigInterface $baseConfig)
    {
        $this->_baseConfig = $baseConfig;
    }

//    public function index(Request $request)
//    {
//
//        if (count($request->all()) > 0) {
//            $_key = array_keys($request->all())[0];
//            $_userAgent = $request->get($_key);
//        } else {
//            $_key = time();
//            $_userAgent = $_key;
//        }
//
//        $_url = $request->url();
//
//        if (strpos($_url, 'uat') !== false) {
//            $_form_url = 'https://agent_api.mintheinkha.com/saipayh5/demo/request';
//
//        } else {
//            $_form_url = Config::get('constants.saisai.h5.form_url');
//        }
//
//        $_post_data = [
//            'mtk_agent' => Config::get('constants.saisai.h5.mtk.agent'),
//            'mtk_agent_code' => Config::get('constants.saisai.h5.mtk.agent_code'),
//            'mtk_agent_secret' => Config::get('constants.saisai.h5.mtk.agent_secret'),
//            'user_agent' => $_userAgent
//        ];
//
//
//
//        $client = new Client([
//            'headers' => ['Content-Type' => 'application/json']
//        ]);
//        $response = $client->post($_form_url,
//            ['form_params' => $_post_data]
//        );
//
//        $_resp_json = json_decode($response->getBody(), true);
//
//        return redirect($_resp_json['mtk_agent_form']);
//
//    }

    public function index(Request $request)
    {

        $_request = $request->getContent();
        $_request_arr = json_decode($_request, true);

//        $_amount = $_request_arr['amount'];
//        $_app = $_request_arr['app'];
        $_app = env('APP_MTK');
        $_bp_trans_id = new TransID();
        $_bp_trans_id = $_bp_trans_id->generatedID($_app);

        $_configData = $this->_baseConfig->getConfig($_app, 'mytel', true);

        $_dateTime = date('Y-m-d H:i:s');

        $data = [
            "amount" => 500,
            "cancelUrlCallback" => "http://localhost:9999/cancel-url",
            "dateTime" => $_dateTime,
            "extTransRefId" => "extTransRefId-20200304-160800",
            "failUrlCallback" => "http://localhost:9999/fail-url",
            "orderDescription" => "order-description-2020-String",
            "orderId" => "orderId-2020-String",
            "cashback" => 0,
            "discount" => 0,
            "partnerCode" => $_configData['partnerCode'],
            "partnerId" => $_configData['partnerId'],
            "phoneNumber" => "09684693443",
            "remark" => "string remark",
            "serviceCode" => $_configData['serviceCode'],
            "shoppingItemList" => json_encode([
                "itemDescription" => "string",
                "price" => 500,
                "quantity" => 1,
                "shoppingItemId" => "string"
            ]),
            "successUrlCallback" => "http://localhost:9999/success-url",
            "walletPaymentStatusCallbackUrl" => "http://localhost:9999/wallet-payment-status-url"
        ];


//        $token = new Token();
//
//// Standard claims are supported
//        $token->addClaim(new Claim\Audience(['audience_1', 'audience_2']));
//        $token->addClaim(new Claim\Expiration(new \DateTime('10 minutes')));
//        $token->addClaim(new Claim\IssuedAt(new \DateTime('now')));
//        $token->addClaim(new Claim\Issuer('mytelpay'));
//        $token->addClaim(new Claim\JwtId($_configData['partnerId']));
////        $token->addClaim(new Claim\NotBefore(new \DateTime('now')));
//        $token->addClaim(new Claim\Subject(json_encode($data)));
//        $jwt = new Jwt\Jwt();
//
//        $algorithm = new Algorithm\Hs256($_configData['secretKey']);
//        $encryption = \Emarref\Jwt\Encryption\Factory::create($algorithm);
//        $serializedToken = $jwt->serialize($token, $encryption);
//        return redirect($_configData['request_url'] . $serializedToken);

        $key = $_configData['secretKey'];
        $payload = array(
            "jti" => $_configData['partnerId'],
            "iat" => time(),
            "sub" => json_encode($data),
            "iss" => "mytelpay",
            'exp' => strtotime('+10 minutes', time())
        );


        return $jwt = JWT::encode($payload, $key);
        $decoded = (array)JWT::decode($jwt, $key, array('HS256'));

        return redirect($_configData['request_url'] . urlencode($jwt));
//
//
//        $config = Configuration::forSymmetricSigner(
//            new Sha256(),
//            new Key($_configData['secretKey'])
//        );
//
//        assert($config instanceof Configuration);
//
//        $now = new DateTimeImmutable();
//
//        $token = $config->createBuilder()
//            // Configures the issuer (iss claim)
//            ->issuedBy('mytelpay')
//            ->identifiedBy($_configData['partnerId'])
////            ->identifiedBy('4f1g23a12aa')
//            ->issuedAt($now)
//            ->expiresAt($now->modify('+10 minute'))
//            ->relatedTo(json_encode($data))
//            ->getToken($config->getSigner(), $config->getSigningKey());
//
//        return redirect($_configData['request_url'] . $token);
    }
}



