<?php

namespace App\Http\Controllers\SaiSaiPay;

use App\Repository\Config\BaseConfigInterface;
use App\Repository\SaiSai\ServiceInterface;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TokenController extends Controller
{
    protected $_service, $_config;

    public function __construct(ServiceInterface $service, BaseConfigInterface $baseConfig)
    {
        $this->_config = $baseConfig;
        $this->_service = $service;
    }

    /** Login
     * @param $_app
     */
    public function loginCredential($_app, $_h5)
    {
        $_app_upper = $this->_config->changeAppName($_app)['app_upper'];

        if (!$_h5)
            $_app_env = env($_app_upper . '_SAISAI_ENV');
        else
            $_app_env = env($_app_upper . '_SAISAI_H5_ENV');


        return $this->_service->getLoginConfig($_app_env, $_app,$_h5);
    }

    /** Merchant Login
     * @param $_app
     * @param $_h5
     * @return mixed
     */
    public function merchanLogin($_app, $_h5)
    {
        $_credential = $this->loginCredential($_app, $_h5);

        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);

        $_url = $_credential['url'];
        unset($_credential['url']);

        $response = $client->post($_url,
            ['form_params' => $_credential]
        );

        $response = $response->getBody();

        return json_decode($response, true)['Token'];
    }
}
