<?php

namespace App\Http\Controllers\SaiSaiPay;

use App\Model\AppCallBackLog;
use App\Model\SaiSai\CallBack;
use App\Repository\Config\BaseConfigInterface;
use App\Repository\SaiSai\ServiceInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Config;

class CallBackController extends Controller
{
    protected $_service, $_baseConfig, $_payment, $_callBackUpdateUrl;

    public function __construct(ServiceInterface $service, BaseConfigInterface $baseConfig)
    {
        $this->_service = $service;
        $this->_baseConfig = $baseConfig;
        $this->_payment = 'saisai';
        $this->_callBackUpdateUrl = env('APP_URL') . 'api/saisaipay/callback/update';
    }

    public function index(Request $request)
    {
        \DB::select('insert into callbacks(data,pay,time)  values (?,?,?)', [json_encode($request->all()), 'saisai', date('Y-m-d H:i:s')]);

        $_callback_arr = $request->all();

        $_request_id = $_callback_arr['InvoiceNo'];
        $_pay_data = $this->selectApp($_request_id);


        $_app = $_pay_data['app'];
        $_h5 = $_pay_data['h5'] == 1;
        $_trans_id = $_pay_data['trans_id'];

        CallBack::create([
            'data' => json_encode($_callback_arr),
            'amount' => $_callback_arr['Amount'],
            'referIntegrationId' => $_callback_arr['ReferIntegrationId'],
            'invoiceNo' => $_request_id,
            'transactionStatus' => $_callback_arr['TransactionStatus'],
            'transactionId' => $_callback_arr['TransactionID'],
            'hashValue' => $_callback_arr['TransactionStatus'],
            'app' => $_app
        ]);

        $_callback_arr['app'] = $_app;
        $_callback_arr['h5'] = $_h5;
        $_callback_arr['trans_id'] = $_trans_id;
        $_callback_arr['bp_trans_id'] = $_pay_data['bp_trans_id'];
        $_callback_arr['amount'] = $_pay_data['amount'];

        $curl_command = "curl --data '" . http_build_query($_callback_arr) . "' -X POST " . $this->_callBackUpdateUrl . " > /dev/null 2>/dev/null &";


        exec($curl_command);

        $_json_string = array(
            'ItemId' => $_request_id,
            'Quantity' => (string)1,
            'EachPrice' => $_callback_arr['Amount']
        );


        $_resp_arr['ReferIntegrationId'] = $_callback_arr['ReferIntegrationId'];
        $_resp_arr['DataType'] = 'Data';
        $_resp_arr['ItemListJsonStr'] = json_encode($_json_string, JSON_FORCE_OBJECT);
        $_resp_arr['RespDescription'] = 'Success';
        $_resp_arr['RespCode'] = $_callback_arr['TransactionStatus'];
        $_resp_arr['HashValue'] = $_callback_arr['TransactionStatus'];

        return $_resp_arr;

    }

    /** Select App
     * @param $_request_id
     * @return array
     */
    public function selectApp($_request_id)
    {
        $_result = DB::select("select app,trans_id,h5,bp_trans_id,amount from saisai_requests where bp_trans_id=?", [$_request_id])[0];

        return [
            'app' => $_result->app,
            'trans_id' => $_result->trans_id,
            'h5' => $_result->h5,
            'bp_trans_id' => $_result->bp_trans_id,
            'amount' => $_result->amount
        ];
    }

    /** Update Temp
     * @param $_temp
     * @param $_trans_id
     * @param $_status
     */
    public function updateTemp($_temp, $_trans_id, $_status)
    {

        if ($_status === '000') {
            $_pay_status = 1;
        } else {
            $_pay_status = 0;
        }

        $_data = $_temp::where('bp_trans_id', $_trans_id)->first();
        $_data->transaction_status = $_status;
        $_data->pay_status = $_pay_status;
        $_data->save();
    }


    /** Update CallBack
     * @param Request $request
     */
    public function updateCallBack(Request $request)
    {

        $_request_arr = $request->all();

        $_app = $_request_arr['app'];
        $_h5 = $_request_arr['h5'];
        $_request_id = $_request_arr['InvoiceNo'];

        $_temp = $this->_service->getTemp($_app, $_h5);

        $this->updateTemp($_temp, $_request_id, $_request_arr['TransactionStatus']);

        $_status = $_request_arr['TransactionStatus'] === '000' ? 1 : 0;
        $_message = $_request_arr['TransactionStatus'] === '000' ? 'success' : 'fail';

        $_call_back_url = $this->_baseConfig->getCallBackUrlUpdate($_app);

        $_redirect_json_arr['amount'] = $_request_arr['amount'];
        $_redirect_json_arr['trans_id'] = $_request_arr['trans_id'];
        $_redirect_json_arr['payment_method'] = $this->_payment;
        $_redirect_json_arr['status'] = $_status;
        $_redirect_json_arr['message'] = $_message;
        $_redirect_json_arr['bp_trans_id'] = $_request_arr['bp_trans_id'];

//        $this->_baseConfig->createCallBackLog($_redirect_json_arr, $_app, $this->_payment);

//        $this->_baseConfig->curlFunc($_call_back_url, $_redirect_json_arr);


        switch ($_app) {
            case env('APP_MTK'):
                $this->_baseConfig->createCallBackLog($_redirect_json_arr, $_app, $this->_payment);

                $this->_baseConfig->callToApp($_call_back_url, $_redirect_json_arr);
                break;

            case env('APP_SERIES'):

                $_get_str = http_build_query($_redirect_json_arr);


                $curl_command = "curl --data '" . $_get_str . "' -X POST " . $_call_back_url . " > /dev/null 2>/dev/null &";
                exec($curl_command);

                break;
        }


    }

}
