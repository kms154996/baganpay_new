<?php

namespace App\Http\Controllers\SaiSaiPay;

use App\Model\RequestLog\SaiSaipayRequestLog;
use App\Repository\Config\BaseConfigInterface;
use App\Repository\SaiSai\ServiceInterface;
use App\TransID;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\helperTrait;
use Response;

class IndexController extends TokenController
{
    use helperTrait;

    /** Check Phone
     * @param Request $request
     */
    public function checkPhone(Request $request)
    {
        $_requestData = $request->getContent();
        $_request_arr = json_decode($_requestData, true);

        if (isset($_request_arr['h5'])) {
            $_request_arr['h5'] = 1;
            $_h5 = true;
        } else {
            $_request_arr['h5'] = 0;
            $_h5 = false;
        }

        $_auth_token = $this->merchanLogin($_request_arr['app'], $_h5);

        $_config_data = $this->_config->getConfig($_request_arr['app'], $_request_arr['payment_method'], false);

        $_check_phone_data = $this->_service->getCheckPhoneConfig($_config_data, $_request_arr['phone']);

        return $this->_service->callUab($_check_phone_data, $_auth_token, $_config_data['check_phone']);

    }

    /** Start Pay
     * @param Request $request
     */
    public function startPay(Request $request)
    {

        $_requestData = $request->getContent();
        $_request_arr = json_decode($_requestData, true);

        $_bp_trans = new TransID();
        $_bp_trans_id = $_bp_trans->generatedID($_request_arr['app']);


        $_trans_id = $this->my_decrypt($_request_arr['trans_id']);

        $_request_arr['bp_trans_id'] = $_bp_trans_id;
        $_request_arr['trans_id'] = $_trans_id;

        if (isset($_request_arr['h5'])) {
            $_request_arr['h5'] = 1;
            $_h5 = true;
        } else {
            $_request_arr['h5'] = 0;
            $_h5 = false;
        }

        $_temp = $this->_service->getTemp($_request_arr['app'], $_h5);

//        dd($_temp);

        $_auth_token = $this->merchanLogin($_request_arr['app'], $_h5);


        $_store_request_arr = $_request_arr;
        unset($_store_request_arr['payment_method']);

//        \App\Model\SaiSai\Request::create($_store_request_arr);

        $_config_data = $this->_config->getConfig($_request_arr['app'], $_request_arr['payment_method'], $_h5);
//dd($_config_data);
        $_start_pay_data = $this->_service->getPayConfig($_config_data, $_request_arr);

        $_checkPrepay = $this->_config->checkPrepay($_temp, $_request_arr['trans_id'], 'pay_status', 1);

        if ($_checkPrepay > 0) {
            return Response::json([
                'status' => 0,
                'message' => 'Prepay'
            ]);
        }


        $this->_service->storeTemp($_request_arr, $_temp);


        $_req_log = SaiSaipayRequestLog::create([
            'app' => $_request_arr['app'],
            'trans_id' => $_trans_id,
            'bp_trans_id' => $_bp_trans_id,
            'raw_request' => json_encode($_start_pay_data)
        ]);

        $_noti_resp = $this->_service->callUab($_start_pay_data, $_auth_token, $_config_data['payment_enquiry_url']);

        $_req_log->raw_response = $_noti_resp;
        $_req_log->save();

        $_noti_resp_arr = json_decode($_noti_resp, true);


        return $this->_service->updateNtoiResp($_noti_resp_arr, $_request_arr, $_temp);

    }


}
