<?php

namespace App\Http\Controllers\OkDollar;

use App\Repository\Config\BaseConfigInterface;
use App\Repository\OkDollar\ServiceInterFace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    protected $_config, $_service;

    public function __construct(BaseConfigInterface $baseConfig, ServiceInterFace $serviceInterFace)
    {
        $this->_config = $baseConfig;
        $this->_service = $serviceInterFace;
    }

    public function index(Request $request)
    {
        $_requestData = $request->getContent();
        $_request_arr = json_decode($_requestData, true);

        return $this->_config->getConfig($_request_arr['app'],$_request_arr['payment_method']);

    }
}
