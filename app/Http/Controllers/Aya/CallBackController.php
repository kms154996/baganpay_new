<?php

namespace App\Http\Controllers\Aya;


use App\Http\Controllers\Controller;
use App\Model\Aya\AyaMtkRedirect;
use App\Model\Aya\AyaMtkTemp;
use App\Repository\AYA\AyaServiceInterface;
use DB;
use Illuminate\Http\Request;
use Config;

class CallBackController extends Controller
{
    protected $_service;

    public function __construct(AyaServiceInterface $ayaService)
    {
        $this->_service = $ayaService;
    }

    public function index(Request $request)
    {
        $_send_data = json_encode($request->all());

        $_send_data_arr = json_decode($_send_data, true);

        $_decode_json = openssl_decrypt($_send_data_arr['paymentResult'], 'AES-256-ECB', 'OESUhLOvw9pSfDsAYCcbrJ5xfvqybrIP');

        $_decode_arr = json_decode($_decode_json, true);

        $_db_result = DB::select('select trans_id,app from aya_requests where bp_trans_id = ?', [$_decode_arr['externalTransactionId']])[0];

        $_trans_id = $_db_result->trans_id;

        $_app = $_db_result->app;

        /** Model */
        $_model = $this->_service->getTemp($_app);

        /** Insert Redirect Table */
        $this->insertRedirect($_model, $_decode_arr, $_send_data);

        /** Update Temp */
        $this->updateTemp($_model, $_decode_arr);

        $_transaction_status = $_decode_arr['status'];

        $_redirect_json_arr['amount'] = (int)$_decode_arr['amount'];
        $_redirect_json_arr['trans_id'] = $_trans_id;
        $_redirect_json_arr['payment_method'] = 'aya_pay';
        $_redirect_json_arr['status'] = $this->checkTransStatus($_transaction_status) === 1 ? 1 : 0;
        $_redirect_json_arr['message'] = $this->checkTransStatus($_transaction_status) === 1 ? 'success' : 'fail';

        $_get_str = http_build_query($_redirect_json_arr);

        $_call_back_url = $_model['callback_url'];

        $curl_command = "curl --data '" . $_get_str . "' -X POST " . $_call_back_url . " > /dev/null 2>/dev/null &";
        exec($curl_command);

    }

    /** Update Temp
     * @param $_model
     * @param $_arr
     */
    private function updateTemp($_model, $_arr)
    {

        $_pay_status = $this->checkTransStatus($_arr['status']);

        $_model['temp']::updateOrCreate(
            [
                'bp_trans_id' => $_arr['externalTransactionId']
            ],
            [
                'amount' => $_arr['amount'],
                'pay_status' => $_pay_status,
                'pay_desc' => $_arr['status'],
                'transRefId' => $_arr['transRefId'],
                'customer_name' => $_arr['customer']['name']
            ]
        );
    }

    /** Insert Redirect */
    private function insertRedirect($_model, $_arr, $_send_data)
    {
        $_model['redirect']::create([
            'raw' => $_send_data,
            'trans_id' => $_arr['externalTransactionId'],
            'phone' => $_arr['customer']['phone'],
            'referenceNumber' => $_arr['referenceNumber'],
            'fees' => json_encode($_arr['fees']),
            'customer_name' => $_arr['customer']['name'],
            'created_time' => $_arr['createdAt'],
            'timetick' => time()
        ]);
    }

    /** Check Trans Status */
    private function checkTransStatus($_status)
    {
        if ($_status === 'done') {
            $_pay_status = 1;
        } else {
            $_pay_status = 0;
        }

        return $_pay_status;
    }


}
