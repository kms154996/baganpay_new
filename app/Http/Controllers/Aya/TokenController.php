<?php

namespace App\Http\Controllers\Aya;

use App\Payment\Aya;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\ayaTrait;
use Illuminate\Routing\ResponseFactory;

class TokenController extends Controller
{
    use ayaTrait;

    /**
     * Generate Access Token
     */
    public function getAccessToken($_key, $_secret, $_url)
    {

        $_auth_token = base64_encode($_key . ':' . $_secret);


        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");

        $headers = array();
        $headers[] = 'Authorization: Basic ' . $_auth_token;
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);


        $_result = json_decode($result);

        return $_result->access_token;
    }

    /**
     * Login User
     */
    public function loginUser($_token, $_login_url)
    {

        $_phone = '095107278';
        $ch = curl_init();

        $_send_data['phone'] = $_phone;
        $_send_data['password'] = "100914";


        curl_setopt($ch, CURLOPT_URL, $_login_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_send_data));

        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Token: Bearer ' . $_token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

        curl_close($ch);


        $_result = json_decode($result, true);

        return $_result['token']['token'];


    }

    /** Start Push Payment */
    public function pushPayment($_user_token, $_access_token, $_json, $_url)
    {


        $ch = curl_init();

//        $_send_data['amount'] = (int)$_amount;
//        $_send_data['externalTransactionId'] = $_trans_id;
//        $_send_data['externalAdditionalData'] = 'Trans';
//        $_send_data['currency'] = 'MMK';
//        $_send_data['customerPhone'] = $_phone;

        curl_setopt($ch, CURLOPT_URL, $_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $_json);

        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer ' . $_user_token;
        $headers[] = 'Token: Bearer ' . $_access_token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;

    }
}
