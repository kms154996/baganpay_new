<?php

namespace App\Http\Controllers\Aya;

use App\Model\Aya\AyaMtkTemp;
use App\Model\Aya\AyaRequest;
use App\Model\RequestLog\AyaRequestLog;
use App\Payment\Aya;
use App\Repository\AYA\AyaServiceInterface;
use App\Repository\Config\BaseConfigInterface;
use App\Traits\helperTrait;
use App\TransID;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Config;
use Response;

class IndexController extends TokenController
{
    use helperTrait;

    protected $_service, $_baseConfig;

    public function __construct(AyaServiceInterface $ayaService, BaseConfigInterface $baseConfig)
    {
        $this->_service = $ayaService;
        $this->_baseConfig = $baseConfig;
    }

    public function startPay(Request $request): array
    {
        $_requestData = $request->getContent();
        $_request_arr = json_decode($_requestData, true);

        list($_app, $_amount, $_trans_id, $_phone) = $this->expandData($_request_arr);

        $_trans_id = $this->my_decrypt($_trans_id);
        $_phone = '0' . substr($_phone, 2);

        $_request_arr['trans_id'] = $_trans_id;

        list($_generate_trans_id, $_request_arr) = $this->getBpId($_request_arr);

        if (isset($_request_arr['payment_method'])) {
            unset($_request_arr['payment_method']);
        }


        $_temp = $this->_service->getTemp($_app)['temp'];
        $_app_short = $this->_service->getTemp($_app)['app_short'];

        /** Store Request  */
        $this->storeRequest($_request_arr);

        $_prepay_status = $this->checkPrePay($_temp, $_trans_id)['prepay'];

        if ($_prepay_status === 1) {

            $_return['status'] = 0;
            $_return['message'] = config::get('constants.fail_msg.aya');

            return $_return;

        }


//        dd($this->_baseConfig->getConfig($_app, 'aya', $this->_baseConfig->checkH5($_request_arr)));
//        dd($this->getConfig($_app_short));
        /** Get Config */
//        list($_key, $_secret, $_login_url, $_token_url, $_push_payment_url) = $this->getConfig($_app_short);

        ['key' => $_key, 'secret' => $_secret, 'login_url' => $_login_url, 'token_url' => $_token_url, 'push_payment_url' => $_push_payment_url] = $this->_baseConfig->getConfig($_app, 'aya', $this->_baseConfig->checkH5($_request_arr));

        $_access_token = $this->getAccessToken($_key, $_secret, $_token_url);

        sleep(3);

        $_user_token = $this->loginUser($_access_token, $_login_url);

        sleep(3);

        unset($_request_arr['app']);

        $_temp::create($_request_arr);

        $_send_data['amount'] = (int)$_amount;
        $_send_data['externalTransactionId'] = $_generate_trans_id;
        $_send_data['externalAdditionalData'] = 'Trans';
        $_send_data['currency'] = 'MMK';
        $_send_data['customerPhone'] = $_phone;

        $_send_data_json = json_encode($_send_data);


        $_request_log = AyaRequestLog::create([
            'trans_id' => $_trans_id,
            'bp_trans_id' => $_generate_trans_id,
            'app' => $_app,
            'raw_request' => $_send_data_json
        ]);

        $_resp_json = $this->pushPayment($_user_token, $_access_token, $_send_data_json, $_push_payment_url);

        $_resp_arr = json_decode($_resp_json, true);

        $_request_log->raw_response = $_resp_json;
        $_request_log->save();

        $_status_code = $_resp_arr['err'];

        $this->_service->updateNoti($_temp, $_generate_trans_id, $_status_code);

        if ($_status_code === 9008 || $_status_code === 1) {

            return Response::json([
                'status' => 0,
                'message' => Config::get('constants.fail_msg.aya_no_phone')
            ]);

        }


        return $this->responseData($_status_code);

    }

    /** Create Requests
     * @param $_request_arr
     */
    private function storeRequest($_request_arr)
    {
        unset($_request_arr['phone']);
        AyaRequest::create($_request_arr);
    }

    /**
     * @param $_app
     * @return array
     */
    private function getConfig($_app): array
    {
        $_aya_data = new Aya();
        $_config_data = $_aya_data->getConfig($_app);

        [
            'key' => $_key,
            'secret' => $_secret,
            'login_url' => $_login_url,
            'token_url' => $_token_url,
            'push_payment_url' => $_push_payment_url
        ] = $_config_data;

        return array($_key, $_secret, $_login_url, $_token_url, $_push_payment_url);
    }


    /**
     * @param $_request_arr
     * @return array
     */
    private function expandData($_request_arr): array
    {
        $_app = $_request_arr['app'];
        $_amount = $_request_arr['amount'];
        $_trans_id = $_request_arr['trans_id'];
        $_phone = $_request_arr['phone'];
        return array($_app, $_amount, $_trans_id, $_phone);
    }

    /** Response Data
     * @param $_resp_code
     * @return
     */
    private function responseData($_resp_code)
    {
        if ($_resp_code === 200) {

            return Response::json([
                'status' => 1,
                'message' => config::get('constants.success_msg.aya')
            ]);

        } else {
            return Response::json([
                'status' => 0,
                'message' => config::get('constants.fail_msg.aya')
            ]);
        }
    }

    /** Check Prepay Status  */
    private function checkPrePay($_temp_model, $_trans_id)
    {

        $_prepay = $_temp_model::where('trans_id', $_trans_id)->where('pay_status', 1)->count();

        if ($_prepay === 1) {
            return array('prepay' => 1);
        } else {
            return array('prepay' => 0);
        }

    }

    /**
     * @param $_request_arr
     * @return array
     */
    private function getBpId($_request_arr): array
    {
        $_trans_class = new TransID();
        $_generate_trans_id = $_trans_class->generatedID($_request_arr['app']);
        $_request_arr['bp_trans_id'] = $_generate_trans_id;
        return array($_generate_trans_id, $_request_arr);
    }
}
