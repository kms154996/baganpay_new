<?php

namespace App\Http\Controllers\Onepay;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Config;

class H5Controller extends Controller
{
    public function index(Request $request)
    {
        if (count($request->all()) > 0) {
            $_key = array_keys($request->all())[0];

        } else {
            $_key = time();
        }

        $_url = $request->url();

        if (strpos($_url, 'uat') !== false) {
            $_form_url = 'https://agent_api.mintheinkha.com/onepayh5/demo/request';

        } else {
            $_form_url = Config::get('constants.saisai.h5.form_url');
        }

        $_post_data = [
            'mtk_agent' => Config::get('constants.onepay.h5.mtk.agent'),
            'mtk_agent_code' => Config::get('constants.onepay.h5.mtk.agent_code'),
            'mtk_agent_secret' => Config::get('constants.onepay.h5.mtk.agent_secret'),
            'user_agent' => $_key
        ];

//dd($_form_url);
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);
        $response = $client->post($_form_url,
            ['form_params' => $_post_data]
        );

        $_resp_json = json_decode($response->getBody(), true);

        return redirect($_resp_json['mtk_agent_form']);

    }
}
