<?php

namespace App\Http\Controllers\Onepay;

use App\Model\Onepay\SeriesTemp;
use App\Model\Onepay\WunzinnTemp;
use App\Model\RequestLog\OnepayRequestLog;
use App\Payment\OnePay;
use App\Repository\One\PayServiceInterface;
use App\TransID;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\onepayTrait;
use App\Model\Onepay\OnePayRequest;
use App\Model\Onepay\OnePay_mtk_Temp;
use Response;
use Config;


class OnePayController extends Controller
{
    use onepayTrait;

    protected $_service;

    public function __construct(PayServiceInterface $service)
    {
        $this->_service = $service;
    }

    /********* Verify Phone ********
     * @param Request $request
     * @return mixed
     */
    public function verifyPhone(Request $request)
    {

        $_send_data = json_encode($request->all());

        $_send_data_array = json_decode($_send_data, true);

        $_config_data = $this->configData($_send_data_array['app']);

        $_hash_arr['Channel'] = $_config_data['channel'];
        $_hash_arr['MerchantUserId'] = $_config_data['user_id'];
        $_hash_arr['OnepayPhoneNo'] = $_send_data_array['phone'];
        $_hash_arr['secret_key'] = $_config_data['secret_key'];

        $_hash_code = $this->verifyPhoneHash($_hash_arr);


        unset($_hash_arr['secret_key']);

        $_hash_arr['HashValue'] = $_hash_code;

        $_data['url'] = $_config_data['verify_phone_url'];
        $_data['data'] = $_hash_arr;

        return $_result = $this->callOnePay($_data);


        $_result = $this->callOnePay($_data);

        return json_decode($_result, true);

    }

    /*
     * Start One Pay Payment
     */
    public function directPay(Request $request)
    {
        $_checkPhone = $this->verifyPhone($request);

        $_checkPhoneArr = json_decode($_checkPhone, true);

        if ($_checkPhoneArr['RespCode'] !== '000') {
            return Response::json([
                'status' => 0,
                'message' => 'သင္၏ ဖုန္းနံပါတ္ျဖင့္ Onepay account ဖြင့္ထားျခင္း မရွိပါ။'
            ]);
        }

        $_send_data = json_encode($request->all());

        $_send_data_array = json_decode($_send_data, true);

        $_app = $_send_data_array['app'];

        unset($_send_data_array['payment_method']);

        $_trans_id = $this->my_decrypt($_send_data_array['trans_id']);

        $_send_data_array['trans_id'] = $_trans_id;

        $_temp = $this->_service->selectTemp($_app);

        $_send_data_array['timetick'] = time();

        OnePayRequest::create($_send_data_array);

        $_trans_class = new TransID();
        $_generate_trans_id = $_trans_class->generatedID($_app);

        $_prepay_status = $this->checkPrePay($_temp, $_trans_id)['prepay'];

        if ($_prepay_status === 1) {

            $_return['status'] = 0;
            $_return['message'] = config::get('constants.fail_msg.wave');

            return $_return;

        }

        $_send_data_array['bp_id'] = $_generate_trans_id;
        $_send_data_array['timetick'] = time();

        unset($_send_data_array['app']);

        $_temp::create($_send_data_array);

        unset($_send_data_array['timetick']);

        $_data['config_data'] = $this->configData($_app);
        $_data['request_data'] = $_send_data_array;

        $_data['data'] = $this->requestArr($_data);
        $_data['url'] = $_data['config_data']['direct_payment_url'];

        $_req_log = OnepayRequestLog::create([
            'trans_id' => $_trans_id,
            'bp_trans_id' => $_generate_trans_id,
            'app' => $_app,
            'raw_request' => json_encode($_data['data'])
        ]);

        $_resp_result = $this->callOnePay($_data);

        $_req_log->raw_response = $_resp_result;
        $_req_log->save();


        return $this->updateNotiStatus($_temp, $_generate_trans_id, $_resp_result);

    }


    /******** Get Config Data ****/
    public function configData($_app)
    {
        $_one_pay = new OnePay();
        return $_one_pay->configData($_app);
    }


    /* Check Prepay Status  */
    private function checkPrePay($_temp_model, $_trans_id)
    {

        $_prepay = $_temp_model::where('trans_id', $_trans_id)->where('status', 1)->count();

        if ($_prepay === 1) {
            return array('prepay' => 1);
        } else {
            return array('prepay' => 0);
        }

    }

    /*
     * Update Noti Status
     */
    private function updateNotiStatus($_temp, $_generate_trans_id, $_resp_result)
    {
        $_resp_arr = json_decode($_resp_result, true);


        ['ReferIntegrationId' => $_integration_id, 'RespCode' => $_resp_code] = $_resp_arr;


        $_temp::updateOrCreate(
            ['bp_id' => $_generate_trans_id],
            [
                'integration_id' => $_integration_id,
                'noti_status' => $_resp_code
            ]
        );

        if ($_resp_code === '000') {

            return Response::json([
                'status' => 1,
                'message' => config::get('constants.success_msg.one')
            ]);

        } else {
            return Response::json([
                'status' => 0,
                'message' => config::get('constants.fail_msg.one')
            ]);
        }

    }
}
