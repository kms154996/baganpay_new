<?php

namespace App\Http\Controllers\Onepay;

use App\Repository\One\PayServiceInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Config;

class CallBackController extends Controller
{
    protected $_service;

    public function __construct(PayServiceInterface $service)
    {
        $this->_service = $service;
    }

    public function index(Request $request)
    {
        $_send_data = json_encode($request->all());

        $_send_data_arr = json_decode($_send_data, true);

        $_integration_id = $_send_data_arr['ReferIntegrationId'];
        $_amount = $_send_data_arr['Amount'];
        $_trans_id = $_send_data_arr['InvoiceNo'];
        $_transaction_id = $_send_data_arr['TransactionID'];
        $_transaction_status = $_send_data_arr['TransactionStatus'];
        $_has_value = $_send_data_arr['HashValue'];

        if ($_transaction_status === '012' || $_transaction_status === '013' || $_transaction_status === '014') {

            $_transaction_status = '000';
            return $this->OnePayResp($_integration_id, $_transaction_id, $_amount, $_has_value, $_transaction_status);

        }

        $_model = $this->_service->getModel($_trans_id);

        $_model['redirect']::create([
            'trans_id' => $_trans_id,
            'integration_id' => $_integration_id,
            'transaction_id' => $_transaction_id,
            'transaction_status' => $_transaction_status,
            'amount' => $_amount
        ]);

//        ->where('integration_id', $_integration_id)

        $_temp_data = $_model['temp']::where('trans_id', $_trans_id)->orderBy('id', 'desc')->limit(1)->first();

        $_temp_data->transaction_status = $_transaction_status;
        $_temp_data->status = $this->checkTransStatus($_transaction_status);
        $_temp_data->save();

        $_redirect_json_arr['amount'] = (int)$_amount;
        $_redirect_json_arr['trans_id'] = $_trans_id;
        $_redirect_json_arr['payment_method'] = 'onepay';
        $_redirect_json_arr['status'] = $this->checkTransStatus($_transaction_status) === 1 ? 1 : 0;
        $_redirect_json_arr['message'] = $this->checkTransStatus($_transaction_status) === 1 ? 'success' : 'fail';

        $_get_str = http_build_query($_redirect_json_arr);

        $_call_back_url = $this->_service->getCallBackUrl($_trans_id);

        $curl_command = "curl --data '" . $_get_str . "' -X POST " . $_call_back_url . " > /dev/null 2>/dev/null &";
        exec($curl_command);

        return $this->OnePayResp($_integration_id, $_transaction_id, $_amount, $_send_data_arr, $_transaction_status);

    }


    /*
     * Check Trans Status
     */
    private function checkTransStatus($_status)
    {
        if ($_status === '000') {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     *
     * Get Call Back URL
     * @param $_trans_id
     * @return
     */
    private function getCallBackUrl($_trans_id)
    {
        /** @var TYPE_NAME $_sub_trans_id */
        $_sub_trans_id = substr($_trans_id, 0, 3);

        if ($_sub_trans_id === 'WUZ') {

            $_url = Config::get('constants.callback_url.WUZ');

        } elseif ($_sub_trans_id === 'MTK') {


            $_url = Config::get('constants.callback_url.MTK');

        }

        return $_url;
    }

    /** Return to OnePay
     * @param $_integration_id
     * @param $_transaction_id
     * @param $_amount
     * @param $_send_data_arr
     * @param $_transaction_status
     * @return mixed
     */
    public function OnePayResp($_integration_id, $_transaction_id, $_amount, $_send_data_arr, $_transaction_status)
    {

        $_json_string = array(
            'ItemId' => $_transaction_id,
            'Quantity' => (string)1,
            'EachPrice' => $_amount
        );


        $_resp_arr['ReferIntegrationId'] = $_integration_id;
        $_resp_arr['DataType'] = 'Data';
        $_resp_arr['ItemListJsonStr'] = json_encode($_json_string, JSON_FORCE_OBJECT);
        $_resp_arr['RespDescription'] = 'Success';
        $_resp_arr['RespCode'] = $_transaction_status;
        $_resp_arr['HashValue'] = $_send_data_arr;

        return $_resp_arr;


    }

    function convert_multi_array($array)
    {
        array_walk_recursive($array, function ($v) use (&$result) {
            $result[] = $v;
        });
        return implode('&', $result);
    }

}
