<?php

namespace App\Http\Controllers\CbPay;

use App\Model\CbPay\CbRequest;
use App\Repository\CB\Service;
use App\Repository\Config\BaseConfigInterface;
use App\TransID;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\helperTrait;
use Response;
use Config;

class IndexController extends Controller
{
    use helperTrait;

    protected $_baseConfig, $_service;

    public function __construct(BaseConfigInterface $baseConfig, Service $_service)
    {
        $this->_baseConfig = $baseConfig;
        $this->_service = $_service;
    }

    public function requestData(Request $request)
    {
        $_requestData = $request->getContent();
        $_request_arr = json_decode($_requestData, true);

        $_encrypt_trans_id = $_request_arr['trans_id'];


        $_payment_method = $_request_arr['payment_method'];
        $_app = $_request_arr['app'];

        $_bp_trans = new TransID();
        $_bp_trans_id = $_bp_trans->generatedID($_request_arr['app']);

        $_h5 = $this->_baseConfig->checkH5($_request_arr);

        $_configData = $this->_baseConfig->getConfig($_app, $_payment_method, $_h5);

        $_request_arr['bp_trans_id'] = $_bp_trans_id;

        $_temp = $this->_service->getTemp($_app);

        $_request_arr['trans_id'] = $this->my_decrypt($_request_arr['trans_id']);

        $this->_service->storeRequest($_request_arr);

        $this->_service->storeTemp($_temp, $_request_arr);

        $_sing = $this->_service->sign($_configData, $_request_arr);

        $_requestData = $this->_service->requestArr($_configData, $_request_arr, $_sing);

        $_respData = $this->_service->reqeustCB($_configData['request_url'] . 'orderpayment-api/v1/request-payment-order.service', $_requestData);

        $_respArr = json_decode($_respData, true);

        switch ($_configData['env']) {
            case 'PROD':
                $_package = '';
                $_scheme = 'cb';
                $_url = 'cb://pay?keyreference=';
                break;
            case 'DEV':
                $_package = 'com.cbbank.cbmbanking.uat';
                $_scheme = 'cbuat';
                $_url = 'cbuat://pay?keyreference=';
        }

        $this->_service->updateTemp($_temp, $_bp_trans_id, $_respArr);

        CbRequest::updateOrCreate(
            [
                'bp_trans_id' => $_bp_trans_id
            ],
            [
                'generateRefOrder' => $_respArr['generateRefOrder']
            ]
        );

        $_url = $_url . $_respArr['generateRefOrder'];

//        return Response::json([
//            'url'=>$_url. $_respArr['generateRefOrder'],
//            'status' => 1,
//            'message' => 'Success'
//        ]);


        return Response::json([
            'url' => env('APP_URL') . 'cb?data=' . $_respArr['generateRefOrder'] . '&scheme=' . $_scheme . '&package=' . $_package . '&app=' . $_app . '&trans_id=' . $_encrypt_trans_id . '&amount=' . $_request_arr['amount'] . '&url=' . $_url,
            'status' => 1,
            'message' => 'Success'
        ]);

//        return env('APP_URL') . 'cb?data=' . $_respArr['generateRefOrder'] . '&scheme=' . $_scheme . '&package=' . $_package;

    }


    public function ui(Request $request)
    {
//        dd($request->all());
        /*    Get APP From Config   */
        $_apps = Config::get('constants.app_view_config');

        $_app = $request->get('app');

        ['text' => $data['app'], 'image_url' => $data['image_url']] = $_apps[$_app];

        $data['trans_id'] = $this->my_decrypt($request->get('trans_id'));

//        switch ($_app) {
//            case env('APP_MTK'):
//                $data['url'] = 'intent://cbpay.com/openPWA?appid=kpae0c44ac8da14a3f980bc0ee363da8&token=8dd7a9030103cbee38792a7036c5f8e9#Intent;scheme=' . $request->get('scheme') . ';package=' . $request->get('package') . ';S.appid=kpae0c44ac8da14a3f980bc0ee363da8;S.token=8dd7a9030103cbee38792a7036c5f8e9;S.browser_fallback_url=http%3A%2F%2Fwww.pay?keyreference=' . $request->get('data') . '&app=;end';
//                break;
//            case env('APP_SERIES'):
//                $data['url'] = "intent://pay?keyreference=" . $request->get('data') . "#Intent;scheme=cbuat;package=com.cbbank.cbmbanking.uat;S.keyreference=fb8c50d7-890e-4848-a02f-ef6e625d16cd;S.browser_fallback_url=http%3A%2F%2Fwww.cbbank.com.mm;end";;
//                break;
//        }

        $data['url'] = $request->get('url');
        $data['scheme'] = $request->get('scheme');
        $data['data'] = $request->get('data');
        $data['amount'] = $request->get('amount');
        $data['app'] = $_app;
//        dd($data);
        return view('cbpay.index', $data);
    }
}


//intent://pay?keyreference=fb8c50d7-890e-4848-a02f-ef6e625d16cd#Intent;scheme=cbuat;package=com.cbbank.cbmbanking.uat;S.keyreference=fb8c50d7-890e-4848-a02f-ef6e625d16cd;S.browser_fallback_url=http%3A%2F%2Fwww.cbbank.com.mm;end

//https://www.kbzpay.com/en/#Intent;scheme=cbuat;package=com.cbbank.cbmbanking.uat;S.appid=kpae0c44ac8da14a3f980bc0ee363da8;S.token=8dd7a9030103cbee38792a7036c5f8e9;S.browser_fallback_url=http%3A%2F%2Fwww.pay?keyreference=6bd49249-6436-430a-b6ae-c078a14fd1af

//intent://kbzpay.com/openPWA?appid=kpae0c44ac8da14a3f980bc0ee363da8&token=8dd7a9030103cbee38792a7036c5f8e9#Intent;scheme=cbuat;package=com.cbbank.cbmbanking.uat;S.appid=kpae0c44ac8da14a3f980bc0ee363da8;S.token=8dd7a9030103cbee38792a7036c5f8e9;S.browser_fallback_url=http%3A%2F%2Fwww.pay?keyreference=6bd49249-6436-430a-b6ae-c078a14fd1af


//intent://kbzpay.com/openPWA?appid=kpae0c44ac8da14a3f980bc0ee363da8&token=8dd7a9030103cbee38792a7036c5f8e9#Intent;scheme=cbuat;package=com.cbbank.cbmbanking.uat;S.appid=kpae0c44ac8da14a3f980bc0ee363da8;S.token=8dd7a9030103cbee38792a7036c5f8e9;S.browser_fallback_url=http%3A%2F%2Fwww.pay?keyreference=6bd49249-6436-430a-b6ae-c078a14fd1af;end
