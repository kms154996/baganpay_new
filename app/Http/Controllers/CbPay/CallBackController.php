<?php

namespace App\Http\Controllers\CbPay;

use App\Model\CbPay\CbCallBack;
use App\Repository\CB\ServiceInterface;
use App\Repository\Config\BaseConfigInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Config;
use Response;

class CallBackController extends Controller
{
    protected $_service, $_baseConfig, $_callBackUpdateUrl, $_payment;

    public function __construct(ServiceInterface $service, BaseConfigInterface $baseConfig)
    {
        $this->_service = $service;
        $this->_baseConfig = $baseConfig;
        $this->_payment = 'cb';
        $this->_callBackUpdateUrl = env('APP_URL') . 'api/cbpay/callback/update';

    }

    public function index(Request $request)
    {

        $_responseData = $request->getContent();

        $_responseArr = json_decode($_responseData, true);

        ['app' => $_app, 'trans_id' => $_trans_id, 'bp_trans_id' => $_bp_trans_id, 'amount' => $_amount] = (array)$this->_service->getApp($_responseArr['generateRefOrder']);


        $_responseArr['app'] = $_app;
        $_responseArr['bp_trans_id'] = $_bp_trans_id;
        $_responseArr['trans_id'] = $_trans_id;
        $_responseArr['amount'] = $_amount;


        CbCallBack::create([
            'generateRefOrder' => $_responseArr['generateRefOrder'],
            'rawData' => $_responseData,
            'status' => $_responseArr['transactionStatus'],
            'app' => $_app,
            'bp_trans_id' => $_bp_trans_id,
            'trans_id' => $_trans_id
        ]);

        $curl_command = "curl --data '" . http_build_query($_responseArr) . "' -X POST " . $this->_callBackUpdateUrl . " > /dev/null 2>/dev/null &";

        exec($curl_command);


        return Response::json([
            'responseCode' => '000',
            'responseMsg' => 'Operation Success'
        ]);


    }

    /** Update Callback
     * @param Request $request
     */
    public function updateCallBack(Request $request)
    {

        $_app = $request->get('app');
        $_bp_trans_id = $request->get('bp_trans_id');
        $_trans_id = $request->get('trans_id');
        $_amount = $request->get('amount');
        $_transactionStatus = $request->get('transactionStatus');

        $_temp = $this->_service->getTemp($_app);

        $this->_service->updateTempCallback($_temp, $_bp_trans_id, $_transactionStatus);

        $_pay_status = $_transactionStatus === 'S' ? 1 : 0;
        $_pay_message = $_transactionStatus === 'S' ? 'success' : 'fail';


        $_redirect_json_arr['amount'] = $_amount;
        $_redirect_json_arr['trans_id'] = $_trans_id;
        $_redirect_json_arr['payment_method'] = $this->_payment;
        $_redirect_json_arr['status'] = $_pay_status;
        $_redirect_json_arr['message'] = $_pay_message;
        $_redirect_json_arr['bp_trans_id'] = $_bp_trans_id;

        $_call_back_url = $this->_baseConfig->getCallBackUrlUpdate($_app);

        $this->_baseConfig->createCallBackLog($_redirect_json_arr, $_app, $this->_payment);

//        $this->_baseConfig->callToApp($_call_back_url, $_redirect_json_arr);

        $this->_baseConfig->curlFunc($_call_back_url, $_redirect_json_arr);


//        $_url = $this->_baseConfig->getCallBackUrl($_app);
//
//        $this->_baseConfig->callBackToApp($_amount, $_trans_id, $_pay_status, $_pay_message, $this->_pay_method, $_url);

    }
}
