<?php

namespace App\Http\Controllers;

use App\Model\PaymentRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Wave\WaveController;
use App\Payment\Wave;
use Config;
use App\Traits\helperTrait;
use Illuminate\Routing\ResponseFactory, Response;
use DB;


class PaymentController extends Controller
{
    use helperTrait;

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function startPayment(Request $request)
    {

        $_payment_method = $request->get('payment_method');

        $_payment_confirm_message = Config::get('constants.payment_confirm_message.' . $_payment_method);

        $request['payment_confirm_message'] = $_payment_confirm_message;

        switch ($_payment_method) {

            case 'kbz':

                $_query_data = http_build_query($request->except('_token'));

                return ResponseFactory::successResp(
                    [
                        'url' => url('/confirm?') . $_query_data,
                        'data' => $request->except('_token'),
                        'confirm' => true
                    ]);

                break;

            case 'wave' || 'one':

                /********** Check Operator ***********/

                $_check_ph = $this->checkOperator($request->get('phone'));

                $_check_operator = $_check_ph['operator_name'];

                /********** Wrong Phone Number ******/
                if ($_check_operator === 0) {
                    return ResponseFactory::errorResp(
                        [
                            'error_msg' => 'Wrong Number',
                            'confirm' => false
                        ]);
                }

                $request['phone'] = $_check_ph['phone_number'];

                $_query_data = http_build_query($request->except('_token'));


                switch ($_payment_method) {

//                    case 'wave': # Must Be Telenor
//                        if ($_check_operator !== 5) {
//                            return ResponseFactory::errorResp(
//                                [
//                                    'error_msg' => 'Phone Must Be Telenor',
//                                    'confirm' => false
//                                ]);
//                        }
//                        break;

                    case 'one':

                        /******** Verify Phone One Pay ******/
                        $_result = app()->call('App\Http\Controllers\Onepay\OnePayController@verifyPhone', [$request->all()]);
//                        dd($_result);
                        ['RespCode' => $_resp_code, 'RespDescription' => $_description] = $_result;

                        if ($_resp_code !== '000') {
                            return ResponseFactory::errorResp(
                                [
                                    'error_msg' => $_description,
                                    'confirm' => false
                                ]);
                        }


                        break;
                }

                break;
            default:
        }

        return ResponseFactory::successResp(
            [
                'url' => url('/confirm?') . $_query_data,
                'data' => $request->except('_token'),
                'confirm' => true
            ]);

    }

    /********* Confirm Payment ****
     * @param Request $request
     * @return mixed
     */
    public function comfirmPayment(Request $request)
    {

        $_payment_method = $request->get('payment_method');
        switch ($_payment_method) {
            case 'wave':
                return app()->call('App\Http\Controllers\Wave\WaveController@index', [$request->all()]);
                break;
            case 'kbz':

                if ($request->has('v2')) {

                    return app()->call('App\Http\Controllers\KBZ\AppController@preCreate', [$request->all()]);
                }


                 $_result = app()->call('App\Http\Controllers\KBZ\WebController@preCreate', [$request->all()]);
                $_result_content = $_result->getContent();
                $_result_arr = json_decode($_result_content, true);

                $_data['url'] = $_result_arr['url'];
                $_data['status'] = $_result_arr['status'];

                return Response::json($_data);

                break;
            case 'one':
                return app()->call('App\Http\Controllers\Onepay\OnePayController@directPay', [$request->all()]);
                break;
            case 'aya':
                return app()->call('App\Http\Controllers\Aya\IndexController@startPay', [$request->all()]);
                break;
            case 'saisai':
                return app()->call('App\Http\Controllers\SaiSaiPay\IndexController@startPay', [$request->all()]);
                break;
            case 'cb':

                return app()->call('App\Http\Controllers\CbPay\IndexController@requestData', [$request->all()]);
                break;
            case 'mytel':
                return app()->call('App\Http\Controllers\MyTel\IndexController@index', [$request->all()]);
                break;
            case 'wave2':
                return app()->call('App\Http\Controllers\Wave\Wave2Controller@requestWave', [$request->all()]);
            case 'mptmoney':
                return app()->call('App\Http\Controllers\MptMoney\IndexController@index', [$request->all()]);
        }
    }

    /************ Cancel Payment ********
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function cancelPayment(Request $request)
    {
        if ($request->has('info')) {
            $_trans_id = $this->my_decrypt($request->get('info'));
        }

        $_payment_request = PaymentRequest::where('trans_id', $_trans_id)->first();

        $_app = $_payment_request->app;

        $_redirect_url = env('APP_URL') . '/payment/' . $_app . '?info=' . $request->get('info');

        return redirect($_redirect_url);

    }

    /**
     * @param $_result
     * @param $_data
     * @return mixed
     */
    private function kbz_static($_result)
    {
        $_result_json = $_result->getContent();
        $_result_arr = json_decode($_result_json, true);
        ['code' => $_code] = $_result_arr['data'];
        ['url' => $_url, 'status' => $_status] = $_result_arr;


        /** @var TYPE_NAME $_status */
        if ($_code === '0' && !ctype_space($_url) && 1 === $_status) {
            $_data['_url'] = $_url;
            $_data['_show_btn'] = true;
        } else {
            $_data['_url'] = '';
            $_data['_show_btn'] = false;
        }
        return $_data;
    }

    /******** Kbz Return Url ******
     * @param Request $request
     */
    public function successKbz(Request $request)
    {

        $_trans_id = $request->get('merch_order_id');


        $_app = DB::select('select app from kbz_requests where trans_id=?', [$_trans_id])[0]->app;

        switch ($_app) {
            case 'MINTHEINKHA':
                $_url = 'mintheinkha://home/home';
                $_btn_text = ' GO TO APPLICATION';
                break;

            case 'SERIES':
                //$_url = 'https://www.wunzinn.com/book-series?type=1';
                //$_url = 'vipseries://home?type=vipseries';
                $_url = 'https://series.wunzinn.com/vipseries/home';
                $_btn_text = 'GO Back To Mobile Application';
                break;

            case env('APP_SNS'):
                $_url='http://shwenarsin.com/myaccount';
                $_btn_text='Go Back To App';
        }

        $_data['trans_id'] = $_trans_id;
        $_data['_url'] = $_url;
        $_data['_btn_text'] = $_btn_text;

        return view('pay.extra.kbz_success', $_data);
    }

}
