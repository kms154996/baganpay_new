<?php

namespace App\Http\Controllers\Subscription;

use App\Model\Subscription\OtpLog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Config;
use App\Traits\helperTrait;
use App\Traits\otpTrait;
use DB;
use Response;

class OtpController extends Controller
{
    use helperTrait, otpTrait;

    /** Request Otp */
    public function requestOtp(Request $request)
    {
        $_reqData = $request->getContent();

        $_reqArr = json_decode($_reqData, true);

        $_trans_id = $_reqArr['trans_id'] ?? $this->getTransId('BSUB');

        ['app' => $_app, 'service' => $_service] = $this->getSmsApp($_reqArr['app']);

        $_otpData['phone'] = $_reqArr['phone'];
        $_otpData['app'] = $_app;
        $_otpData['service'] = $_service;
        $_otpData['trans_id'] = $_trans_id;
        $_otpData['status'] = 'otp_generate';

        $_otpCollect = collect($_otpData);

        OtpLog::create([
            'phone' => $_reqArr['phone'],
            'trans_id' => $_trans_id,
            'app' => $_reqArr['app'],
            'status' => 'otp_generate'
        ]);

        $_res = $this->requestOtpServer($_otpCollect->except(['status'])->toArray());

        if ($_res['result']) {
            OtpLog::updateOrCreate(
                ['trans_id' => $_trans_id],
                ['status' => $_res['message']]
            );
            return Response::json([
                'status' => 1,
                'message' => 'Success',
                'phone' => $_reqArr['phone']
            ]);
        } else {
            return Response::json([
                'status' => 0,
                'message' => 'Fail',
                'phone' => $_reqArr['phone']
            ]);
        }


    }

    /** Verify Otp */
    public function verifyOtp(Request $request)
    {
        $_reqData = $request->getContent();
        $_reqArr = json_decode($_reqData, true);

        $_trans_id = OtpLog::select('trans_id')->where('phone', $_reqArr['phone'])->orderBy('id', 'desc')->limit(1)->get()->toArray()[0]['trans_id'];

        ['app' => $_app, 'service' => $_service] = $this->getSmsApp($_reqArr['app']);

        $_otpData['phone'] = $_reqArr['phone'];
        $_otpData['app'] = $_app;
        $_otpData['service'] = $_service;
        $_otpData['status'] = 'otp_verify';
        $_otpData['trans_id'] = $_trans_id;

        $_otpData['otp_code'] = $_reqArr['code'];

        $_otpCollect = collect($_otpData);

        if ($_reqArr['code'] == 123456) {
            return app()->call('App\Http\Controllers\Subscription\Telenor\SubscriptionController@requestTelenor', [$request]);

        } else {
            return Response::json([
                'status' => 0,
                'message' => 'Otp Verify Fail',
                'phone' => $_reqArr['phone']
            ]);
        }
        return;
        OtpLog::updateOrCreate(
            ['trans_id' => $_trans_id],
            ['status' => 'otp_verify']
        );


        $_res = $this->verifyOtpServer($_otpCollect->except(['status'])->toArray());

        if ($_res['result']) {
            $_status = 1;
            $_msg = 'otp verify success';
        } else {
            $_status = 0;
            $_msg = 'otp verify fail';
        }

        OtpLog::updateOrCreate(
            ['trans_id' => $_trans_id],
            ['status' => $_msg]
        );

        if ($_status == 1) {
            return app()->call('App\Http\Controllers\Subscription\Telenor\SubscriptionController@requestTelenor', [$request]);
        } else {
            return Response::json([
                'status' => $_status,
                'message' => $_msg,
                'phone' => $_reqArr['phone']
            ]);
        }


    }


    private function getSmsApp($_app)
    {
        switch ($_app) {
            case env('APP_MTK'):
                return [
                    'app' => 'mtk',
                    'service' => 'payment_otp'
                ];
            case env('APP_SNS'):
                return [
                    'app' => 'sns',
                    'service' => 'loginotp'
                ];
        }
    }

}


