<?php

namespace App\Http\Controllers\Subscription\Telenor;

use App\Repository\Subscription\BaseConfigInterFace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscriptionController extends Controller
{
    protected $_tokenController, $_baseConfig;

    public function __construct(BaseConfigInterFace $baseConfigInterFace)
    {

        $this->_tokenController = new TokenController();
        $this->_baseConfig = $baseConfigInterFace;
    }

    public function requestTelenor(Request $request)
    {
        return $this->telenorAction($request, 'subscribe');
    }

    public function unsubscribeTelenor(Request $request)
    {
        return $this->telenorAction($request, 'unsubscribe');
    }

    public function telenorAction(Request $request, string $action)
    {
        $_req = $request->getContent();
        $_reqArr = json_decode($_req, true);
        $_app = $_reqArr['app'];
        $_productCode = $_reqArr['product_code'];
        $_transId = $_reqArr['trans_id'] ?? time();

        $_configData = $this->_baseConfig->getConfigData($_app, env('TELENOR'));

        /** Config Data For Token */
        ['clientId' => $_clientId, 'clientSecret' => $_clientSecret, 'access_token_url' => $_token_url, 'request_url' => $_request_url, 'env' => $_env
        ] = $_configData;

        switch ($_env) {
            case env('PROD'):
                $_phone = substr($_reqArr['phone'], 2);
                break;
            case env('DEV'):
                $_phone = "9790305105";
                break;
        }

        /** Get Tables */
        ['temp' => $_tempTable, 'log' => $_logTable] = $this->_baseConfig->getTables($_app);

        /** @var From TokenController $_accessToken */
        $_accessToken = $this->_tokenController->accessToken([
            'client_id' => $_clientId,
            'client_secret' => $_clientSecret,
            'access_token_url' => $_token_url,
            'app' => $_app
        ]);

        /** Config Data for Request */
        ['CP_ID' => $_cpID, 'CP_NAME' => $_cpName, 'USERNAME' => $_userName, 'PASSWORD' => $_password] = $_configData;

        /** @var Request Data for Telenor $_requestData */
        $_requestData = $this->getRequestData($_cpID, $_transId, $_userName, $_password, $_phone, $_productCode, $action);

        $_reqHeader = $this->getReqHeader($_accessToken);

        $_temp = $this->createTemp($_tempTable, $_phone, $_transId, $_productCode, $_requestData, $action);

        $_raw_resp = $this->getDataUpdate($_request_url, $_requestData, $_reqHeader);

        $_resp['phone'] = $_reqArr['phone'];

        if (isset($_raw_resp['status'])) {

            $_raw_resp['message'] = ucfirst($action) . ' Success';
            $_temp->reason = $_raw_resp['status'];
            $_raw_resp['status'] = 1;

            $_resp['status'] = 1;
            $_resp['message'] = 'Success';

        } else {
            $_raw_resp['status'] = 0;
            $_temp->reason = $_raw_resp['message'];

            if ($_raw_resp['code'] == '500.023.102') {
                $_resp['status'] = 2;
            } else {
                $_resp['status'] = 0;

            }

            $_resp['message'] = $_raw_resp['message'];
        }

        $_temp->raw_response = json_encode($_raw_resp);
        $_temp->save();

        return $_resp;
    }


    public function getDataUpdate($_url, $_data, $_headers = null)
    {
        $data = json_encode($_data);

        $cu = curl_init();

        curl_setopt_array($cu, array(
            CURLOPT_URL => $_url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_RETURNTRANSFER => true
        ));

        curl_setopt($cu, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data),
            $_headers
        ));
        $_res = curl_exec($cu);

        $response = json_decode($_res, true);

        curl_close($cu);
        return $response;
    }

    /**
     * @param $_cpID
     * @param  $_transId
     * @param $_userName
     * @param $_password
     * @param  $_phone
     * @param  $_productCode
     * @param  $_action
     * @return array
     */
    public function getRequestData($_cpID, $_transId, $_userName, $_password, $_phone, $_productCode, $_action): array
    {
        return [
            "cpID" => (string)$_cpID,
            "clientTransactionId" => (string)$_transId,
            "loginName" => $_userName,
            "password" => $_password,
            "id" => [
                "type" => "MSISDN",
                "value" => $_phone
            ],
            "action" => $_action,
            "productCode" => $_productCode
        ];
    }

    /**
     * @param $_tempTable
     * @param string $_phone
     * @param  $_transId
     * @param $_productCode
     * @param array $_requestData
     * @return mixed
     */
    public function createTemp($_tempTable, $_phone, $_transId, $_productCode, array $_requestData, $action)
    {
        return $_tempTable::create([
            'msisdn' => $_phone,
            'clientTransactionId' => $_transId,
            'productCode' => $_productCode,
            'raw_request' => json_encode($_requestData),
            'action' => $action
        ]);
    }

    /**
     * @param $_accessToken
     * @return string
     */
    public function getReqHeader($_accessToken): string
    {
        return "Authorization: Bearer " . $_accessToken;
    }
}
