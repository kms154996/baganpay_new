<?php

namespace App\Http\Controllers\Subscription\Telenor;

use App\Model\Subscription\Telenor\AccessToken;
use App\Repository\Subscription\BaseConfigInterFace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class TokenController extends Controller
{
    public function accessToken($_arr)
    {
        ['client_id' => $_clientId, 'client_secret' => $_clientSecret, 'access_token_url' => $_url, 'app' => $_app] = $_arr;

        $_tokenQuery = "SELECT * FROM access_tokens WHERE app = ? ";

        $_tokenResult = DB::select($_tokenQuery, [$_app]);


        if (count($_tokenResult) != 0) {

            $datetime1 = strtotime($_tokenResult[0]->updated_at);
            $datetime2 = strtotime(date('Y-m-d H:i:s'));
            $interval = abs($datetime2 - $datetime1);
            $minutes = round($interval / 60);

            if ($minutes <= 10.0) {
                return $_token = $_tokenResult[0]->token;
            } else {
                $_token = $this->get_accesstoken($_clientId, $_clientSecret, $_url);

                AccessToken::updateOrCreate(
                    ['app' => $_app],
                    ['token' => $_token]
                );
            }

        } else {

            $_token = $this->get_accesstoken($_clientId, $_clientSecret, $_url);

            AccessToken::updateOrCreate(
                ['app' => $_app],
                ['token' => $_token]
            );

        }

        return $_token;

    }

    function get_accesstoken($_clientId, $_clientSecret, $_url)
    {

        $header = array();

        $header[] = 'Content-Type:application/x-www-form-urlencoded';

        $ch = curl_init($_url);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "client_id=" . $_clientId . "&client_secret=" . $_clientSecret . "&grant_type=client_credentials");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response_json = curl_exec($ch);

        curl_close($ch);

        $response_arr = json_decode($response_json, true);

        if ($response_arr['status'] == "approved") {
            $accesstoken = $response_arr['accessToken'];

            return $accesstoken;
        } else {
            return null;
        }


    }

}
