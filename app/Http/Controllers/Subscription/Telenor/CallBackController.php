<?php

namespace App\Http\Controllers\Subscription\Telenor;

use App\Repository\Subscription\BaseConfigInterFace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Config;
use Response;

class CallBackController extends Controller
{
    protected $_baseConfig;

    public function __construct(BaseConfigInterFace $baseConfigInterFace)
    {
        $this->_baseConfig = $baseConfigInterFace;
    }

    public function callBack(Request $request)
    {

        ['productCode' => $_productCode, 'clientTransactionId' => $_trans_id, 'chargingType' => $_chargingType, 'billingId' => $_billingId, 'lifeCycle' => $_lifeCycle] = $request->all();

        $_app = $this->getApp($_productCode);

        DB::select('insert into callbacks (content,app,created_at) values (?,?,?)', [json_encode($request->all()),$_app,date('Y-m-d H:i:s')]);


        ['temp' => $_tempTable, 'log' => $_logTable] = $this->_baseConfig->getTables($_app);

        if ($_chargingType == 'ACT' || $_chargingType == 'DCT') {
            $_temp = $_tempTable::where('clientTransactionId', $_trans_id)->first();
            $_temp->chargingType = $_chargingType;
            $_temp->billingId = $_billingId;
            $_temp->lifeCycle = $_lifeCycle;
            $_temp->save();
        }

        $_logTable::create(collect($request->all())->except('Lang')->toArray());

        $_call_back_data_str = http_build_query($request->all());

        $_call_back_url = $this->getCallBackUrl($_app);

        \Log::info($_call_back_data_str);

        $curl_command = "curl --data '" . $_call_back_data_str . "' -X POST " . $_call_back_url . " > /dev/null 2>/dev/null &";

        exec($curl_command);

        return Response::json([
            'status' => "Success"
        ]);
    }

    private function getApp($_proCode)
    {
        return Config::get('subscription.appByProduct.' . $_proCode);
    }

    private function getCallbackUrl($_app)
    {
        switch ($_app) {
            case env('APP_MTK');
                return Config::get('subscription.callback.' . $_app);
        }
    }
}
