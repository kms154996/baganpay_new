<?php

namespace App\Http\Controllers;

use App\Model\PaymentRequest;
use App\Traits\helperTrait;
use Illuminate\Http\Request;
use Config;
use DB;
use Response;

class UiController extends Controller
{
    use helperTrait;

    /********* Create Index Ui *******/
    public function getUi(Request $request, $_app)
    {

        /*    Get APP From Config   */
        $_apps = config::get('constants.app_view_config');

        /*     Check Request APP Exist */
        if (!isset($_apps[$_app])) {
            return view('pay.error');
        }

        $_info = $request->get('info');

        /*  Decrypt Trans Id */
        $_trans_id = $this->my_decrypt($_info);

        if (!$_trans_id) {
            return view('pay.error');
        }

        $_app_data = PaymentRequest::select('app', 'amount')->where('trans_id', $_trans_id)->get()[0];

        $_amount = $_app_data->amount;

        $_payment = DB::select('select payment from app_payment where app_name = ?', [$_app])[0]->payment;

        $_payment_arr = json_decode($_payment, true);

        if ($request->has('dev')) {

            $_payment_arr_[]='one';

        } else {

            foreach ($_payment_arr as $_key => $_value) {
                if ($_value === 1) {
                    $_tmp[] = $_key;
                }
            }

            $_payment_arr_ = $_tmp;

        }

        ['text' => $_app_, 'image_url' => $_image_url] = $_apps[$_app];

        $_data['payment_method'] = $_payment_arr_;
        $_data['app_name'] = $_app_;
        $_data['app'] = $_app_data->app;
        $_data['image_url'] = $_image_url;
        $_data['amount'] = $_amount;
        $_data['trans_id'] = $_info;

        return view('pay.index', $_data);

    }

    /********* Submit Data *****
     * @param Request $request
     * @param $_payment
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function postData(Request $request, $_payment)
    {
        $_payment_method = $request->get('payment_method');
        $data['app'] = $request->get('app');
        $data['payment_method'] = $_payment;
        $data['trans_id'] = $request->get('trans_id');
        $data['amount'] = $request->get('amount');


        switch ($_payment) {
            case 'kbz':
                $_payment_confirm_message = Config::get('constants.payment_confirm_message.' . $_payment_method);
                $data['payment_confirm_message'] = $_payment_confirm_message;
                return view('pay.confirm', $data);
                break;
            default:
                return view('pay.partial.index', $data);

        }


    }


    /********* Confirm Ui *******
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public
    function comfirmUi(Request $request)
    {

        foreach ($request->all() as $_key => $_value) {
            $_data[$_key] = $_value;
        }
//        dd($_data);
        return view('pay.confirm', $_data);
    }
}
