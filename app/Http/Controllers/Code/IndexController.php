<?php

namespace App\Http\Controllers\Code;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class IndexController extends BaseController
{
    public function index(Request $request)
    {
        $_send_data = json_encode($request->all());

        $_send_data_array = json_decode($_send_data, true);

        if ($this->checkData($_send_data_array)) {

            $_result = $this->getCode($_send_data_array['app']);
            
            $_res = $this->formatCode($_result);
            return $this->returnCode('Return Credit Codes', $_res);

        }

    }

    /*
     * Get Credit Code
     */
    private function getCode($_app)
    {
        $_query = "SELECT credit_codes.text,credit_codes.amount,credit_codes.bp_id
                            FROM credit_codes
                                JOIN payments on payments.id=credit_codes.payment_id
                            where payments.name=?
                            and credit_codes.active = 1";

        $_app = strtoupper($_app);

        return DB::select($_query, [$_app]);

    }

    /*
     * Format Credit Code
     */
    private function formatCode($_result)
    {
        foreach ($_result as $_re) {
            $_tmp['text'] = $_re->text . ' - ' . $_re->amount;
            $_tmp['bp_code'] = $_re->bp_id;
            $_tmp['amount'] = $_re->amount;
            $_res[] = $_tmp;
            unset($_tmp);
        }
        /** @var TYPE_NAME $_res */
        return $_res;
    }


}
