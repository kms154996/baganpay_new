<?php

namespace App\Http\Controllers\Code;

use App\Exceptions\Code\BasicAuth;
use App\Exceptions\Code\SignCheck;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Config;

class BaseController extends Controller
{
    /**
     * This function check Sign String
     * @param $_array
     * @return string
     */
    public function sign_check($_array)
    {
        $_app = $_array['app'];

        $_array['credential'] = $this->getAppCredential($_app);

        $_sing_generate = $this->generate_sing($_array);

        $_request_sing = $_array['sign'];

        if (strcmp($_sing_generate, $_request_sing) === 0) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * This function check ID
     * @param $_array
     * @return bool|false
     */
    public function appIdCheck($_array)
    {
        $_app_id = $_array['app_id'];

        $_array['credential'] = $this->getAppCredential($_array['app']);

        $_app_credential_app = $_array['credential']['app_id'];

        if (strcmp($_app_credential_app, $_app_id) === 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * This function generate Sing Key
     * @param $_array
     */
    private function generate_sing($_array)
    {
        $_nonce_str = strtoupper($_array['nonce_str']);

        $_app_credential_app = $_array['credential']['app_id'];
        $_app_credential_key = $_array['credential']['key'];

        $_sing_str = 'appid=' . $_app_credential_app . '&nonce_str=' . $_nonce_str . '&key=' . $_app_credential_key;

        return strtoupper(hash('sha256', $_sing_str));
    }

    /**
     * This function return Credential For App
     * @param $_app
     * @return mixed
     */
    private function getAppCredential($_app)
    {
        return Config::get('constants.code_credential.' . $_app);
    }


    /** Check Sig And Header Auth
     * @param $_send_data_array
     * @return bool
     * @throws SignCheck
     */
    public function checkData($_send_data_array)
    {
        $_app_id_check = $this->appIdCheck($_send_data_array);
        $_sign_check = $this->sign_check($_send_data_array);

        if ($_app_id_check && $_sign_check) {
            return true;
        } else {

            if (!$_app_id_check) {
                throw new SignCheck('Check App Id');
            }

            if (!$_sign_check) {
                throw new SignCheck('Check Sign key');

            }

        }

    }

    /*
     * Return Code
     */
    public function returnCode($_message, $_result)
    {
        $_data['status'] = 1;
        $_data['respDescription'] = 'Success';
        $_data['message'] = $_message;
        $_data['data'] = $_result;

        return \Response::json($_data);
    }

}

//797AD69DB8BA0C7CE7CCDE298E42B03C5C9FCE8C4BAB8E3929B4F2811D8F9FA6

//DA1A5ACD8ADF8F4F6EC2AD77CA152CFB2161AEE3B9314842BA79A748F9DCB0E7
