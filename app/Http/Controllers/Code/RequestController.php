<?php

namespace App\Http\Controllers\Code;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Config;
use App\Model\CreditCode\CreditCodeLog;
use Response;

class RequestController extends BaseController
{
    /** Request Code Sample
     * @param Request $request
     * @return
     * @throws \App\Exceptions\Code\SignCheck
     */
    public function requestCode(Request $request)
    {
        $_send_data = json_encode($request->all());

        $_send_data_array = json_decode($_send_data, true);

        $length = 15;
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $nonce_str = '';

        if ($this->checkData($_send_data_array)) {

            for ($i = 0; $i < $length; $i++) {
                $nonce_str .= $characters[rand(0, $charactersLength - 1)];
            }

            return $this->returnCode('Return Credit Code', $nonce_str);

        }

    }


}
