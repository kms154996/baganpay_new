<?php

namespace App\Http\Controllers\KBZ;

use App\Http\Requests\Kbz\WebRequest;
use App\Model\Kbz\kbz_mtk_temp;
use App\Model\Kbz\kbz_wunzinn_temp;
use App\Model\RequestLog\KbzRequestLog;
use App\Repository\Config\BaseConfigInterface;
use App\Repository\KBZ\KbzServiceInterface;
use App\Repository\KBZ\PAY\PayServiceInterface;
use App\Traits\helperTrait;
use App\Traits\kbzTrait;
use App\TransID;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Config;
use Response;

class AppController extends Controller
{


    use kbzTrait, helperTrait;

    protected $_kbzService, $_payService, $_baseConfig;

    public function __construct(KbzServiceInterface $_kbzService, PayServiceInterface $_payService, BaseConfigInterface $baseConfig)
    {
        $this->_baseConfig = $baseConfig;
        $this->_kbzService = $_kbzService;
        $this->_payService = $_payService;
    }

    public function preCreate(WebRequest $request)
    {


        $_send_data = json_encode($request->all());

        $_bp_trans_class = new TransID();

        $_send_data_array = json_decode($_send_data, true);

        $_send_data_array['trans_id'] = $this->my_decrypt($_send_data_array['trans_id']);

        $_app = $_send_data_array['app'];
        $_trans_id = $_send_data_array['trans_id'];
        $_amount = $_send_data_array['amount'];


        $_is_h5 = false;
        $_trade_type = 'app';

        //        $this->_kbzService->changeConfigDev($_app, $_is_h5);

        $_check_prepay = $this->checkPrePay($_trans_id, $_app)['prepay'];

        /* BP Trans ID */
        $_generate_trans_id = $_bp_trans_class->generatedID($_app);

        $_send_data_array['bp_trans_id'] = $_generate_trans_id;

        $_merch_order_id = $_trans_id;

        $this->createRequest($_send_data_array);

        $_request_ = $this->_payService->requestArr($_merch_order_id, $_amount, $_app, $_trade_type, $_is_h5);

        ['_request' => $_request_array, 'static_url' => $_static_url, 'precreate_url' => $_precreate_url] = $_request_;

        $_url = Config::get('constants.kbz.precreate__url');
        $_url = $_precreate_url;


        $_header = array();

        $_header[] = 'content-type:application/json';

        $_temp_model = $this->_kbzService->getModel($_app);

        $this->insertTemp($_temp_model, $_send_data_array);

        if ($_check_prepay === 1) {

            $_resp_json['url'] = '';
            $_resp_json['status'] = 0;
            $_resp_json['message'] = 'Fail';

            return Response::json($_resp_json);
        }


        $_req_log = KbzRequestLog::create([
            'app' => $_app,
            'trans_id' => $_trans_id,
            'bp_trans_id' => $_trans_id,
            'raw_request' => json_encode($_request_array)
        ]);

        /** @var TYPE_NAME $_precreate_resp */
        $_precreate_resp = $this->callCurl($_url, $_header, $_request_array);
        $_req_log->raw_response = $_precreate_resp;
        $_req_log->save();


        return $this->preCreateResp($_precreate_resp, $_app, $_send_data_array, $_is_h5);
    }


    /************* Return Precreate Resp *******
     * @param $_precreate_resp
     * @param $_temp_model
     * @return
     */
    public function preCreateResp($_precreate_resp, $_app, $_send_data_array, $_is_h5)
    {

        $_precreate_arr = json_decode($_precreate_resp, true)['Response'];

        list($_app_, $_temp_model) = $this->_kbzService->getAppData($_app);


        $_configData = $this->_baseConfig->getConfig($_app, 'kbz', $_is_h5);

        ['app_id' => $_appid, 'merch_code' => $_merch_code, 'static_url' => $_static_url, 'precreate__url' => $_precreate__url, 'key' => $_app_key] = $_configData;


        //        $_configData = $this->_baseConfig->getConfig($_app, 'kbz', false);

        ['app_id' => $_appid, 'merch_code' => $_merch_code, 'static_url' => $_static_url, 'precreate__url' => $_precreate__url, 'key' => $_app_key] = $_configData;


        $_prepay_status = $_precreate_arr['result'];


        if (array_key_exists('prepay_id', $_precreate_arr)) {

            $_nonce_str_resp = $_precreate_arr['nonce_str'];
            $_prepay_id = $_precreate_arr['prepay_id'];
            $_timestamp = time();
            //            $_app_key = Config::get('constants.kbz.vue.' . $_app_ . '.key');


            $_merch_order_id = $_precreate_arr['merch_order_id'];

            $OrderInfo = "appid=" . $_appid . "&merch_code=" . $_merch_code . "&nonce_str=" . $_nonce_str_resp . "&prepay_id=" . $_prepay_id . "&timestamp=" . $_timestamp . "";

            $sign_key = hash('sha256', 'appid=' . $_appid . '&merch_code=' . $_merch_code . '&nonce_str=' . $_nonce_str_resp . '&prepay_id=' . $_prepay_id . '&timestamp=' . $_timestamp . '&key=' . $_app_key);

            $_sign = strtoupper($sign_key);

            $_resp_json['prepay_id'] = $_prepay_id;
            $_resp_json['OrderInfo'] = $OrderInfo;
            $_resp_json['sign'] = $_sign;
            $_resp_json['status'] = 1;
            $_resp_json['message'] = 'သင္၏ေမးခြန္းကို လက္ခံရရွိပါသည္။ ေငြေပးေခ်မွု ျပဳလုပ္ရန္အတြက္ KBZPay mobile application တြင္ သင္၏ PIN နံပါတ္ကို ရိုက္ထည့္ျပီး ေငြေပးေခ်မွု ျပဳလုပ္ႏိုင္ပါသည္။';
            $_resp_json["merch_code"] = $_merch_code;
            $_resp_json["merch_order_id"] = $_merch_order_id;
        } else {
            $_prepay_id = '';
            $_merch_order_id = $_send_data_array['trans_id'];

            $_resp_json['prepay_id'] = '';
            $_resp_json['OrderInfo'] = '';
            $_resp_json['sign'] = '';
            $_resp_json['status'] = 0;
            $_resp_json['message'] = 'Fail';
            $_resp_json["merch_code"] = $_merch_code;
            $_resp_json["merch_order_id"] = $_merch_order_id;
        }

        $_update_ = $_temp_model::where('merch_order_id', $_merch_order_id)->first();
        $_update_->prepay_id = $_prepay_id;
        $_update_->prepay_status = $_prepay_status;
        $_update_->save();

        //        $_resp_json['data'] = $_precreate_arr;

        return Response::json($_resp_json);
    }


    /**
     * @param $_precreate_resp
     * break;
     * case env('APP_MTK'):
     * $_app_config = 'mtk';
     * break;
     * @param $_app
     * @return array
     */
    private function configData($_precreate_resp, $_app)
    {
        $_precreate_arr = json_decode($_precreate_resp, true)['Response'];

        list($_app_, $_temp_model) = $this->_kbzService->getAppData($_app);

        $_static_url = Config::get('constants.kbz.static_url');

        $_merch_code = Config::get('constants.kbz.' . $_app_ . '.merch_code');
        $_appid = Config::get('constants.kbz.' . $_app_ . '.app_id');
        $_app_key = Config::get('constants.kbz.' . $_app_ . '.key');
        return array($_precreate_arr, $_temp_model, $_static_url, $_merch_code, $_appid, $_app_key);
    }

    /* Check Prepay Status  */
    private function checkPrePay($_trans_id, $_app)
    {
        list($_app_, $_temp) = $this->_kbzService->getAppData($_app);

        $_prepay = $_temp::where('merch_order_id', $_trans_id)->where('status', 1)->count();

        if ($_prepay === 1) {
            ;
            return array('prepay' => 1);
        } else {
            return array('prepay' => 0);
        }
    }
}
