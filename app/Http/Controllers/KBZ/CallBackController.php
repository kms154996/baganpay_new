<?php

namespace App\Http\Controllers\KBZ;

use App\Model\Kbz\kbz_mtk_temp;
use App\Model\Kbz\kbz_wunzinn_temp;
use App\Model\Kbz\SeriesTemp;
use App\Repository\KBZ\KbzServiceInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use App\Model\Kbz\kbz_request;
use Config;
use DB;
use App\Model\Kbz\CallBack;

class CallBackController extends Controller
{

    protected $_kbzService;

    public function __construct(KbzServiceInterface $kbzService)
    {
        $this->_kbzService = $kbzService;
    }

    public function callBack(Request $request)
    {

        \Log::info(json_encode($request->all()));

//        \DB::select('insert into callbacks(data,pay,time)  values (?,?,?)', [json_encode($request->all()), 'kbz', date('Y-m-d H:i:s')]);

        $_call_back_data = $request->getContent();
        $_call_back_arr_ = json_decode($_call_back_data, true);
        $_call_back_arr = $_call_back_arr_['Request'];

        $_merch_code_id = $_call_back_arr['merch_order_id'];

        $_request_source = kbz_request::where('trans_id', $_merch_code_id)->get();

        if ($_request_source->count() === 0) {
            $_return_json['status'] = 0;
            $_return_json['message'] = 'Fail';
            return Response::json($_return_json);
        }

        $_request_source_app = $_request_source[0]->app;
        $_trade_status = $_call_back_arr['trade_status'];
        $_amount = $_request_source[0]->amount;
        $_trans_end_time = $_call_back_arr['trans_end_time'];

        if ($_trade_status === 'PAY_SUCCESS') {
            $_pay_status = 1;
            $_pay_message = 'Success';
        } else {
            $_pay_status = 0;
            $_pay_message = 'Fail';
        }

        $_update_arr['_trade_status'] = $_trade_status;
        $_update_arr['_pay_status'] = $_pay_status;
        $_update_arr['_merch_order_id'] = $_merch_code_id;
        $_update_arr['_app'] = $_request_source_app;

        $_send_back_data['trans_id'] = $_merch_code_id;
        $_send_back_data['status'] = $_pay_status;
        $_send_back_data['amount'] = $_amount;
        $_send_back_data['payment_method'] = 'kbz_pay';
        $_send_back_data['message'] = $_pay_message;

        switch ($_request_source_app) {
            case env('APP_MTK'):
                $_send_back_data['remark'] = $_call_back_arr['mm_order_id'];
                break;
        }

        $_call_back_data_str = http_build_query($_send_back_data);

         $_call_back_url = $this->_kbzService->getCallBackUrl($_update_arr['_app']);

        $curl_command = "curl --data '" . $_call_back_data_str . "' -X POST " . $_call_back_url . " > /dev/null 2>/dev/null &";

        $this->updateTemp($_update_arr);

        exec($curl_command);

        CallBack::create([
            'data' => $request->getContent(),
            'merch_order_id' => $_merch_code_id,
            'amount' => $_amount,
            'trade_status' => $_trade_status,
            'trans_end_time' => $_trans_end_time,
            'app' => $_request_source_app,
            'timetick' => time()
        ]);


        return "success";
    }

    /*********** Update Temp Status **********/
    public function updateTemp($_data)
    {
        $_app = $_data['_app'];
        $_temp_model = $this->_kbzService->getModel($_app);
        $_update_model = $_temp_model->where('merch_order_id', $_data['_merch_order_id'])->first();
        $_update_model->status = $_data['_pay_status'];
        $_update_model->trade_status = $_data['_trade_status'];
        $_update_model->save();
    }

    public function testCallback(Request $request)
    {
        \DB::select('insert into callbacks(data,pay,time)  values (?,?,?)', [json_encode($request->all()), 'kbztest', date('Y-m-d H:i:s')]);
        $ch_data = curl_init('https://pay_dev.baganit.com/api/kbz/callback');
        $json_str = json_encode($request->all());

//        curl_setopt($ch_data, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch_data, CURLOPT_POSTFIELDS, $json_str);
        # Return response instead of printing.
        curl_setopt($ch_data, CURLOPT_RETURNTRANSFER, true);
        # Send request.
        $response = curl_exec($ch_data);
        curl_close($ch_data);

        return 'success';

    }
}
