<?php

namespace App\Http\Controllers\KBZ\H5;

use App\Model\Kbz\H5\OpenIdMtk;
use App\Repository\KBZ\H5\H5ServiceInteFace;
use App\Repository\KBZ\KbzServiceInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Routing\ResponseFactory, Response;
use Config;

class IndexController extends Controller
{
    protected $h5Service, $kbzService;

    public function __construct(H5ServiceInteFace $h5ServiceInteFace, KbzServiceInterface $_kbzService)
    {
        $this->h5Service = $h5ServiceInteFace;
        $this->kbzService = $_kbzService;
    }

    public function index(Request $request)
    {
        
        $_url = $request->url();

        if (strpos($_url, 'uat') !== false) {
            $_kbz_url = 'http://api.kbzpay.com/web/gateway/uat/queryCustInfo';

        } else {
            $_kbz_url = Config::get('constants.kbz.h5.query_cust_info_url');
        }

        $_kbz_request_data = $this->h5Service->requestRequArr('APPH5', 'mtk', $request->get('kbzpay_token'));

        $_open_id_resp = $this->h5Service->callCurl($_kbz_url, $_kbz_request_data);


        $_format_resp = $this->h5Service->formatOpenIdResp($_open_id_resp);

        if (isset($_format_resp['Response']['customer_info'])) {
            $_custom_info = $_format_resp['Response']['customer_info'];
        }

        if (!isset($_custom_info)) {
            return $_open_id_resp;
        }

        $_openId = $_custom_info['openID'];

        $this->h5Service->storeOpenId($_openId);

        $_resp_json = $this->h5Service->formUrl($_openId);

        return redirect($_resp_json['mtk_agent_form']);

    }

    public function h5(Request $request)
    {
        $_resp = app()->call('App\Http\Controllers\KBZ\WebController@precreate', [$request->all()]);

        $_resp_arr = json_decode($_resp->getContent(), true);

        if (!isset($_resp_arr['data']['prepay_id'])) {
            return ResponseFactory::errorResp([
                'url' => '',
                'status' => 0,
                'message' => 'Fail'
            ]);
        }

        $_url_query = http_build_query($_resp_arr['data']);

        $_url = env('APP_URL') . 'h5/confirm?' . $_url_query;

        return ResponseFactory::successResp([
            'url' => $_url,
            'status' => 1,
            'message' => 'Success'
        ]);

    }

    /** H5 Confirm Password */
    public function h5ConfirmUi(Request $request)
    {
//        $this->kbzService->changeConfigDev(env('APP_MTK'), true);

        [
            'prepay_id' => $_prepay_id,
            'merch_order_id' => $_merch_order_id,
            'nonce_str' => $_nonce_str,
            'sign_type' => $_sign_type,
            'sign' => $_sign,
            'code' => $_code,
            'msg' => $_msg
        ] = $request->all();

        $_trans_id = $_merch_order_id;

        $_db_result = DB::select('select trans_id,app from kbz_requests where trans_id=?', [$_merch_order_id])[0];

        switch ($_db_result->app) {
            case env('APP_MTK'):
                $data['url'] = 'http://agent_api.mintheinkha.com/demo/confirm?trans_id=' . $_trans_id;
                break;
            case env('APP_SNS'):
                $data['url'] = 'http://h5.shwenarsin.com/myaccount?source=baganpay';
                break;
        }

        $data['trans_id'] = $_trans_id;
        $data['prepay_id'] = $_prepay_id;

        $_order_info = $this->h5Service->orderInfoH5($_prepay_id, 'mtk', $_nonce_str);

        $data['sign'] = $this->h5Service->singKey($_order_info);
        unset($_order_info['key']);
        $data['order_info'] = http_build_query($_order_info);


//        dd($data);

        return view('h5.confirm', $data);
    }
}
