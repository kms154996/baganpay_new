<?php

namespace App\Http\Controllers\Wave;

use App\Model\Wave2\Wave2CallBack;
use App\Repository\Config\BaseConfigInterface;
use App\Repository\Wave2\ServiceInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class Wave2CallBackController extends Controller
{
    protected $_baseConfig, $_service, $_payment, $_callBackUpdateUrl;

    public function __construct(BaseConfigInterface $baseConfig, ServiceInterface $_service)
    {
        $this->_service = $_service;
        $this->_baseConfig = $baseConfig;
        $this->_payment = 'wave2';
        $this->_callBackUpdateUrl = env('APP_URL') . 'api/wave2pay/callback/update';
    }

    public function index(Request $request)
    {

        \DB::select('insert into callbacks(data,pay,time)  values (?,?,?)', [json_encode($request->all()), 'wave2', date('Y-m-d H:i:s')]);

        $_callbackArr = json_decode($request->getContent(), true);


        $status = $_callbackArr['status'];
        $orderId = $_callbackArr['orderId'];
        $phone = $_callbackArr['initiatorMsisdn'];
        $hashValue = $_callbackArr['hashValue'];
        $amount = $_callbackArr['amount'];


        ['app' => $_app, 'h5' => $_h5, 'trans_id' => $_trans_id] = (array)DB::select("select app,trans_id,h5,amount from wave2_requests where bp_trans_id=?", [$orderId])[0];

        Wave2CallBack::create([
            'bp_trans_id' => $orderId,
            'orderId' => $orderId,
            'phone' => $phone,
            'status' => $status,
            'hashValue' => $hashValue,
            'amount' => $amount,
            'app' => $_app,
            'rawData' => $request->getContent()
        ]);

        $_callbackArr['app'] = $_app;
        $_callbackArr['h5'] = $_h5;
        $_callbackArr['trans_id'] = $_trans_id;
        $_callbackArr['bp_trans_id'] = $orderId;
        $_callbackArr['amount'] = $amount;

//        return (http_build_query($_callbackArr));

        $curl_command = "curl --data '" . http_build_query($_callbackArr) . "' -X POST " . $this->_callBackUpdateUrl . " > /dev/null 2>/dev/null &";

        exec($curl_command);

    }

    public function updateCallBack(Request $request)
    {
        $_app = $request->get('app');
        $_h5 = $request->get('h5');
        $status = $request->get('status');
        $orderId = $request->get('orderId');
        $amount = $request->get('amount');
        $_trans_id = $request->get('trans_id');
        $_bp_trans_id = $request->get('bp_trans_id');

        $_temp = $this->_service->selectTemp($_app, $_h5);

        $_trans_status = $this->checkTransStatus($status);

        $_message = $_trans_status == 1 ? 'success' : 'fail';

        $_updateTemp = $_temp::where('bp_trans_id', $orderId)->first();
        $_updateTemp->status = $_trans_status;
        $_updateTemp->save();

        if ($_trans_status) {

            $_call_back_url = $this->_baseConfig->getCallBackUrlUpdate($_app);

            $_redirect_json_arr['amount'] = $amount;
            $_redirect_json_arr['trans_id'] = $_trans_id;
            $_redirect_json_arr['payment_method'] = $this->_payment;
            $_redirect_json_arr['status'] = $_trans_status;
            $_redirect_json_arr['message'] = $_message;
            $_redirect_json_arr['bp_trans_id'] = $_bp_trans_id;

            $this->_baseConfig->createCallBackLog($_redirect_json_arr, $_app, $this->_payment);

//            $this->_baseConfig->callToApp($_call_back_url, $_redirect_json_arr);
            $this->_baseConfig->curlFunc($_call_back_url, $_redirect_json_arr);
        }
    }

    /** Check Trans Status
     * @param $_status
     * @return int
     */
    private function checkTransStatus($_status)
    {
        switch ($_status) {
            case 'PAYMENT_CONFIRMED':
                return 1;
            default:
                return 0;
        }
    }


    public function testCallback(Request $request)
    {
        \DB::select('insert into callbacks(data,pay,time)  values (?,?,?)', [json_encode($request->all()), 'wave2Dev', date('Y-m-d H:i:s')]);
        $ch_data = curl_init('https://pay_dev.baganit.com/api/wave2/callback');
        $json_str = json_encode($request->all());

//        curl_setopt($ch_data, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch_data, CURLOPT_POSTFIELDS, $json_str);
        # Return response instead of printing.
        curl_setopt($ch_data, CURLOPT_RETURNTRANSFER, true);
        # Send request.
        $response = curl_exec($ch_data);
        curl_close($ch_data);

    }
}
