<?php

namespace App\Http\Controllers\Wave;

use App\Model\Wave2\Wave2Request;
use App\Repository\Config\BaseConfigInterface;
use App\Repository\Wave2\ServiceInterface;
use App\Traits\helperTrait;
use App\TransID;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use League\Flysystem\Config;
use Response;

class Wave2Controller extends Controller
{
    use helperTrait;

    protected $_config, $_service;

    public function __construct(BaseConfigInterface $baseConfig, ServiceInterface $_service)
    {
        $this->_config = $baseConfig;
        $this->_service = $_service;
    }

    public function requestWave(Request $request)
    {
        $_send_data = json_encode($request->all());

        $_send_data_array = json_decode($_send_data, true);

        $_app = $_send_data_array['app'];

        /* BP Trans ID */
        $_trans_class = new TransID();
        $_generate_trans_id = $_trans_class->generatedID($_app);
        $_h5 = $this->_config->checkH5($_send_data_array);

        $_send_data_array['bp_trans_id'] = $_generate_trans_id;

        $_send_data_array['h5'] = $_h5;

        $_send_data_array['trans_id'] = $this->my_decrypt($_send_data_array['trans_id']);

        unset($_send_data_array['payment_method']);

        Wave2Request::create($_send_data_array);

        $_temp = $this->_service->selectTemp($_app, $_h5);

        $_configData = $this->_config->getConfig($_app, 'wave2', $_h5);

        $_send_data_array['merchantId'] = $_configData['merchantId'];

        $frontend_result_url = \Config::get('constants.app_redirect_url_update.' . $_app . '.' . $_configData['env']) . $_send_data_array['trans_id'];

        $_waveRequest = $this->_service->requestCreation($_send_data_array, $frontend_result_url);

        $_items_json = $this->_service->itemsJson([
            [
                'name' => $_app . '_' . (string)$_send_data_array['amount'],
                'amount' => $_send_data_array['amount']
            ]
        ]);

        $_waveRequest['request_url'] = $_configData['request_url'];

        $_temp::create([
            'trans_id' => $_send_data_array['trans_id'],
            'bp_trans_id' => $_send_data_array['bp_trans_id'],
            'order_id' => $_send_data_array['bp_trans_id'],
            'amount' => $_send_data_array['amount']
        ]);


        $_hashData = $this->_service->hashPayLoad($_waveRequest, $_configData['secretKey']);

        try {
            $_postWaveData = $this->_service->requestWave($_waveRequest, $_items_json, $_hashData);

        } catch (GuzzleException $e) {
            return Response::json([
                'status' => 0,
                'message' => 'Request Fail to Wave Server'
            ]);
        }

        $_respArr = json_decode($_postWaveData, true);

        $_respUpdate = $_temp::where('order_id', $_send_data_array['bp_trans_id'])->first();
        $_respUpdate->transaction_id = $_respArr['transaction_id'];
        $_respUpdate->save();

        return Response::json([
            'url' => $_configData['auth_url'] . $_respArr['transaction_id'],
            'status' => 1,
            'message' => 'Success'
        ]);

    }
}
