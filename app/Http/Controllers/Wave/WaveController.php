<?php

namespace App\Http\Controllers\Wave;

use App\Model\MTKWaveRedirect;
use App\Model\MTKWaveRequest;
use App\Model\SeriesWaveRedirect;
use App\Model\SeriesWaveRequest;
use App\Model\SeriesWaveTemp;
use App\Model\WaveRequest;
use App\Model\WunzinWaveRedirect;
use App\Model\WunzinWaveRequest;
use App\Model\WunzinWaveTemp;
use App\Model\MTKWaveTemp;
use App\Payment\Wave;
use App\TransID;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\requestStore;
use App\Traits\waveNoti;
use DB;
use Config;
use Response;
use App\Traits\helperTrait;

class WaveController extends Controller
{
    use requestStore, waveNoti, helperTrait;

    /********* WavePay ***********/
    public function index(Request $request)
    {

        $_send_data = json_encode($request->all());

        $_send_data_array = json_decode($_send_data, true);

        $_app = $_send_data_array['app'];

        /* BP Trans ID */
        $_trans_class = new TransID();
        $_generate_trans_id = $_trans_class->generatedID($_app);

        $_send_data_array['trans_id'] = $this->paymentRequsetId($_send_data_array['trans_id']);

        $_send_data_array['requestID'] = $_generate_trans_id;

        $_send_data_array['app'] = $_app;

        WaveRequest::create([
            'trans_id' => $_send_data_array['trans_id'],
            'bp_trans_id' => $_send_data_array['requestID'],
            'amount' => $_send_data_array['amount'],
            'app' => $_app
        ]);

        $_send_data_array = $this->getModel($_app, $_send_data_array);

        $_check_prepay = $this->checkPrePay($_send_data_array['temp_model'], $_send_data_array['trans_id'])['prepay'];

        if ($_check_prepay === 1) {

            $_return['status'] = 0;
            $_return['message'] = config::get('constants.fail_msg.wave');

            return $_return;

        } else {

            $this->storeRequest($_send_data_array);

            $this->insert_temp($_send_data_array);

            return $this->sendNotiWave($_send_data_array);
        }

    }

    private function sendNotiWave($_data)
    {
        $wave = new Wave();

        $_config_data = $wave->configData($_data['app']);


        foreach ($_config_data as $_key => $_value) {
            $_data[$_key] = $_value;
        }

        $_noti_result = $this->sendNoti($_data);

        $_noti_result_json = json_decode($_noti_result);

        return $this->updateNotiStatus($_data, $_noti_result_json);
    }

    /************** GET PAYMENT REQUEST ID **********/
    public function paymentRequsetId($_trans_id)
    {
        $_trans_id = $this->my_decrypt($_trans_id);

        return $_trans_id;
    }

    /*************** Insert Temp ************/
    public function insert_temp($_data)
    {
        $_data['temp_model']::create([
            'phone' => $_data['phone'],
            'amount' => $_data['amount'],
            'trans_id' => $_data['trans_id'],
            'payment_request_id' => $_data['requestID'],
            'status' => 'pending',
            'request_time' => time()
        ]);
    }


    /************ Update Noti Status *********
     * @param $_data
     * @param $result
     * @return
     */
    public function updateNotiStatus($_data, $result)
    {

        try {
            $_noti_status = $result->statusCode;

            $_requestID = $_data['requestID'];

            $_temp = $_data['temp_model']::where('payment_request_id', $_requestID)->first();
            $_temp->noti_status = $_noti_status;
            $_temp->response_code = json_encode($result);
            $_temp_result = $_temp->save();

            if ($_noti_status === 102) {
                $_return['status'] = 1;
                $_return['message'] = config::get('constants.success_msg.wave');
            } else {
                $_return['status'] = 0;
                $_return['message'] = config::get('constants.fail_msg.wave');
            }

        } catch (\Exception $exception) {
            $_return['status'] = 0;
            $_return['message'] = 'Fail';
        }

        return $_return;


    }

    /************ Redirect *************/
    public function redirect(Request $request)
    {

        $_redirect_model = '';
        $_redirect_json = $request->getContent();
        $_redirect_header = $request->header();

        $_redirect_json_arr = [];

        $_redirect_arr = json_decode($_redirect_json, true);


        DB::select("insert into test(data) value (?)", [$_redirect_json]);


        $_request_id = $_redirect_arr['requestId'];
        $_status_code = $_redirect_arr['statusCode'];

        $_phone = $_redirect_arr['purchaserMsisdn'];
        $_amount = $_redirect_arr['purchaserAmount'];
        $_statusDesc = $_redirect_arr['statusDescription'];
        $_fees = $_redirect_arr['fees'];
        $_completedOn = $_redirect_arr['completedOn'];

        if ($_status_code === "107") {

            $_redirect_json_arr['status'] = 1;
            $_redirect_json_arr['message'] = 'Success';

        } else {

            $_redirect_json_arr['status'] = 0;
            $_redirect_json_arr['message'] = 'Fail';
        }

        ['redirect_model' => $_redirect_model, 'temp' => $_redirect_temp, 'callback_url' => $_call_back_url] = $this->getCallBackData($_request_id);

        $_redirect_model::create([
            'json_str' => $_redirect_json,
            'header_str' => json_encode($_redirect_header),
            'phone' => $_phone,
            'amount' => $_amount,
            'statusCode' => $_status_code,
            'statusDescription' => $_statusDesc,
            'requestId' => $_request_id,
            'completedOn' => $_completedOn,
            'fees' => $_fees,
            'timetick' => time()
        ]);

        $_temp = $_redirect_temp->where('payment_request_id', $_request_id)->first();

        $_temp->redirect_status_code = $_status_code;
        $_temp->status = 'Success';
        $_temp->redirect_json = $_redirect_json;
        $_temp->response_time = $_redirect_arr['completedOn'];
        $_temp->save();

        $_redirect_json_arr['amount'] = $_temp->amount;
        $_redirect_json_arr['trans_id'] = $_temp->trans_id;
        $_redirect_json_arr['payment_method'] = 'wave';


        $_get_str = http_build_query($_redirect_json_arr);


        $curl_command = "curl --data '" . $_get_str . "' -X POST " . $_call_back_url . " > /dev/null 2>/dev/null &";
        exec($curl_command);


//        return Response::json($_get_str);

    }

    /* Check Prepay Status  */
    private function checkPrePay($_temp_model, $_trans_id)
    {

        $_prepay = $_temp_model::where('trans_id', $_trans_id)->where('redirect_status_code', 107)->count();

        if ($_prepay === 1) {
            return array('prepay' => 1);
        } else {
            return array('prepay' => 0);
        }

    }

    /**
     * @param $_app
     * @param $_send_data_array
     * @return mixed
     */
    public function getModel($_app, $_send_data_array)
    {
        switch ($_app) {
            case env('APP_WUNZINN'):

                $_send_data_array['temp_model'] = new WunzinWaveTemp();

                $_send_data_array['request_model'] = new WunzinWaveRequest();

                break;

            case env('APP_MTK'):

                $_send_data_array['temp_model'] = new MTKWaveTemp();;

                $_send_data_array['request_model'] = new MTKWaveRequest();
                break;

            case env('APP_SERIES'):

                $_send_data_array['temp_model'] = new SeriesWaveTemp();;

                $_send_data_array['request_model'] = new SeriesWaveRequest();


        }
        return $_send_data_array;
    }

    public function getCallBackData($request_id)
    {

        \Log::info($request_id);
        $_app = DB::select('select app from wave_requests where bp_trans_id=?', [$request_id])[0]->app;

        switch ($_app) {
            case env('APP_WUNZINN'):
                $_redirect_model = new WunzinWaveRedirect();
                $_redirect_temp = new WunzinWaveTemp();
                $_call_back_url = Config::get('constants.callback_url.WUZ');
                break;
            case env('APP_MTK'):
                $_redirect_model = new MTKWaveRedirect();
                $_redirect_temp = new MTKWaveTemp();
                $_call_back_url = Config::get('constants.callback_url.MTK');
                break;
            case env('APP_SERIES'):
                $_redirect_model = new SeriesWaveRedirect();
                $_redirect_temp = new SeriesWaveTemp();
                $_call_back_url = Config::get('constants.callback_url.SER');
        }

        /** @var TYPE_NAME $_redirect_temp */
        /** @var TYPE_NAME $_redirect_model */
        /** @var TYPE_NAME $_call_back_url */
        return [
            'redirect_model' => $_redirect_model,
            'temp' => $_redirect_temp,
            'callback_url' => $_call_back_url];


    }
}



