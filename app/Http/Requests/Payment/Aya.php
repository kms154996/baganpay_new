<?php


namespace App\Payment;

use App\Repository\AYA\AyaServiceInterface;
use Config;

class Aya
{

    public function getConfig($_app)
    {
        return $this->baseConfig($_app);
    }

    private function baseConfig($_app)
    {
        $_data['callback_url'] = Config::get('constants.aya.' . $_app . '.callback_url');

        $_app_upper = strtoupper($_app);

        switch (env('AYA_' . $_app_upper . '_ENV')) {

            case 'DEV':

                $_data['key'] = '1IgOKNr6IRvgvdhNFm_iNodv6jka';
                $_data['secret'] = 'nSmu6pt9FYSdGts3fWKX2icn_Zga';
                $_data['token_url'] = 'https://opensandbox.ayainnovation.com/token';
                $_data['login_url'] = 'https://opensandbox.ayainnovation.com/merchant/1.0.0/thirdparty/merchant/login';
                $_data['push_payment_url'] = 'https://opensandbox.ayainnovation.com/merchant/1.0.0/thirdparty/merchant/requestPushPayment';
                break;

            case 'PROD':

                $_data['key'] = Config::get('constants.aya.' . $_app . '.key');
                $_data['secret'] = Config::get('constants.aya.' . $_app . '.secret');
                $_data['token_url'] = Config::get('constants.aya.' . $_app . '.token_url');
                $_data['login_url'] = Config::get('constants.aya.' . $_app . '.login_url');
                $_data['push_payment_url'] = Config::get('constants.aya.' . $_app . '.push_payment_url');

                break;
        }

        return $_data;
    }

}
