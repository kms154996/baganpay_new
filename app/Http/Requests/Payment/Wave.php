<?php


namespace App\Payment;

use Config;

class Wave
{
    protected $_app;

    public function configData($_app)
    {
        $this->_app = $_app;

        switch ($_app) {
            case env('APP_WUNZINN'):
                $_app_ = 'wunzinn';
                break;
            case env('APP_MTK'):
                $_app_ = 'mtk';
                break;
            case env('APP_SERIES'):
                $_app_ = 'series';
        }

        return $this->getConfigData($_app_);


    }

    public function getConfigData($_app_)
    {
        $_result = [];

        $_result['call_back_url'] = Config::get('constants.wave.callback_url');


        if (env('WUNZINN_WAVE_ENV') === 'PROD') {

            $_result['url'] = Config::get('constants.wave.url');
            $_result['client_id'] = Config::get('constants.wave.' . $_app_ . '.client_id');
            $_result['client_secret'] = Config::get('constants.wave.' . $_app_ . '.client_secret');
            $_result['hash_value'] = Config::get('constants.wave.' . $_app_ . '.hash_code');
        } else {
            $_result['url'] = 'https://testapi.wavemoney.io:8102/wmt-mfs-merchant-exp/pay-with-wave';
            $_result['client_id'] = "09304064395c4f76a8ed896e75f4635b";
            $_result['client_secret'] = "0398e781a442473AA10DB79731234d22";
            $_result['hash_value'] = "910f82f145a16e7b533573146d5fdefdb0d4d0ee";

        }

        return $_result;
    }


//    public function getConfigWUNZINN()
//    {
//        $_result = [];
//
//        $_result['call_back_url'] = Config::get('constants.wave.callback_url');
//
//
//        if (env('WUNZINN_WAVE_ENV') === 'PROD') {
//
//            $_result['url'] = Config::get('constants.wave.url');
//            $_result['client_id'] = Config::get('constants.wave.wunzinn.client_id');
//            $_result['client_secret'] = Config::get('constants.wave.wunzinn.client_secret');
//            $_result['hash_value'] = Config::get('constants.wave.wunzinn.hash_code');
//        } else {
//            $_result['url'] = 'https://testapi.wavemoney.io:8102/wmt-mfs-merchant-exp/pay-with-wave';
//            $_result['client_id'] = "09304064395c4f76a8ed896e75f4635b";
//            $_result['client_secret'] = "0398e781a442473AA10DB79731234d22";
//            $_result['hash_value'] = "910f82f145a16e7b533573146d5fdefdb0d4d0ee";
//
//        }
//
//        return $_result;
//    }

//    public function getConfigMTk()
//    {
//        $_result = [];
//
//        $_call_back_url = Config::get('constants.wave.callback_url');
//
//        $_result['call_back_url'] = $_call_back_url;
//
//
//        if (env('MTK_WAVE_ENV') === 'PROD') {
//
//
//            $_result['url'] = Config::get('constants.wave.url');
//            $_result['client_id'] = Config::get('constants.wave.mtk.client_id');
//            $_result['client_secret'] = Config::get('constants.wave.mtk.client_secret');
//            $_result['hash_value'] = Config::get('constants.wave.mtk.hash_code');
//
//        } else {
//
//            $_result['url'] = 'https://testapi.wavemoney.io:8102/wmt-mfs-merchant-exp/pay-with-wave';
//            $_result['client_id'] = "09304064395c4f76a8ed896e75f4635b";
//            $_result['client_secret'] = "0398e781a442473AA10DB79731234d22";
//            $_result['hash_value'] = "910f82f145a16e7b533573146d5fdefdb0d4d0ee";
//
//        }
//
//        return $_result;
//    }
}
