<?php

namespace App\Exceptions\Code;

use Exception;
use Response;

class BasicAuth extends Exception
{
    public function render()
    {
        return response()->json([
            'message' => 'Authorization Fail',
            'respDesc' => $this->getMessage(),
            'status' => 0,
            'data' => []
        ],
            401
        );
    }
}
