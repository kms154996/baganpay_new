<?php


namespace App\Payment;

use Config;

class OnePay
{
    protected $_app;

    public function configData($_app)
    {
        $this->_app = $_app;

        $_result=$this->getConfig($_app);

//        switch ($_app) {
//            case env('APP_WUNZINN'):
//                $_result = $this->getConfigWUNZINN();
//                break;
//            case env('APP_MTK'):
//                $_result = $this->getConfigMTK();
//                break;
//        }

        return $_result;

    }

    public function getConfig($_app)
    {
        $_result = [];



        $_result['call_back_url'] = Config::get('constants.onepay.call_back_url');
        $_result['version'] = Config::get('constants.onepay.version');
        $_result['expire_second'] = Config::get('constants.onepay.expire_second');



        switch ($_app) {
            case env('APP_WUNZINN'):
                $_app_ = 'wunzinn';
                $_app = 'WUNZINN';
                break;
            case env('APP_MTK'):
                $_app_ = 'mtk';
                $_app = 'MTK';
                break;
            case env('APP_SERIES'):
                $_app_ = 'series';
                $_app = 'SERIES';
        }

        if (env($_app.'_ONEPAY_ENV') === 'PROD') {

            $_result['user_id'] = Config::get('constants.onepay.'.$_app_.'.user_id');
            $_result['channel'] = Config::get('constants.onepay.'.$_app_.'.channel');
            $_result['secret_key'] = Config::get('constants.onepay.'.$_app_.'.secret_key');

            $_result['verify_phone_url'] = Config::get('constants.onepay.verify_phone_url');
            $_result['direct_payment_url'] = Config::get('constants.onepay.direct_payment_url');
            $_result['remark'] = Config::get('constatns.onepay.remark');
            $_result['remark'] = 'Testing';


        } else {

            if (env($_app.'_ONEPAY_ENV') === 'UAT') {

                $_result['user_id'] = 'MM202052241052281291128';
                $_result['channel'] = 'MIN THEIN KHA';
                $_result['secret_key'] = '7CCA91A540C1';

                $_result['verify_phone_url'] = 'https://onepayuat.agdbank.com/API/Ver01/Wallet/Wallet_VerifyPhoneNumber';
                $_result['direct_payment_url'] = 'https://onepayuat.agdbank.com/API/Ver02/Wallet/Wallet_DirectAPIV2';


            } elseif (env($_app.'_ONEPAY_ENV') === 'DEV') {


                $_result['user_id'] = '';
                $_result['channel'] = '';
                $_result['secret_key'] = '';

                $_result['verify_phone_url'] = 'https://onepayuat.agdbank.com:88/API/Ver01/Wallet/Wallet_VerifyPhoneNumber';
                $_result['direct_payment_url'] = 'https://onepayuat.agdbank.com:88/API/Ver02/Wallet/Wallet_DirectAPIV2';

                $_result['remark'] = 'Testing';


            }

            $_result['remark'] = 'Testing';


        }

        return $_result;
    }

}
