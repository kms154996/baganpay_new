<?php

namespace App\Providers;

use App\Repository\KBZ\H5\H5Service;
use App\Repository\KBZ\H5\H5ServiceInteFace;
use Illuminate\Support\ServiceProvider;

class KbzH5Provider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(H5ServiceInteFace::class, H5Service::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
