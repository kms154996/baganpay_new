<?php

namespace App\Providers;

use App\Repository\Config\BaseConfig;
use App\Repository\Config\BaseConfigInterface;
use App\Repository\KBZ\KbzService;
use App\Repository\KBZ\KbzServiceInterface;
use App\Repository\MYTEL\PayService;
use App\Repository\MYTEL\PayServiceInterface;
use App\Repository\SaiSai\Service;
use App\Repository\SaiSai\ServiceInterface;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {

//        /********** Error Resp *********/
        ResponseFactory::macro('errorResp', function ($data) {

            $_error_data = [
                'status' => 0
            ];


            foreach ($data as $_k => $_va) {
                $_error_data[$_k] = $_va;
            }

            return $_error_data;
        });

        /********* Success Resp **********/
        ResponseFactory::macro('successResp', function ($data) {

            $_data = [
                'status' => 1
            ];


            foreach ($data as $_k => $_va) {
                $_data[$_k] = $_va;
            }

            return $_data;
        });

        $this->app->bind(KbzServiceInterface::class, KbzService::class);
        $this->app->bind(BaseConfigInterface::class, BaseConfig::class);
        $this->app->bind(PayServiceInterface::class, PayService::class);
        $this->app->bind(ServiceInterface::class, Service::class);
        $this->app->bind(\App\Repository\Wave2\ServiceInterface::class, \App\Repository\Wave2\Service::class);
        $this->app->bind(\App\Repository\CB\ServiceInterface::class,\App\Repository\CB\Service::class);
        /** Subscription */
        $this->app->bind(\App\Repository\Subscription\BaseConfigInterFace::class, \App\Repository\Subscription\BaseConfig::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}




