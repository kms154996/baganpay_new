<?php

namespace App\Providers;

use App\Repository\AYA\AyaService;
use App\Repository\AYA\AyaServiceInterface;
use Illuminate\Support\ServiceProvider;

class AyaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AyaServiceInterface::class, AyaService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
