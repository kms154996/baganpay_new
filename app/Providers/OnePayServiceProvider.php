<?php

namespace App\Providers;

use App\Repository\One\PayService;
use App\Repository\One\PayServiceInterface;
use Illuminate\Support\ServiceProvider;

class OnePayServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PayServiceInterface::class, PayService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
