<?php

namespace App\Providers;

use App\Repository\KBZ\PAY\PayService;
use App\Repository\KBZ\PAY\PayServiceInterface;
use Illuminate\Support\ServiceProvider;

class KbzPayProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PayServiceInterface::class, PayService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
