<?php

namespace App\Repository\AYA;

interface AyaServiceInterface
{
    public function getTemp($_app);

    /** Update TempTable Noti Status */
    public function updateNoti($_temp, $_trans_id, $_noti);

}
