<?php


namespace App\Repository\AYA;

use App\Model\Aya\AyaMtkRedirect;
use App\Model\Aya\AyaMtkTemp;
use App\Model\Aya\SeriesRedirect;
use App\Model\Aya\SeriesTemp;
use App\Payment\Aya;
use Config;

class AyaService implements AyaServiceInterface
{
    /** Get Temp */
    public function getTemp($_app)
    {
        switch ($_app) {
            case env('APP_MTK'):
                $_data['app_short'] = 'mtk';
                $_data['temp'] = new AyaMtkTemp();
                $_data['redirect'] = new AyaMtkRedirect();
                $_data['callback_url'] = Config::get('constants.callback_url.MTK');
                break;
            case env('APP_SERIES'):
                $_data['app_short'] = 'series';
                $_data['temp'] = new SeriesTemp();
                $_data['redirect'] = new SeriesRedirect();
                $_data['callback_url'] = Config::get('constants.callback_url.SER');
                break;
        }

        return $_data;
    }

    /** Update TempTable Noti Status */
    public function updateNoti($_temp, $_trans_id, $_noti)
    {
        $_temp::updateOrCreate(
            ['bp_trans_id' => $_trans_id],
            [
                'noti_status' => $_noti
            ]
        );
    }

}
