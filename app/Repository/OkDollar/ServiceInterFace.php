<?php

namespace App\Repository\OkDollar;

interface ServiceInterFace
{
    public function createJson($_arr);

    public function enCrypt($_text, $key);

    public function createString($_encryptText, $_iv, $_merchantNumber);
}
