<?php


namespace App\Repository\OkDollar;


class Service implements ServiceInterFace
{
    public function createJson($_arr)
    {
        return json_encode($_arr);
    }

    public function enCrypt($_text, $key)
    {
        $plaintext = "message to be encrypted";
        $cipher = "aes-128-gcm";
        if (in_array($cipher, openssl_get_cipher_methods())) {
            $ivlen = openssl_cipher_iv_length($cipher);
            $iv = openssl_random_pseudo_bytes($ivlen);
            $_encrypt_text = openssl_encrypt($plaintext, $cipher, $key, $options = 0, $iv, $tag);

//            $original_plaintext = openssl_decrypt($ciphertext, $cipher, $key, $options=0, $iv, $tag);
            return [
                'iv' => $iv,
                'encrypted' => $_encrypt_text
            ];
        }
    }

    public function createString($_encryptText, $_iv, $_merchantNumber)
    {
        return $_encryptText . ',' . $_iv . ',' . $_merchantNumber;
    }
}
