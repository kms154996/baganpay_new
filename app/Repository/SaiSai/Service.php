<?php


namespace App\Repository\SaiSai;

use App\Model\SaiSai\MtkH5Temp;
use App\Model\SaiSai\MtkTemp;
use App\Model\SaiSai\SeriesTemp;
use App\Repository\Config\BaseConfigInterface;
use Config;
use GuzzleHttp\Client;

class Service implements ServiceInterface
{

    protected $_uat_url, $_prod_url, $_config;


    public function __construct(BaseConfigInterface $baseConfig)
    {
        $this->_uat_url = 'http://webapi.uatuab.com:8080/';
        $this->_prod_url = 'https://www.uabpaybusiness.com/';
        $this->_config = $baseConfig;
    }


    public function getLoginConfig($_app_env, $_app, $_h5)
    {
        $_res = [];

        if (!$_h5)

            switch ($_app_env) {

                case 'DEV':

                    switch ($_app) {

                        case env('APP_MTK'):
                            $_res['username'] = 'UABMM202000180900298372058';
                            $_res['password'] = 'bit@uat2020';
                            break;
                        case env('APP_SERIES'):
                            $_res['username'] = 'UABMM202053161253203072613';
                            $_res['password'] = 'D78D9454';
                            break;
                    }

                    $_res['url'] = $this->_uat_url . 'API/Ver01/Wallet/Wallet_Login';
                    break;

                case 'PROD':
                    switch ($_app) {
                        case env('APP_MTK'):
                            $_res['username'] = 'UABMM202005031205299803308';
                            $_res['password'] = '@bit*mtk!uab20';
                            break;
                        case env('APP_SERIES'):
                            $_res['username'] = 'UABMM202002031202559832204';
                            $_res['password'] = '@bit*wuz!uab20';
                            break;
                    }
                    $_res['url'] = $this->_prod_url . 'API/Ver01/Wallet/Wallet_Login';
            }

        else

            switch ($_app_env) {

                case 'DEV':

                    switch ($_app) {

                        case env('APP_MTK'):
                            $_res['username'] = 'UABMM202000180900298372058';
                            $_res['password'] = 'bit@uat2020';
                            break;
                        case env('APP_SERIES'):
                            $_res['username'] = 'UABMM202053161253203072613';
                            $_res['password'] = 'D78D9454';
                            break;
                    }

                    $_res['url'] = $this->_uat_url . 'API/Ver01/Wallet/Wallet_Login';
                    break;

                case 'PROD':
                    switch ($_app) {
                        case env('APP_MTK'):
                            $_res['username'] = 'UABMM202005031205299803308';
                            $_res['password'] = '@bit*mtk!uab20';
                            break;
                        case env('APP_SERIES'):
                            $_res['username'] = 'UABMM202002031202559832204';
                            $_res['password'] = '@bit*wuz!uab20';
                            break;
                    }
                    $_res['url'] = $this->_prod_url . 'API/Ver01/Wallet/Wallet_Login';
            }


        return $_res;
    }

    public function getCheckPhoneConfig($_config_data, $_phone)
    {
        $_uab_phone = '0' . substr($_phone, 2);
        $_app_name = 'saisaipay';
        $_hash_string = $_config_data['Channel'] . $_config_data['MerchantUserId'] . $_uab_phone . $_app_name;
        $_hash_value = $this->hashValue($_hash_string, $_config_data['secretKey']);
        return [
            "Channel" => $_config_data['Channel'],
            "MerchantUserId" => $_config_data['MerchantUserId'],
            "UabpayPhoneNo" => $_uab_phone,
            "AppName" => $_app_name,
            "HashValue" => $_hash_value
        ];
    }

    public function callUab($_arr, $_token, $_url)
    {
        $client = new Client([
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => $_token
            ]
        ]);


        $response = $client->post($_url,
            ['form_params' => $_arr]
        );

        $response = $response->getBody();
        return $response;
    }

    /** get Pay Data
     * @param $_config_data
     * @param $_request_arr
     */
    public function getPayConfig($_config_data, $_request_arr)
    {
        $_appName = "saisaipay";
        $_sequence_str = $this->generateSeqString();
        $_phone = '0' . substr($_request_arr['phone'], 2);
        $_expSeconds = 200;
        $_remark = 'Testing';
        $_hash_string = $_config_data['Channel'] . $_appName . $_config_data["MerchantUserId"] . $_phone . $_request_arr['amount'] . $_remark . $_request_arr['bp_trans_id'] . $_sequence_str . $_config_data['callback_url'] . $_expSeconds;
        $_hash_value = $this->hashValue($_hash_string, $_config_data['secretKey']);
        return [
            "Channel" => $_config_data['Channel'],
            "AppName" => $_appName,
            "MerchantUserId" => $_config_data["MerchantUserId"],
            "InvoiceNo" => $_request_arr['bp_trans_id'],
            "SequenceNo" => $_sequence_str,
            "Amount" => (string)$_request_arr['amount'],
            "Remark" => $_remark,
            "WalletUserID" => $_phone,
            "CallBackUrl" => $_config_data['callback_url'],
            "ExpiredSeconds" => $_expSeconds,
            "HashValue" => $_hash_value
        ];
    }

    private function hashValue($_str, $_key)
    {
        return strtoupper(hash_hmac('sha1', $_str, $_key, false));
    }

    /********* Generate Sequence String ********/
    public function generateSeqString()
    {
        $length = 32;
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $seq_str = '';
        for ($i = 0; $i < $length; $i++) {
            $seq_str .= $characters[rand(0, $charactersLength - 1)];
        }

        return $seq_str;
    }

    /** Store Temp */
    public function storeTemp($_arr, $_temp)
    {
        $_temp::create([
            'trans_id' => $_arr['trans_id'],
            'bp_trans_id' => $_arr['bp_trans_id'],
            'phone' => $_arr['phone'],
            'amount' => $_arr['amount']
        ]);
    }

    /** Update Noti Resp */
    public function updateNtoiResp($_noti_resp, $_arr, $_temp)
    {

        $_data = $_temp::where('bp_trans_id', $_arr['bp_trans_id'])->first();

        if ($_noti_resp['RespCode'] === '000') {
            $_noti_status = 1;
            $_message = \Config::get('constants.success_msg.saisai');

        } else {
            $_noti_status = 0;
            $_message = \Config::get('constants.fail_msg.saisai');
        }

        $_data->noti_resp = $_noti_resp['RespCode'];
        $_data->noti_status = $_noti_status;
        $_data->save();

        return [
            'status' => $_noti_status,
            'message' => $_message,
            'url'=>''
        ];
    }

    /** Get Temp
     * @param $_app
     * @param $_h5
     * @return MtkTemp|SeriesTemp
     */
    public function getTemp($_app, $_h5)
    {
        if (!$_h5)
            switch ($_app) {
                case env('APP_MTK'):
                    return new MtkTemp();
                case env('APP_SERIES'):
                    return new SeriesTemp();
            }
        else
            switch ($_app) {
                case env('APP_MTK'):
                    return new MtkH5Temp();
                case env('APP_SERIES'):
                    return new SeriesTemp();
            }
    }

}
