<?php

namespace App\Repository\SaiSai;

interface ServiceInterface
{
    public function getLoginConfig($_app_env,$_app,$_h5);

    public function getCheckPhoneConfig($_config_data, $phone);

    public function callUab($_arr, $_token, $_url);

    public function getTemp($_app,$_h5);
}
