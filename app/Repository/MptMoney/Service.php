<?php


namespace App\Repository\MptMoney;


use App\Model\MptMoney\MptMoneyMtkTemp;
use App\Repository\Config\BaseConfigInterface;
use Config;


include 'RSAUtils.php';

class Service implements ServiceInterface
{
    protected $_baseConfig;

    public function __construct(BaseConfigInterface $baseConfig)
    {
        $this->_baseConfig = $baseConfig;
    }

    public function createArray($_configData, $_requestData)
    {

//        $_app = $this->_baseConfig->changeAppName($_requestData['app'])['app_upper'];

        return [
            'partnerRefNo' => $_requestData['bp_trans_id'],
            'merchantShortCode' => $_configData['merchantShortCode'],
            'goods' => 'goods',
            'amount' => (float)$_requestData['amount'],
            'fee' => 0,
            'currency' => 'MMK',
            'merchant' => $_configData['merchant'],
            'backURL' => \Config::get('constants.mptmoney.callback_url') . '/' . $_requestData['app']
        ];

    }

    public function rsaEncrypt($_arr, $_key)
    {
        $_json = json_encode($_arr);

        $_pubKey = \RSAUtils::str2key($_key, \RSAUtils::PUBLIC_TYPE);

        return $priEncrypt = base64_encode(\RSAUtils::encryptByPublicKey($_json, $_pubKey));

        return \RSAUtils::decryptByPublicKey('RktDVGZDbHlBNU1PdTd6VGFWSnZHUks2NWRHUFdHVEx3OEpsT2c3R1BHb2txdTdSUkJHaHBTalJpMkVnM3BuR2t5RUNrSG1iUTE2QQpTR3dGdmFjbXd5L1hPSGFaRFduR29hRUY2RnlwQy80UHdrNEtBd0kxaEZJNzlsYzk0ZU9pSSsvZ3owWWhDSWZxTzZNWktQMUI0VDlCClQzeVFrWThvMUFleVppb3VDOXgwajlPZkhzb2wyYjlSVGQ0WjkzMEltSklTUloyYjA3eFFzdDgzUk9lN0tpcTB5V0RHR2FraWJUT2wKdU1ydGljWk9vT0UvbEhlQUtXVXFYRVd4MllOcVJ5Z3dSSHZTVCtkaitpOVdFWGRVTFB0Z0M0aEpUbE9KSkJ5QlkyY2VjYTFZY3N5NgpzRHIySVQ4aVBCWldNTFhRUmZSMWFoRWJhMmRwWUNKQkZyKzFoZz09', $_pubKey);

    }

    public function getApp($_bp_trnas_id)
    {
        return \DB::select('select trans_id,bp_trans_id,app,amount from mpt_money_requests where bp_trans_id=?', [$_bp_trnas_id])[0];
    }

    public function decrypt($_key, $_data)
    {
        $_pubKey = \RSAUtils::str2key($_key, \RSAUtils::PUBLIC_TYPE);

        return \RSAUtils::decryptByPublicKey(base64_decode($_data), $_pubKey);
    }

    /** Get Temp */
    public function getTemp($_app)
    {
        switch ($_app) {
            case env('APP_MTK'):
                $_temp = new MptMoneyMtkTemp();
                break;
            case env('APP_SERIES'):
                $_temp = "";
                break;
        }
        return $_temp;
    }

}



