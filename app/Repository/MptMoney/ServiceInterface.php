<?php

namespace App\Repository\MptMoney;

interface ServiceInterface
{
    public function createArray($_configData, $_requestData);

    public function rsaEncrypt($_arr, $_key);

}
