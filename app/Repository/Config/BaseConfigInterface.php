<?php

namespace App\Repository\Config;

interface BaseConfigInterface
{
    public function getConfig($_app, $_payment_method, $_h5);

    public function getData($_app_, $_app_env, $_payment_method, $_h5);

    public function payConfig($_app_, $_payment_method, $_payment_data, $_env, $_h5);

    /** Get Call Back Url
     * @param $_app
     */
    public function getCallBackUrl($_app);

    /** Send Callback To App Server
     * @param $_amount
     * @param $_trans_id
     * @param $_status
     * @param $_message
     * @param $_method
     * @param $_url
     */
    public function callBackToApp($_amount, $_trans_id, $_status, $_message, $_method, $_url);

    /*** Check Field H5 **
     * @param $_arr
     * @return bool
     */
    public function checkH5($_arr);

    /** Create CallBack Log
     * @param $_redirect_json_arr
     * @param $_app
     * @param $_payment_method
     */
    public function createCallBackLog($_redirect_json_arr, $_app, $_payment_method);

    /** Time Out Test
     * @param $_url
     * @param $_data
     */
    public function callToApp($_url, $_data);

    /**
     * @param $_request_arr
     * @return array
     */
    public function getBpId($_request_arr);

    /** Call App Server with CURL
     * @param $url
     * @param $data
     * @return mixed
     */
    public function curlFunc($url, $data);
}
