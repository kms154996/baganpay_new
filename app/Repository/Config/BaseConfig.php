<?php


namespace App\Repository\Config;

use App\Model\AppCallBackLog;
use Config;
use GuzzleHttp\Client;
use App\TransID;

class BaseConfig implements BaseConfigInterface
{

    public function changeAppName($_appName)
    {
        switch ($_appName) {
            case env('APP_WUNZINN'):
                $_app_ = 'wunzinn';
                $_app_upper = 'WUNZINN';
                break;
            case env('APP_MTK'):
                $_app_ = 'mtk';
                $_app_upper = 'MTK';
                break;
            case env('APP_SERIES'):
                $_app_ = 'series';
                $_app_upper = 'SERIES';
                break;
            case env('APP_SNS'):
                $_app_ = 'sns';
                $_app_upper = 'SNS';
                break;
            case env('APP_SAT'):
                $_app_ = 'sat';
                $_app_upper = 'SAT';
                break;
        }

        return [
            'app' => $_app_,
            'app_upper' => $_app_upper
        ];
    }

    public function getConfig($_app, $_payment_method, $_h5)
    {

        ['app' => $_app_, 'app_upper' => $_app_upper] = $this->changeAppName($_app);

        $_payment_method_upper = strtoupper($_payment_method);
        $_app_env = $_app_upper . '_' . $_payment_method_upper;
        switch ($_payment_method) {
            case "one":
                $_app_env = $_app_upper . '_' . $_payment_method_upper . 'PAY';
                break;
        }


        if ($_h5)
            $_app_env .= '_H5_ENV';

        else
            $_app_env .= '_ENV';


        /** @var TYPE_NAME $_payment_method */
        return $this->getData($_app_, $_app_env, $_payment_method, $_h5);

    }


    public function getData($_app_, $_app_env, $_payment_method, $_h5)
    {
        $_base_path = base_path() . '/app/Repository/Config';
        $_file_path = $_base_path . '/payment_key_name.json';

        $_file_data = json_decode(file_get_contents($_file_path), true);

        $_payment_data = $_file_data[$_payment_method];

        $_env = env($_app_env);

        return $this->payConfig($_app_, $_payment_method, $_payment_data, $_env, $_h5);

    }

    public function payConfig($_app_, $_payment_method, $_payment_data, $_env, $_h5)
    {

        switch ($_env) {
            case 'PROD':

                switch ($_payment_method) {
                    case 'one':
                        $_pay_ = $_payment_method . 'pay';
                        break;

                    default:
                        $_pay_ = $_payment_method;
                        break;

                }

                if (!$_h5)
                    foreach ($_payment_data['only'] as $_key => $_data) {
                        $_result[$_data] = Config::get('constants.' . $_pay_ . '.' . $_app_ . '.' . $_data);

                    }
                else
                    foreach ($_payment_data['only'] as $_key => $_data) {

                        $_result[$_data] = Config::get('constants.' . $_pay_ . '.h5.' . $_app_ . '.' . $_data);
                    }


                if (!$_h5)
                    foreach ($_payment_data['url'] as $_key => $_data) {
                        $_result[$_data] = Config::get('constants.' . $_pay_ . '.' . $_data);
                    }
                else

                    foreach ($_payment_data['h5_url'] as $_key => $_data) {

                        $_result[$_data] = Config::get('constants.' . $_pay_ . '.h5.' . $_data);

                    }
                $_result['env'] = $_env;

                return $_result;

                break;

            case 'DEV':

                $_base_path = base_path() . '/app/Repository/Config';
                $_file_path = $_base_path . '/dev_key.json';

                $_file_data = json_decode(file_get_contents($_file_path), true);

                $_payment_key_collect = collect($_payment_data['only']);

                $_payment_key_data = $_file_data[$_payment_method];

                if (!$_h5)
                    $_payment_url = $_file_data[$_payment_method]['url'];
                else
                    $_payment_url = $_file_data[$_payment_method]['h5_url'];


                if (!$_h5)
                    $_payment_key_data_arr = collect($_payment_key_data[$_app_])->except('url')->toArray();
                else
                    $_payment_key_data_arr = collect($_payment_key_data['h5'][$_app_])->except('url')->toArray();

                $_config_data = $_payment_key_collect->combine($_payment_key_data_arr);

                foreach ($_payment_url as $_key => $_value) {
                    $_config_data[$_key] = $_value;
                }

                $_config_data['env'] = $_env;
                return $_config_data->all();

                break;
        }

    }


    /** Get Call Back Url */
    public function getCallBackUrl($_app)
    {

        $_callback_url = '';
        switch ($_app) {
            case env('APP_MTK'):
                $_callback_url = Config::get('constants.callback_url.MTK');
                break;
            case env('APP_WUNZINN'):
                $_callback_url = Config::get('constants.callback_url.WUZ');
                break;
            case env('APP_SERIES'):
                $_callback_url = Config::get('constants.callback_url.SER');
        }

        return $_callback_url;
    }


    public function getCallBackUrlUpdate($_app)
    {
        $_callback_url = '';
        switch ($_app) {
            case env('APP_MTK'):
                $_callback_url = Config::get('constants.callback_url.MTK');
                break;
            case env('APP_WUNZINN'):
                $_callback_url = '';
                break;
            case env('APP_SERIES'):
                $_callback_url = 'https://seriesapi.wunzinn.com/api/baganpay/callback';
        }

        return $_callback_url;
    }

    /** Send Callback To App Server
     * @param $_amount
     * @param $_trans_id
     * @param $_status
     * @param $_message
     * @param $_method
     * @param $_url
     */
    public function callBackToApp($_amount, $_trans_id, $_status, $_message, $_method, $_url)
    {
        $_redirect_json_arr['amount'] = $_amount;
        $_redirect_json_arr['trans_id'] = $_trans_id;
        $_redirect_json_arr['payment_method'] = $_method;
        $_redirect_json_arr['status'] = $_status;
        $_redirect_json_arr['message'] = $_message;

        $_get_str = http_build_query($_redirect_json_arr);


        $curl_command = "curl --data '" . $_get_str . "' -X POST " . $_url . " > /dev/null 2>/dev/null &";
        exec($curl_command);
    }

    /** Check Prepay
     * @param $_temp
     * @param $_trans_id
     * @param $_checkColumn
     * @param $_checkStatus
     * @return bool
     */
    public function checkPrepay($_temp, $_trans_id, $_checkColumn, $_checkStatus)
    {
        $_count = $_temp::where('trans_id', $_trans_id)->where($_checkColumn, $_checkStatus)->count();
        if ($_count > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*** Check Field H5 **
     * @param $_arr
     * @return bool
     */
    public function checkH5($_arr)
    {
        if (isset($_arr['h5']) && $_arr['h5'] = true)
            return true;
        else
            return false;
    }


    /** Time Out Test
     * @param $_url
     * @param $_data
     */
    public function callToApp($_url, $_data)
    {
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);

        try {

            $client = new Client([
                'headers' => ['Content-Type' => 'application/json']
            ]);
            $response = $client->post($_url,
                [
                    'timeout' => 10,
                    'form_params' => $_data]
            );


            \Log::info(json_encode($response->getBody()));
            \Log::info($_data);

            $_response_data = json_decode($response->getBody(), true);

            $_callback_status = 1;

        } catch (\GuzzleHttp\Exception\RequestException $exception) {

            $_callback_status = 2;


        }

        AppCallBackLog::updateOrCreate(
            [
                'trans_id' => $_data['trans_id']
            ], [
                'callback_status' => $_callback_status
            ]
        );

    }

    /** Call App Server with CURL
     * @param $url
     * @param $data
     * @return mixed
     */
    public function curlFunc($url, $data)
    {
        $cu = curl_init();
        curl_setopt_array($cu, array(
            CURLOPT_URL => $url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_RETURNTRANSFER => true
        ));
        curl_setopt($cu, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($cu, CURLOPT_TIMEOUT, 15);

        $response = json_decode(curl_exec($cu));

        if (curl_errno($cu))
            $status = 2;
        else
            $status = 1;

        curl_close($cu);


        AppCallBackLog::updateOrCreate(
            [
                'trans_id' => $data['trans_id']
            ], [
                'callback_status' => $status
            ]
        );

        die();

    }


    /** Return CallBack Str
     * @param $_arr
     * @return string
     */
    public function callBackStr($_arr)
    {
        return http_build_query($_arr);
    }

    /** Create CallBack Log
     * @param $_redirect_json_arr
     * @param $_app
     * @param $_payment_method
     */
    public function createCallBackLog($_redirect_json_arr, $_app, $_payment_method)
    {
        AppCallBackLog::create([
            'trans_id' => $_redirect_json_arr['trans_id'],
            'bp_trans_id' => $_redirect_json_arr['bp_trans_id'],
            'trans_status' => $_redirect_json_arr['status'],
            'amount' => $_redirect_json_arr['amount'],
            'rawData' => $this->callBackStr($_redirect_json_arr),
            'app' => $_app,
            'payment_method' => $_payment_method
        ]);
    }

    /**
     * @param $_request_arr
     * @return array
     */
    public function getBpId($_request_arr): array
    {
        $_trans_class = new TransID();
        $_generate_trans_id = $_trans_class->generatedID($_request_arr['app']);
        $_request_arr['bp_trans_id'] = $_generate_trans_id;
        return array($_generate_trans_id, $_request_arr);
    }
}


//namespace App\Repository\Config;
//
//use App\Model\AppCallBackLog;
//use Config;
//use GuzzleHttp\Client;
//use App\TransID;
//
//class BaseConfig implements BaseConfigInterface
//{
//
//    public function changeAppName($_appName)
//    {
//        switch ($_appName) {
//            case env('APP_WUNZINN'):
//                $_app_ = 'wunzinn';
//                $_app_upper = 'WUNZINN';
//                break;
//            case env('APP_MTK'):
//                $_app_ = 'mtk';
//                $_app_upper = 'MTK';
//                break;
//            case env('APP_SERIES'):
//                $_app_ = 'series';
//                $_app_upper = 'SERIES';
//        }
//
//        return [
//            'app' => $_app_,
//            'app_upper' => $_app_upper
//        ];
//    }
//
//    public function getConfig($_app, $_payment_method, $_h5)
//    {
//
//        ['app' => $_app_, 'app_upper' => $_app_upper] = $this->changeAppName($_app);
//
//        $_payment_method_upper = strtoupper($_payment_method);
//        $_app_env = $_app_upper . '_' . $_payment_method_upper;
//        switch ($_payment_method) {
//            case "one":
//                $_app_env = $_app_upper . '_' . $_payment_method_upper . 'PAY';
//                break;
//        }
//
//
//        if ($_h5)
//            $_app_env .= '_H5_ENV';
//
//        else
//            $_app_env .= '_ENV';
//
//
//        /** @var TYPE_NAME $_payment_method */
//        return $this->getData($_app_, $_app_env, $_payment_method, $_h5);
//
//    }
//
//
//    public function getData($_app_, $_app_env, $_payment_method, $_h5)
//    {
//        $_base_path = base_path() . '/app/Repository/Config';
//        $_file_path = $_base_path . '/payment_key_name.json';
//
//        $_file_data = json_decode(file_get_contents($_file_path), true);
//
//        $_payment_data = $_file_data[$_payment_method];
//
//        $_env = env($_app_env);
//
//        return $this->payConfig($_app_, $_payment_method, $_payment_data, $_env, $_h5);
//
//    }
//
//    public function payConfig($_app_, $_payment_method, $_payment_data, $_env, $_h5)
//    {
//
//        switch ($_env) {
//            case 'PROD':
//
//                switch ($_payment_method) {
//                    case 'one':
//                        $_pay_ = $_payment_method . 'pay';
//                        break;
//
//                    default:
//                        $_pay_ = $_payment_method;
//                        break;
//
//                }
//
//                if (!$_h5)
//                    foreach ($_payment_data['only'] as $_key => $_data) {
//                        $_result[$_data] = Config::get('constants.' . $_pay_ . '.' . $_app_ . '.' . $_data);
//
//                    }
//                else
//                    foreach ($_payment_data['only'] as $_key => $_data) {
//                        $_result[$_data] = Config::get('constants.' . $_pay_ . '.h5.' . $_app_ . '.' . $_data);
//
//                    }
//
//
//                foreach ($_payment_data['url'] as $_key => $_data) {
//                    $_result[$_data] = Config::get('constants.' . $_pay_ . '.' . $_data);
//
//                }
//
//                $_result['env'] = $_env;
//
//                return $_result;
//
//                break;
//
//            case 'DEV':
//
//                $_base_path = base_path() . '/app/Repository/Config';
//                $_file_path = $_base_path . '/dev_key.json';
//
//                $_file_data = json_decode(file_get_contents($_file_path), true);
//
//                $_payment_key_collect = collect($_payment_data['only']);
//
//                $_payment_key_data = $_file_data[$_payment_method];
//
//                $_payment_url = $_file_data[$_payment_method]['url'];
//
//
//                if (!$_h5)
//                    $_payment_key_data_arr = collect($_payment_key_data[$_app_])->except('url')->toArray();
//                else
//                    $_payment_key_data_arr = collect($_payment_key_data['h5'][$_app_])->except('url')->toArray();
//
//                $_config_data = $_payment_key_collect->combine($_payment_key_data_arr);
//
//                foreach ($_payment_url as $_key => $_value) {
//                    $_config_data[$_key] = $_value;
//                }
//
//                $_config_data['env'] = $_env;
//                return $_config_data->all();
//
//                break;
//        }
//
//    }
//
//
//    /** Get Call Back Url */
//    public function getCallBackUrl($_app)
//    {
//
//        $_callback_url = '';
//        switch ($_app) {
//            case env('APP_MTK'):
//                $_callback_url = Config::get('constants.callback_url.MTK');
//                break;
//            case env('APP_WUNZINN'):
//                $_callback_url = Config::get('constants.callback_url.WUZ');
//                break;
//            case env('APP_SERIES'):
//                $_callback_url = Config::get('constants.callback_url.SER');
//        }
//
//        return $_callback_url;
//    }
//
//
//    public function getCallBackUrlUpdate($_app)
//    {
//        $_callback_url = '';
//        switch ($_app) {
//            case env('APP_MTK'):
//                $_callback_url = Config::get('constants.callback_url.MTK');
//                break;
//            case env('APP_WUNZINN'):
//                $_callback_url = '';
//                break;
//            case env('APP_SERIES'):
//                $_callback_url = 'https://seriesapi.wunzinn.com/api/baganpay/callback';
//        }
//
//        return $_callback_url;
//    }
//
//    /** Send Callback To App Server
//     * @param $_amount
//     * @param $_trans_id
//     * @param $_status
//     * @param $_message
//     * @param $_method
//     * @param $_url
//     */
//    public function callBackToApp($_amount, $_trans_id, $_status, $_message, $_method, $_url)
//    {
//        $_redirect_json_arr['amount'] = $_amount;
//        $_redirect_json_arr['trans_id'] = $_trans_id;
//        $_redirect_json_arr['payment_method'] = $_method;
//        $_redirect_json_arr['status'] = $_status;
//        $_redirect_json_arr['message'] = $_message;
//
//        $_get_str = http_build_query($_redirect_json_arr);
//
//
//        $curl_command = "curl --data '" . $_get_str . "' -X POST " . $_url . " > /dev/null 2>/dev/null &";
//        exec($curl_command);
//    }
//
//    /** Check Prepay
//     * @param $_temp
//     * @param $_trans_id
//     * @param $_checkColumn
//     * @param $_checkStatus
//     * @return bool
//     */
//    public function checkPrepay($_temp, $_trans_id, $_checkColumn, $_checkStatus)
//    {
//        $_count = $_temp::where('trans_id', $_trans_id)->where($_checkColumn, $_checkStatus)->count();
//        if ($_count > 0) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//    /*** Check Field H5 **
//     * @param $_arr
//     * @return bool
//     */
//    public function checkH5($_arr)
//    {
//        if (isset($_arr['h5']) && $_arr['h5'] = true)
//            return true;
//        else
//            return false;
//    }
//
//
//    /** Time Out Test
//     * @param $_url
//     * @param $_data
//     */
//    public function callToApp($_url, $_data)
//    {
//        $client = new Client([
//            'headers' => ['Content-Type' => 'application/json']
//        ]);
//
//        try {
//
//            $client = new Client([
//                'headers' => ['Content-Type' => 'application/json']
//            ]);
//            $response = $client->post($_url,
//                [
//                    'timeout' => 10,
//                    'form_params' => $_data]
//            );
//
//
//            \Log::info(json_encode($response->getBody()));
//            \Log::info($_data);
//
//            $_response_data = json_decode($response->getBody(), true);
//
//            $_callback_status = 1;
//
//        } catch (\GuzzleHttp\Exception\RequestException $exception) {
//
//            $_callback_status = 2;
//
//
//        }
//
//        AppCallBackLog::updateOrCreate(
//            [
//                'trans_id' => $_data['trans_id']
//            ], [
//                'callback_status' => $_callback_status
//            ]
//        );
//
//    }
//
//    /** Call App Server with CURL
//     * @param $url
//     * @param $data
//     * @return mixed
//     */
//    public function curlFunc($url, $data)
//    {
//        $cu = curl_init();
//        curl_setopt_array($cu, array(
//            CURLOPT_URL => $url,
//            CURLOPT_POST => 1,
//            CURLOPT_POSTFIELDS => json_encode($data),
//            CURLOPT_RETURNTRANSFER => true
//        ));
//        curl_setopt($cu, CURLOPT_CONNECTTIMEOUT, 10);
//        curl_setopt($cu, CURLOPT_TIMEOUT, 15);
//
//        $response = json_decode(curl_exec($cu));
//
//        if (curl_errno($cu))
//            $status = 2;
//        else
//            $status = 1;
//
//        curl_close($cu);
//
//
//        AppCallBackLog::updateOrCreate(
//            [
//                'trans_id' => $data['trans_id']
//            ], [
//                'callback_status' => $status
//            ]
//        );
//
//        die();
//
//    }
//
//
//    /** Return CallBack Str
//     * @param $_arr
//     * @return string
//     */
//    public function callBackStr($_arr)
//    {
//        return http_build_query($_arr);
//    }
//
//    /** Create CallBack Log
//     * @param $_redirect_json_arr
//     * @param $_app
//     * @param $_payment_method
//     */
//    public function createCallBackLog($_redirect_json_arr, $_app, $_payment_method)
//    {
//        AppCallBackLog::create([
//            'trans_id' => $_redirect_json_arr['trans_id'],
//            'bp_trans_id' => $_redirect_json_arr['bp_trans_id'],
//            'trans_status' => $_redirect_json_arr['status'],
//            'amount' => $_redirect_json_arr['amount'],
//            'rawData' => $this->callBackStr($_redirect_json_arr),
//            'app' => $_app,
//            'payment_method' => $_payment_method
//        ]);
//    }
//
//    /**
//     * @param $_request_arr
//     * @return array
//     */
//    public function getBpId($_request_arr): array
//    {
//        $_trans_class = new TransID();
//        $_generate_trans_id = $_trans_class->generatedID($_request_arr['app']);
//        $_request_arr['bp_trans_id'] = $_generate_trans_id;
//        return array($_generate_trans_id, $_request_arr);
//    }
//}
//
