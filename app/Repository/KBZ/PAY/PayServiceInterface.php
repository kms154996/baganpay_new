<?php

namespace App\Repository\KBZ\PAY;

interface PayServiceInterface
{
    /************ Generate NONCE STR **********/
    public function nonce_str_generate();

    public function signStr($_req_arr);

    public function signKey($_req_arr);

    public function requestArr($_merch_order_id, $_amount, $_app, $_trade_type,$_is_h5);

    public function configData($_merch_order_id, $_amount, $_app, $_trade_type,$_is_h5);

//    public function transFormApp($_app);
}
