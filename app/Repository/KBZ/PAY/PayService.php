<?php


namespace App\Repository\KBZ\PAY;

use App\Repository\Config\BaseConfigInterface;
use App\Repository\KBZ\KbzService;
use Config;

class PayService implements PayServiceInterface
{
    protected $_kbzService, $_baseConfig;

    public function __construct(KbzService $kbzService, BaseConfigInterface $baseConfig)
    {
        $this->_kbzService = $kbzService;
        $this->_baseConfig = $baseConfig;
    }

    /************ Generate NONCE STR **********/
    public function nonce_str_generate()
    {
        $length = 32;
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $nonce_str = '';
        for ($i = 0; $i < $length; $i++) {
            $nonce_str .= $characters[rand(0, $charactersLength - 1)];
        }

        return $nonce_str;
    }

    public function signStr($_req_arr)
    {
        unset($_req_arr['sign']);
        unset($_req_arr['sign_type']);
        ['biz_content' => $_bizcontent] = $_req_arr;

        unset($_req_arr['biz_content']);
        unset($_bizcontent['refer']);

        $_data = array_merge($_bizcontent, $_req_arr);

        ksort($_data);

        $key = $_data['key'];

        unset($_data['key']);

        $_signStr = '';

        foreach ($_data as $_key => $_value) {
            $_signStr .= $_key . '=' . $_value . '&';
        }

        return $_signStr . 'key=' . $key;
    }

    public function signKey($_req_arr)
    {
        $_sign_str = $this->signStr($_req_arr);
        return strtoupper(hash('sha256', $_sign_str));
    }

    public function requestArr($_merch_order_id, $_amount, $_app, $_trade_type, $_is_h5)
    {
        ['request_data'=>$_req_arr,'static_url'=>$_static_url,'precreate_url'=>$_precreate_url] = $this->configData($_merch_order_id, $_amount, $_app, $_trade_type, $_is_h5);
//dd($_req_arr);
        $_req_arr['sign'] = $this->signKey($_req_arr);
        unset($_req_arr['key']);
        $_tmp['_request']['Request'] = $_req_arr;
        $_tmp['static_url']=$_static_url;
        $_tmp['precreate_url']=$_precreate_url;
        return $_tmp;
    }

    public function configData($_merch_order_id, $_amount, $_app, $_trade_type, $_is_h5)
    {

        $_configData = $this->_baseConfig->getConfig($_app, 'kbz', $_is_h5);
//dd($_configData);
        ['app_id' => $_app_id, 'merch_code' => $_merch_code, 'key' => $_key, 'static_url' => $_static_url, 'precreate__url' => $_precreate__url] = $_configData;

        $_notify_url = Config::get('constants.kbz.notify_url');

        switch ($_trade_type) {
            case 'h5':
            case 'app':
                $_refer_url = null;
                break;
            case 'web':
                $_refer_url = Config::get('constants.kbz.refer_url');
                break;
            default:
                $_refer_url = null;
        }

        $_req_arr = [
            'key' => $_key,
            'timestamp' => (string)time(),
            'method' => Config::get('constants.kbz.pre_create_method'),
            'notify_url' => $_notify_url,
            'nonce_str' => $this->nonce_str_generate(),
            'sign_type' => Config::get('constants.kbz.sign_type'),
            'sign' => '',
            'version' => '1.0',
            'biz_content' => [
                'merch_order_id' => $_merch_order_id,
                'merch_code' => $_merch_code,
                'appid' => $_app_id,
                'trade_type' => Config::get('constants.kbz.trade_type_' . $_trade_type),
                'total_amount' => (string)$_amount,
                'trans_currency' => Config::get('constants.kbz.trans_currency'),
                'timeout_express' => Config::get('constants.kbz.timeout_express'),
            ]
        ];

        if ($_refer_url !== null) {
            $_req_arr['biz_content']['refer'] = $_refer_url;
        }
//        $_req_arr['static_url'] = $_static_url;
//        $_req_arr['precreate_url'] = $_precreate__url;


        return ['request_data'=>$_req_arr,'static_url'=>$_static_url,'precreate_url'=>$_precreate__url];
    }

}


