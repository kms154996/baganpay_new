<?php

namespace App\Repository\KBZ\H5;

interface H5ServiceInteFace
{
    /************ Generate NONCE STR **********/
    public function nonce_str_generate();

    /** Sign String
     * @param $_config_data
     */
    public function signString($_config_data);

    /** Sign Key
     * @param $_config_data
     */
    public function singKey($_config_data);

    /** Create Request Array
     * @param $_trade_type
     * @param $_app
     * @param $_access_token
     */
    public function requestRequArr($_trade_type, $_app, $_access_token);

    /**
     * @param $_url
     * @param $_header
     * @param $_data
     * @return mixed
     */
    public function callCurl($_url, $_data);

    /**
     * @param $_open_id_resp
     * @return mixed
     */
    public function formatOpenIdResp($_open_id_resp);

    /** Call Form Url */
    public function formUrl($_openId);

    /** Order Info H5 */
    public function orderInfoH5($_prepay_id, $_app,$_nonce_str);

    /** Store OpenId */
    public function storeOpenId($_openId);

}
