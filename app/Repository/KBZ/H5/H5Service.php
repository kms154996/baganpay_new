<?php


namespace App\Repository\KBZ\H5;

use App\Model\Kbz\H5\OpenIdMtk;
use GuzzleHttp\Client;
use Config;

class H5Service implements H5ServiceInteFace
{

    protected $configData;

    /************ Generate NONCE STR **********/
    public function nonce_str_generate()
    {
        $length = 32;
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $nonce_str = '';
        for ($i = 0; $i < $length; $i++) {
            $nonce_str .= $characters[rand(0, $charactersLength - 1)];
        }

        return $nonce_str;
    }

    /** Sign String */
    public function signString($_config_data)
    {

        unset($_config_data['sign']);
        unset($_config_data['sign_type']);

        if (isset($_config_data['biz_content'])) {

            ['biz_content' => $_bizcontent] = $_config_data;
            $_data = array_merge($_bizcontent, $_config_data);
        } else {
            $_data = $_config_data;
        }

        unset($_data['biz_content']);

        ksort($_data);

        $key = $_data['key'];

        unset($_data['key']);

        $_signStr = '';


        foreach ($_data as $_key => $_value) {
            $_signStr .= $_key . '=' . $_value . '&';
        }

        return $_signStr . 'key=' . $key;

    }


    /** Sign Key */
    public function singKey($_config_data)
    {
        $_sign_str = $this->signString($_config_data);
//        return [hash('sha256', $_sign_str), $_sign_str];
        return strtoupper(hash('sha256', $_sign_str));

    }

    /** Create Request Array */
    public function requestRequArr($_trade_type, $_app, $_access_token)
    {
        $_config_data = $this->getConfigDdata($_trade_type, $_app, $_access_token);
        $_sign_key = $this->singKey($_config_data);
        unset($_config_data['key']);
        $_config_data['sign'] = $_sign_key;
        $_tmp['Request'] = $_config_data;
        return $_tmp;
    }


    /** Config Data */
    private function getConfigDdata($_trade_type, $_app, $_access_token)
    {
        return [
            'method' => Config::get('constants.kbz.h5.method'),
            'timestamp' => (string)time(),
            'nonce_str' => $this->nonce_str_generate(),
            'key' => Config::get('constants.kbz.h5.' . $_app . '.key'),
            'sign_type' => Config::get('constants.kbz.h5.sign_type'),
            'sign' => '',
            'version' => '1.0',
            'biz_content' => [
                'merch_code' => Config::get('constants.kbz.h5.' . $_app . '.merch_code'),
                'appid' => Config::get('constants.kbz.h5.' . $_app . '.app_id'),
                'trade_type' => $_trade_type,
                'access_token' => $_access_token,
                'resource_type' => 'OPENID'
            ]
        ];
    }

    /**
     * @param $_url
     * @param $_data
     * @return bool|string
     */
    public function callCurl($_url, $_data)
    {

        $_header = array();
        $_header[] = 'content-type:application/json';

        $ch = curl_init($_url);
        $json_str = json_encode($_data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $_header);
//        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_str);
        # Return response instead of printing.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    /** Format OpenId Resp */
    public function formatOpenIdResp($_open_id_resp)
    {
        $_resp_arr = json_decode($_open_id_resp, true);
        return $_resp_arr;
    }

    /** Call Form Url */
    public function formUrl($_openId)
    {
        $_post_data = [
            'mtk_agent' => Config::get('constants.kbz.h5.mtk.agent'),
            'mtk_agent_code' => Config::get('constants.kbz.h5.mtk.agent_code'),
            'mtk_agent_secret' => Config::get('constants.kbz.h5.mtk.agent_secret'),
            'user_agent' => $_openId
        ];

        $_url = Config::get('constants.kbz.h5.mtk.form_url');

//        dd($_url);

        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);
        $response = $client->post($_url,
            ['form_params' => $_post_data]
        );
        return $response = json_decode($response->getBody(), true);
    }

    /** Order Info H5
     * @param $_prepay_id
     * @param $_app
     * @return array
     */
    public function orderInfoH5($_prepay_id, $_app, $_nonce_str)
    {
        return [
            'prepay_id' => $_prepay_id,
            'merch_code' => Config::get('constants.kbz.h5.' . $_app . '.merch_code'),
            'appid' => Config::get('constants.kbz.h5.' . $_app . '.app_id'),
            'timestamp' => (string)time(),
            'nonce_str' => $_nonce_str,
            'key' => Config::get('constants.kbz.h5.' . $_app . '.key'),
        ];
    }

    /** Store Open Id */
    public function storeOpenId($_openId)
    {
        OpenIdMtk::create([
            'open_id' => $_openId,
            'time' => time()
        ]);
    }
}
