<?php


namespace App\Repository\KBZ;


use App\Model\Kbz\kbz_mtk_temp;
use App\Model\Kbz\kbz_wunzinn_temp;
use App\Model\Kbz\Olp;
use App\Model\Kbz\SeriesTemp;
use App\Model\Kbz\ShweAmyuTay;
use App\Repository\Config\BaseConfigInterface;
use Config;
use App\Model\Kbz\SNS\H5Temp;

class KbzService implements KbzServiceInterface
{
    public $_baseConfig;

    public function __construct(BaseConfigInterface $baseConfig)
    {
        $this->_baseConfig = $baseConfig;
    }

    /********* Get Model *********/
    public function getModel($_app)
    {
        list($_app_, $_temp_model) = $this->getAppData($_app);
        return $_temp_model;
    }

    /** Change Config */
    public function changeConfigDev($_app, $_is_h5)
    {
        list($_app_, $_temp_model) = $this->getAppData($_app);
        $_app_ = strtoupper($_app_);

        //        dd($this->_baseConfig->getConfig($_app,'kbz',true));

        if (env($_app_ . '_KBZ_ENV') === 'DEV') {
            config([
                'constants.kbz.static_url' => 'https://static.kbzpay.com/pgw/uat/pwa/#/?',
                'constants.kbz.precreate__url' => 'http://api.kbzpay.com/payment/gateway/uat/precreate',
            ]);
        }

        if ($_is_h5 && env('MTK_KBZ_H5_ENV') === 'UAT') {
            config([
                'constants.kbz.static_url' => 'https://static.kbzpay.com/pgw/uat/pwa/#/?',
                'constants.kbz.precreate__url' => 'http://api.kbzpay.com/payment/gateway/uat/precreate',
                'constants.kbz.mtk.app_id' => 'kp86e5fd99177840879729c12bab4654',
                'constants.kbz.mtk.merch_code' => '200042',
                'constants.kbz.mtk.key' => 'bit123456',
            ]);
        }
    }

    /** Get App Data */
    public function getAppData($_app)
    {

        switch ($_app) {
            case env('APP_MTK'):
                $_app_ = 'mtk';
                $_temp_model = new kbz_mtk_temp();
                break;
            case env('APP_WUNZINN'):
                $_app_ = 'wunzinn';
                $_temp_model = new kbz_wunzinn_temp();
                break;
            case env('APP_SERIES'):
                $_app_ = 'series';
                $_temp_model = new SeriesTemp();
                break;
            case env('APP_SNS'):
                $_app_ = 'sns';
                $_temp_model = new H5Temp();
                break;
            case env('APP_OLP'):
                $_app_ = 'olp';
                $_temp_model = new Olp();
                break;
            case env('APP_SAT'):
                $_app_ = 'sat';
                $_temp_model = new ShweAmyuTay();
                break;
        }

        return array($_app_, $_temp_model);
    }

    /********* Get CallBack Url *******/
    public function getCallBackUrl($_app)
    {
        $_callback_url = '';
        switch ($_app) {
            case env('APP_MTK'):
                $_callback_url = Config::get('constants.callback_url.MTK');
                break;
            case env('APP_WUNZINN'):
                $_callback_url = Config::get('constants.callback_url.WUZ');
                break;
            case env('APP_SERIES'):
                $_callback_url = Config::get('constants.callback_url.SER');
                break;
            case env('APP_SNS'):
                $_callback_url = Config::get('constants.callback_url.SNS');
                break;
            case env('APP_OLP'):
                $_callback_url = Config::get('constants.callback_url.OLP');
                break;
        }

        return $_callback_url;
    }
}
