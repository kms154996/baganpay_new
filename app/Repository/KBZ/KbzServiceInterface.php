<?php

namespace App\Repository\KBZ;

interface KbzServiceInterface
{
    public function getAppData($_app);

    public function getModel($_app);

    public function getCallBackUrl($_app);

    public function changeConfigDev($_app, $_is_h5);
}
