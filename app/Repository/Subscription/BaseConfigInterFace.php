<?php

namespace App\Repository\Subscription;

interface BaseConfigInterFace
{
    public function getConfigData($_app, $_operator);
    public function readJson($_fileName);
    public function getTables($_app): array;
}