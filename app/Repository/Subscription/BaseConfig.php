<?php


namespace App\Repository\Subscription;

use App\Model\Subscription\Telenor\TelenorMtkLog;
use App\Model\Subscription\Telenor\TelenorMtkTemp;
use Config;

class BaseConfig implements BaseConfigInterFace
{
    protected $_base;

    public function __construct(\App\Repository\Config\BaseConfigInterface $baseConfig)
    {
        $this->_base = $baseConfig;
    }

    public function getConfigData($_app, $_operator)
    {
        $_keyName = $this->readJson('keyName');

        ['app' => $_app, 'app_upper' => $_appUpper] = $this->_base->changeAppName($_app);

        $_env = $this->getEnv($_appUpper, $_operator);

        switch ($_env) {
            case env('DEV'):
                $_result = $this->getConfigDev($_operator, $_app, $_keyName);
                break;
            case env('PROD'):
                $_result = $this->getConfigProd($_operator, $_app, $_keyName);
                break;
        }

        $_result['env'] = $_env;
        return $_result;

    }

    /** Get Env */
    public function getEnv($_app, $_op)
    {
        return env($_app . '_' . $_op . '_ENV');
    }

    public function readJson($_fileName)
    {
        $_base_path = base_path() . '/app/Repository/Subscription';
        $_file_path = $_base_path . '/' . $_fileName . '.json';

        return json_decode(file_get_contents($_file_path), true);
    }

    public function getConfigDev($_operator, $_app, $_keyName)
    {
        $_devKey = $this->readJson('devKey');
        return array_merge($_devKey[$_operator]['url'],
            $_devKey[$_operator][$_app]);

    }

    public function getConfigProd($_operator, $_app, $_keyName)
    {
        $_devKey = $this->readJson('devKey');
        foreach ($_keyName[$_operator]['url'] as $_key) {
//            dd($_key);
            $_config[$_key] = Config::get('subscription.' . $_operator . '.' . $_key);
        }
        foreach ($_keyName[$_operator]['key'] as $_key) {


            $_config[$_key] = Config::get('subscription.' . $_operator . '.' . $_app . '.' . $_key);
        }

        return $_config;

    }

    public function getTables($_app): array
    {
        switch ($_app) {
            case env('APP_MTK'):
                $_temp = new TelenorMtkTemp();
                $_log = new TelenorMtkLog();
                break;
        }

        return [
            'temp' => $_temp,
            'log' => $_log
        ];
    }
}