<?php


namespace App\Repository\One;


use App\Model\Onepay\OnePay_mtk_Redirect;
use App\Model\Onepay\OnePay_mtk_Temp;
use App\Model\Onepay\SeriesRedirect;
use App\Model\Onepay\SeriesTemp;
use App\Model\Onepay\WunzinnRedirect;
use App\Model\Onepay\WunzinnTemp;
use DB;
use Config;

class PayService implements PayServiceInterface
{
    /*
        * Select Temp Table
        */
    public function selectTemp($_app)
    {
        switch ($_app) {
            case env('APP_MTK'):
                $_temp = new OnePay_mtk_Temp();
                break;
            case env('APP_WUNZINN'):
                $_temp = new WunzinnTemp();
                break;
            case env('APP_SERIES'):
                $_temp = new SeriesTemp();
                break;
        }
        return $_temp;
    }

    /*
    * Get Model
    */
    public function getModel($_trans_id)
    {
        $_app = DB::select('select app from onepay_requests where trans_id=?', [$_trans_id])[0]->app;

        switch ($_app) {
            case env('APP_WUNZINN'):
                $_model['redirect'] = new WunzinnRedirect();
                $_model['temp'] = new WunzinnTemp();
                break;
            case env('APP_MTK'):
                $_model['redirect'] = new OnePay_mtk_Redirect();
                $_model['temp'] = new  OnePay_mtk_Temp();
                break;
            case  env('APP_SERIES'):
                $_model['redirect'] = new SeriesRedirect();
                $_model['temp'] = new SeriesTemp();
                break;
        }

        return $_model;
    }

    /********* Get CallBack Url *******/
    public function getCallBackUrl($_trans_id)
    {
        $_app = DB::select('select app from onepay_requests where trans_id=?', [$_trans_id])[0]->app;
        $_callback_url = '';
        switch ($_app) {
            case env('APP_MTK'):
                $_callback_url = Config::get('constants.callback_url.MTK');
                break;
            case env('APP_WUNZINN'):
                $_callback_url = Config::get('constants.callback_url.WUZ');
                break;
            case env('APP_SERIES'):
                $_callback_url = Config::get('constants.callback_url.SER');
        }

        return $_callback_url;
    }
}
