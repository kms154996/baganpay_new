<?php

namespace App\Repository\MYTEL;

use App\Model\MyTel\MtkTemp;
use App\Model\MyTel\MyTelRequest;
use App\Model\MyTel\SeriesTemp;
use Config;
use phpseclib\Crypt\RSA;
use App\Traits\helperTrait;
use DB;

include 'RSAUtils.php';


class PayService implements PayServiceInterface
{
    use helperTrait;

    /** Request Arr
     * @param $_send_data_array
     * @param $_generate_trans_id
     * @param $_service_id
     * @param $_service_code
     * @return array
     */
    public function requestArr($_send_data_array, $_generate_trans_id, $_service_id, $_service_code)
    {
        return [
            "partnerServiceId" => $_service_id,
            "serviceCode" => $_service_code,
            "currentMillisecondsStr" => (string)round(microtime(true) * 1000),
            "msisdn" => (string)$_send_data_array['phone'],
            "amount" => (string)$_send_data_array['amount'],
            "payDescription" => "BaganPay",
            "orderId" => $_generate_trans_id,
        ];
    }

    /** Hash Request Arr
     * @param $_arr
     * @param $_key
     * @return string
     */
    public function hashArr($_arr, $_key)
    {
        $_str = json_encode($_arr);
        return hash_hmac('sha256', $_str, $_key, false);
    }

    /** Create Encode Url
     * @param $_arr
     * @return string
     */
    public function createUrl($_arr)
    {
        $_json = json_encode($_arr);
//       return $_json;
        return urlencode($_json);
    }

    public function rsaEncrypt($_arr, $_key)
    {
        $_json = json_encode($_arr);
        $rsa = new RSA();
        $rsa->loadKey($_key);
        $rsa->setEncryptionMode(2);
        $plaintext = $_json;
        $ciphertext = $rsa->encrypt($plaintext);


        return (base64_encode($ciphertext));
    }

    /** Store Request */
    public function storeRequest($_arr)
    {
        $_arr['trans_id'] = $this->my_decrypt($_arr['trans_id']);
        unset($_arr['payment_method']);
        MyTelRequest::create($_arr);
    }

    /** Temp Model */
    public function tempModel($_app)
    {
        switch ($_app) {
            case env('APP_MTK'):
                $_temp = new MtkTemp();
                break;
            case env('APP_SERIES'):
                $_temp = new SeriesTemp();
                break;
        }
        return $_temp;
    }

    /** Store Temp */
    public function storeTemp($_model, $_arr)
    {
        unset($_arr['payment_method']);
        unset($_arr['app']);
        $_decrypt_trans_id = $this->my_decrypt($_arr['trans_id']);
        $_arr['trans_id'] = $_decrypt_trans_id;
        $_arr['order_id'] = $_arr['bp_trans_id'];
        $_arr['msisdn'] = $_arr['phone'];
        unset($_arr['phone']);
        $_model::create($_arr);
    }

    /** Get App Data
     * @param $_orderId
     * @return mixed
     */
    public function getAppData($_orderId)
    {
        return DB::select('select app,trans_id,bp_trans_id,amount from my_tel_requests where bp_trans_id=?', [$_orderId])[0];
    }

    /** Update Temp From Callback */
    public function updateCallBackTemp($_temp, $_pay_status, $_bp_trans_id, $_getContent)
    {
        $_update = $_temp::where('order_id', $_bp_trans_id)->first();
        $_update->status = $_pay_status;
        $_update->redirect_json = $_getContent;
        $_update->save();
    }
}
