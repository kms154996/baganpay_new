<?php

namespace App\Repository\MYTEL;

interface PayServiceInterface
{
    /** Create Request Arr */
    public function requestArr($_send_data_array, $_generate_trans_id, $_service_id, $_service_code);

    /** Create Hash Value */
    public function hashArr($_arr, $_key);

    /** Create Encode Url */
    public function createUrl($_arr);

}
