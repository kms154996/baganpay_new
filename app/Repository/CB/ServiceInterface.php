<?php

namespace App\Repository\CB;

interface ServiceInterface
{
    public function requestArr($_configData, $_arr, $_sign);

    public function sign($_configData, $_arr);
}
