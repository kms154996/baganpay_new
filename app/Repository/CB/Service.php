<?php


namespace App\Repository\CB;

use App\Model\CbPay\CbMtkTemp;
use App\Model\CbPay\CbRequest;
use App\Model\CbPay\CbSeriesTemp;
use Config;
use DB;

class Service implements ServiceInterface
{
    /** Return Request Arr */
    public function requestArr($_configData, $_arr, $_sign)
    {
        return [
            "authenToken" => $_configData['authenToken'],
            "ecommerceId" => $_configData['ecommerceId'],
            "subMerId" => $_configData['subMerId'],
            "transactionType" => "1",
            "orderId" => $_arr['bp_trans_id'],
            "orderDetails" => "Pay To BaganIt",
            "amount" => (string)$_arr['amount'],
            "currency" => "MMK",
            "notifyUrl" => Config::get('constants.cb.callback_url'),
            "signature" => $_sign
        ];
    }

    /** Return Sign */
    public function sign($_configData, $_arr)
    {
        $_signStr = $_configData['authenToken'] . '&' . $_configData['ecommerceId'] . '&' . $_configData['subMerId'] . '&' . $_arr['bp_trans_id'] . '&' . $_arr['amount'] . '&' . 'MMK';

        return hash('sha256', $_signStr);
    }

    /** Request To Cb */
    public function reqeustCB($_url, $_arr)
    {
//        return ($_url);
        $client = new \GuzzleHttp\Client([
            'http_errors' => false,
            'verify' => false
        ]);

        $response = $client->request('post', $_url, [
            'headers' => [
                'content-type' => "application/json",
            ],
            'json' => $_arr
        ]);

        return $response->getBody();
    }

    /** Store Request */
    public function storeRequest($_arr)
    {
        CbRequest::create([
            'trans_id' => $_arr['trans_id'],
            'bp_trans_id' => $_arr['bp_trans_id'],
            'amount' => $_arr['amount'],
            'app' => $_arr['app']
        ]);
    }

    /** Store Temp */
    public function storeTemp($_temp, $_arr)
    {
        $_temp::create([
            'trans_id' => $_arr['trans_id'],
            'orderId' => $_arr['bp_trans_id'],
            'amount'=>$_arr['amount']
        ]);
    }

    /** Update Temp */
    public function updateTemp($_temp, $_bp_trans_id, $_respArr)
    {
        $_update_data = $_temp::where('orderId', $_bp_trans_id)->first();
        $_update_data->generateRefOrder = $_respArr['generateRefOrder'];
        $_update_data->generateRefOrderCode = $_respArr['code'];
        $_update_data->save();
    }

    /** Select Temp */
    public function getTemp($_app)
    {
        switch ($_app) {
            case env('APP_MTK'):
                return new CbMtkTemp();
            case env('APP_SERIES'):
                return new CbSeriesTemp();
        }
    }

    /** Update Temp Callback */
    public function updateTempCallback($_temp, $_bp_trans_id, $_transStatus)
    {
        $_updateData = $_temp::where('orderId', $_bp_trans_id)->first();

        if ($_transStatus === 'S') {
            $_updateData->pay_status = 1;
        }
        $_updateData->save();

    }

    /** Get App */
    public function getApp($_trans_id)
    {
        return DB::select('select trans_id,bp_trans_id,app from cb_requests where bp_trans_id=?', [$_trans_id])[0];
    }
}
