<?php


namespace App\Repository\Wave2;

use App\Model\Wave2\Wave2MtkTemp;
use App\Model\Wave2\Wave2SeriesTemp;
use Config;

class Service implements ServiceInterface
{
    /** Request Creation
     * @param $_arr
     * @param $frontend_result_url
     * @return array
     */
    public function requestCreation($_arr, $frontend_result_url)
    {
        return [
            // Time to Live for Transaction in seconds
                'time_to_live_in_seconds' => "3000",

            // string - Merchant Name for Payment Screen
            'merchant_name' => "BaganIT",

            // string - Merchant id provided by Wave Money
            'merchant_id' => $_arr['merchantId'],

            // unsigned integer - Order id provided Merchant
            'order_id' => (string)$_arr['bp_trans_id'],
            // unsigned integer - Total Amount of transaction
            'amount' => (string)$_arr['amount'],

            // string - mendatory backend url for Payment Service
            'backend_result_url' => 'https://pay.baganit.com/api/wave2/callback',

//                Config::get('constants.wave2.callback_url'),

            // string - mandatory frontend url for Payment Service
            'frontend_result_url' => $frontend_result_url,

            // string - Unique Merchant Reference ID for Transaction, reference_id is unsigned integer
            'merchant_reference_id' => (string)$_arr['bp_trans_id'],

            // string - Payment Description for Payment Screen from Merchant
            'payment_description' => "Pay To BaganIT"
        ];
    }

    /** Create Item Json String */
    public function itemsJson($_item)
    {
        return json_encode($_item);
    }

    /** Hash Value For PayLoad */
    public function hashPayLoad($data, $_key)
    {
        return hash_hmac('sha256', implode("", [
            $data['time_to_live_in_seconds'],
            $data['merchant_id'],
            $data['order_id'],
            $data['amount'],
            $data['backend_result_url'],
            $data['merchant_reference_id'],
        ]), $_key);
    }


    /** Request Wave
     * @param $data
     * @param $items
     * @param $hash
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function requestWave($data, $items, $hash)
    {

        $client = new \GuzzleHttp\Client([
            'http_errors' => false,
            'verify' => false
        ]);

        $response = $client->request('post', $data['request_url'], [
            'headers' => [
                'Accept' => "application/json",
            ],
            'form_params' => [
                "time_to_live_in_seconds" => $data['time_to_live_in_seconds'],
                "merchant_id" => $data['merchant_id'],
                "order_id" => $data['order_id'],
                "merchant_reference_id" => $data['merchant_reference_id'],
                "frontend_result_url" => $data['frontend_result_url'],
                "backend_result_url" => $data['backend_result_url'],
                "amount" => $data['amount'],
                "payment_description" => $data['payment_description'],
                "merchant_name" => $data['merchant_name'],
                "items" => $items,
                "hash" => $hash
            ]
        ]);

        return $response->getBody();
    }

    /** Select Temp
     * @param $_app
     * @param $_h5
     * @return Wave2MtkTemp|Wave2SeriesTemp
     */
    public function selectTemp($_app, $_h5)
    {
        if (!$_h5)
            switch ($_app) {
                case env('APP_MTK'):
                    return new Wave2MtkTemp();
                case env('APP_SERIES'):
                    return new Wave2SeriesTemp();
            }
    }

}

