<?php

namespace App\Repository\Wave2;

interface ServiceInterface
{
    /** Request Creation
     * @param $_arr
     * @param $frontend_result_url
     */
    public function requestCreation($_arr,$frontend_result_url);

    /** Create Item Json String */
    public function itemsJson($_item);

    /** Hash Value For PayLoad */
    public function hashPayLoad($data, $_key);

    /** Request Wave
     * @param $data
     * @param $items
     * @param $hash
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function requestWave($data, $items, $hash);

    /** Select Temp
     * @param $_app
     * @param $_h5
     */
    public function selectTemp($_app, $_h5);
}
