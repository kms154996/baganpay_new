<?php

namespace App\Traits;


trait requestStore
{
    public function storeRequest($_data)
    {
        $_data['request_model']::create([
            'phone' => $_data['phone'],
            'trans_id' => $_data['trans_id'],
            'amount' => $_data['amount'],
            'timetick' => time()
        ]);
    }
}