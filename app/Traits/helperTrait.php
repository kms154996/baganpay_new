<?php

namespace App\Traits;

use Config;

trait helperTrait
{
    public function callCurl($_url, $_header, $_data)
    {

        $ch = curl_init($_url);
        $json_str = json_encode($_data);
//dd($json_str);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $_header);
//        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_str);
        # Return response instead of printing.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    public function encryptTransId($_trans_id)
    {
        $cipher_method = 'aes-128-ctr';
        $enc_key = openssl_digest(php_uname(), 'SHA256', TRUE);
        $enc_iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($cipher_method));
        $crypted_token = openssl_encrypt($_trans_id, $cipher_method, $enc_key, 0, $enc_iv) . "::" . bin2hex($enc_iv);
        unset($token, $cipher_method, $enc_key, $enc_iv);
        return $crypted_token;
    }

    public function decryptTransId($token)
    {
        if (isset($token)) {
            [$crypted_token, $enc_iv] = explode("::", $token);;
            $cipher_method = 'aes-128-ctr';
            $enc_key = openssl_digest(php_uname(), 'SHA256', TRUE);
            $token = openssl_decrypt($crypted_token, $cipher_method, $enc_key, 0, hex2bin($enc_iv));
            unset($crypted_token, $cipher_method, $enc_key, $enc_iv);
            return $token;
        }
    }

    function my_encrypt($data)
    {
        $key = config::get('constants.key_for_trans_id');
        // Remove the base64 encoding from our key
        $encryption_key = base64_decode($key);
        // Generate an initialization vector
        $iv = "0011001100110011";
        // Encrypt the data using AES 256 encryption in CBC mode using our encryption key and initialization vector.
        $encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
        // The $iv is just as important as the key for decrypting, so save it with our encrypted data using a unique separator (::)
        return base64_encode($encrypted . '::' . $iv);
    }

    function my_decrypt($data)
    {

        $key = config::get('constants.key_for_trans_id');

        $encryption_key = base64_decode($key);

        try {
            [$encrypted_data, $iv] = explode('::', base64_decode($data), 2);
            return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);

        } catch (\Exception $e) {
            return false;
        }


    }

    /********* Check Operator *********/
    function checkOperator($phone)
    {
        $phone_number = $phone;
        $first_number = substr($phone_number, 0, 1);
        $start_number = substr($phone_number, 0, 3);
        $phone_length = strlen($phone_number);
        $operator = 0;

        // replace with 95 with 0 if phone number starts with 09
        if ($first_number == 0) {
            $phone_number = '95' . substr($phone_number, 1);
        } elseif ($first_number == 9) {
            /** @var TYPE_NAME $start_number */
            if ($start_number == '959') {
                if ($phone_length == 8) { // 959 can be 9 + mpt
                    $operator = 1; //MPT
                    return $this->sendSuccessResponse($operator, '95' . $phone_number);
                } elseif ($phone_length == 9) { // 959 can be a pure ooredoo
                    $operator = 2; // Ooredoo
                    return $this->sendSuccessResponse($operator, $phone_number);
                }
            } else { // add 95 because already started with 9

                if ($this->isOoredoo('959' . $phone_number)) {
                    $operator = 2; // Ooredoo
                    return $this->sendSuccessResponse($operator, '959' . $phone_number);
                }
                $phone_number = '95' . $phone_number;
                // return $phone_number;
                // return $this->sendFailResponse();
            }
        } else {
            $phone_number = '959' . $phone_number;
        }

        if ($this->is_mpt($phone_number)) {
            $operator = 1; // MPT
        } elseif ($this->isTelenor($phone_number)) {
            $operator = 5; //Telenor
        } elseif ($this->isOoredoo($phone_number)) {
            $operator = 2; // Ooredoo
        } elseif ($this->isMytel($phone_number)) {
            $operator = 6; // Mytel
        } elseif ($this->isMec($phone_number)) {
            $operator = 4; // MEC
        }
        if (in_array($operator, [0, 3, 4], true)) {
            return $this->sendFailResponse();
        }
        return $this->sendSuccessResponse($operator, $phone_number);
    }


    function is_mpt($phone)
    {

        $is_mpt = false;

        $first_4 = substr($phone, 0, 4);
        $first_5 = substr($phone, 0, 5);

        $ph_length = strlen($phone);

        switch ($first_4) {

            case '9592':
                if ($ph_length == 10) {
                    $is_mpt = true;
                } elseif ($ph_length == 12) {
                    if ($first_5 == '95925' || $first_5 == '95926') {
                        $is_mpt = true;
                    }
                }

                break;


            case '9594':
                if ($ph_length == 11) {
                    if ($first_5 == '95941' || $first_5 == '95943') {
                        $is_mpt = true;
                    }

                } elseif ($ph_length == 12) {
                    if ($first_5 == '95940' || $first_5 == '95942' || $first_5 == '95944' || $first_5 == '95945') {
                        $is_mpt = true;
                    }
                }

                break;

            case '9595':
                if ($ph_length == 10) {
                    $is_mpt = true;
                }

                break;


            case '9598':
                if ($ph_length == 12) {
                    if ($first_5 == '95988' || $first_5 == '95989') {
                        $is_mpt = true;
                    }

                }

                break;


            default:
                # code...
                break;
        }
        return $is_mpt;
    }

    function isTelenor($phone = "")
    {
        $is_telenor = false;
        $start_number = substr($phone, 0, 4);
        $phone_length = strlen($phone);
        if ($start_number == "9597" && $phone_length == 12) {
            $is_telenor = true;
        }
        return $is_telenor;
    }

    public function isOoredoo($phone = "")
    {
        $is_ooredoo = false;
        $start_number = substr($phone, 0, 4);
        $phone_length = strlen($phone);
        if ($start_number == "9599" && $phone_length == 12) {
            $is_ooredoo = true;
        }
        return $is_ooredoo;
    }

    public function isMyTel($phone = "")
    {
        $is_mytel = false;
        $start_number = substr($phone, 0, 4);
        $phone_length = strlen($phone);
        if ($start_number == "9596" && $phone_length == 12) {
            $is_mytel = true;
        }
        return $is_mytel;
    }

    public function isMec($phone = "")
    {
        $is_mec = false;
        $start_number = substr($phone, 0, 4);
        $phone_length = strlen($phone);
        if ($start_number == "9593") {
            if ($phone_length == 11) {
                $is_mec = true;
            } elseif ($start_number == "95934" && $phone_length == 12) {
                $is_mec = true;
            }
        }
        return $is_mec;
    }

    private function sendSuccessResponse($operator, $phone_number)
    {
        return [
            "status" => 1,
            "message" => "Successful",
            "operator_name" => $operator,
            "phone_number" => $phone_number
        ];
    }

    private function sendFailResponse()
    {
        return [
            "status" => 0,
            'operator_name' => 0,
            "message" => "Operator Not Support",
            "phone_number" => 0
        ];
    }


}
