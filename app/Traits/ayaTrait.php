<?php


namespace App\Traits;


trait ayaTrait
{
    /************** CURL ************
     * @param $header
     * @param $json_arr
     * @param $url
     * @return bool|string
     */
    public function callCurl($header,$_data ,$url)
    {

        $ch_data = curl_init($url);
        $json_str = json_encode($_data);


        curl_setopt($ch_data, CURLOPT_HTTPHEADER,$header);
        curl_setopt($ch_data, CURLOPT_POSTFIELDS, $json_str);
        curl_setopt($ch_data, CURLOPT_CUSTOMREQUEST, 'POST');

        # Return response instead of printing.
        curl_setopt($ch_data, CURLOPT_RETURNTRANSFER, true);
        # Send request.
        $response = curl_exec($ch_data);
//        curl_close($ch_data);

        return $response;
    }
}
