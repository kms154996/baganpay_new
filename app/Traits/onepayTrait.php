<?php


namespace App\Traits;

use App\Traits\helperTrait;
use Zttp\Zttp;

trait onepayTrait
{
    use helperTrait;

    /****** Hash Value For Verify Phone ****
     * @param $_hash_arr
     * @return string
     */
    public function verifyPhoneHash($_hash_arr): string
    {
        ['Channel' => $Channel, 'MerchantUserId' => $MerchantUserId, 'OnepayPhoneNo' => $OnepayPhoneNo, 'secret_key' => $secret_key] = $_hash_arr;

        $_hash_string = $Channel . $MerchantUserId . $OnepayPhoneNo;

        $_hash_code = hash_hmac('sha1', $_hash_string, $secret_key, false);

        return strtoupper($_hash_code);

    }

    /****** Hash Value For Direct Payment *****
     * @param $_data
     * @return string
     */
    public function paymentDirectHash($_data): string
    {
        ['config_data' => $_config_data, 'request_data' => $_request_data, 'seq_str' => $_seq_str, 'trans_id' => $_trans_id] = $_data;

        /******** Destruct Config Data Array *******/
        [
            'version' => $version, 'channel' => $channel, 'user_id' => $user_id,
            'call_back_url' => $_call_back_url, 'expire_second' => $_expire_second,
            'remark' => $_remark, 'secret_key' => $secret_key
        ] = $_config_data;

        /******** Request Data *******/
        $phone = $_request_data['phone'];
        $amount = $_request_data['amount'];

        $_hash_string = $version . $channel . $user_id . $phone . $amount . $_remark . $_trans_id . $_seq_str . $_call_back_url . $_expire_second;

        $_hash_code = hash_hmac('sha1', $_hash_string, $secret_key, false);

        return strtoupper($_hash_code);

    }

    /********* Call One Pay Server ******
     * @param $_data
     * @return bool|string
     */
    public function callOnePay($_data)
    {
        $header = array();
        $header[] = 'Content-Type:application/json';

        ['url' => $_url, 'data' => $json_str] = $_data;

        $_json_str = json_encode($json_str);

        $ch = curl_init($_url);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $_json_str);
        # Return response instead of printing.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    /********* Generate Sequence String ********/
    public function generateSeqString()
    {
        $length = 32;
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $seq_str = '';
        for ($i = 0; $i < $length; $i++) {
            $seq_str .= $characters[rand(0, $charactersLength - 1)];
        }

        return $seq_str;
    }

    /********** Get Direct Payment Request Array *******
     * @param $_data
     * @return array
     */
    public function requestArr($_data): array
    {
        ['config_data' => $_config_data, 'request_data' => $_request_data] = $_data;

        $_seq_str = $this->generateSeqString();

        $_trans_id = $_request_data['trans_id'];


        $_request_arr = [];

        $_request_arr = $this->requestArray($_data, $_seq_str, $_trans_id, $_config_data, $_request_arr, $_request_data);

        return $_request_arr;

    }

    /**
     * @param $_data
     * @param string $_seq_str
     * @param string $_trans_id
     * @param $_config_data
     * @param array $_request_arr
     * @param $_request_data
     * @return array
     */
    public function requestArray($_data, string $_seq_str, string $_trans_id, $_config_data, array $_request_arr, $_request_data): array
    {
        $_data['seq_str'] = $_seq_str;
        $_data['trans_id'] = $_trans_id;

        $_request_arr['Version'] = $_config_data['version'];
        $_request_arr['Channel'] = $_config_data['channel'];
        $_request_arr['MerchantUserId'] = $_config_data['user_id'];
        $_request_arr['InvoiceNo'] = $_trans_id;
        $_request_arr['SequenceNo'] = $_seq_str;
        $_request_arr['Amount'] = $_request_data['amount'];
        $_request_arr['Remark'] = $_config_data['remark'];
        $_request_arr['WalletUserID'] = $_request_data['phone'];
        $_request_arr['CallBackUrl'] = $_config_data['call_back_url'];
        $_request_arr['ExpiredSeconds'] = $_config_data['expire_second'];
        $_request_arr['HashValue'] = $this->paymentDirectHash($_data);
        return $_request_arr;
    }

}
