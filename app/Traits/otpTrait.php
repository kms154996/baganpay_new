<?php


namespace App\Traits;

use Config;

trait  otpTrait
{
    public function requestOtpServer($_data)
    {
        $_url = Config::get('subscription.request_otp');
        return $this->getDataUpdate($_url, $_data);
    }

    public function verifyOtpServer($_data)
    {
        $_url = Config::get('subscription.verify_otp');
        return $this->getDataUpdate($_url, $_data);
    }

    public function getDataUpdate($_url, $_data, $_headers = null)
    {
        $data = json_encode($_data);

        $cu = curl_init();

        curl_setopt_array($cu, array(
            CURLOPT_URL => $_url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_RETURNTRANSFER => true
        ));

        curl_setopt($cu, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data),
            $_headers
        ));
        $_res = curl_exec($cu);

        $response = json_decode($_res, true);

        curl_close($cu);
        return $response;
    }

    /** Generate TransId */
    public function getTransId($_prefix)
    {
        $char = "abcdefghijknmopqrestuvyxyz12345678910";
        return $_trans_id = $_prefix . '_' . substr(uniqid(str_shuffle($char), true), 0, 9);
    }
}