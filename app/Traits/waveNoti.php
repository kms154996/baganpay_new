<?php

namespace App\Traits;

trait waveNoti
{
    public function sendNoti($_data)
    {
        $header = array();
        $header[] = 'content-type:application/json';
        $header[] = 'cache-control: no-cache';
        $header[] = 'callbackurl: ' . $_data['call_back_url'];
        $header[] = 'client_id: ' . $_data['client_id'];
        $header[] = 'client_secret: ' . $_data['client_secret'];
        $header[] = 'paymentrequestid: "' . $_data['requestID'] . '"';

        $_phone = substr($_data['phone'], 2);

        $json_arr = array(
            "purchaserMsisdn" => $_phone,
            "purchaserAmount" => $_data['amount'],
            "timeOut" => 1000000,
            "hashValue" => $_data['hash_value']
        );

//        return array($_phone, $_data,$header);

        return $this->callCurlWave($header, $json_arr, $_data['url']);

    }


    /************** CURL *************/
    public function callCurlWave($header, $json_arr, $url)
    {

        $ch_data = curl_init($url);
        $json_str = json_encode($json_arr);

        curl_setopt($ch_data, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch_data, CURLOPT_POSTFIELDS, $json_str);
        # Return response instead of printing.
        curl_setopt($ch_data, CURLOPT_RETURNTRANSFER, true);
        # Send request.
        $response = curl_exec($ch_data);
        curl_close($ch_data);

        return $response;
    }


}
