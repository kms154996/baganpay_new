<?php

namespace App\Traits;

use App\Model\Kbz\kbz_mtk_temp;
use App\Model\Kbz\kbz_request;
use App\Model\Kbz\kbz_wunzinn_temp;
use App\Model\Kbz\SeriesTemp;
use Config;

trait kbzTrait
{
    /************ Generate NONCE STR **********/
    public function nonce_str_generate()
    {
        $length = 32;
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $nonce_str = '';
        for ($i = 0; $i < $length; $i++) {
            $nonce_str .= $characters[rand(0, $charactersLength - 1)];
        }

        return $nonce_str;
    }

    /************ Generate SIGN KEY ************
     * @param $_app_id
     * @param $_key
     * @param $_merch_order_id
     * @param $_amount
     * @param $_timestamp
     * @param $_trade_type
     * @param $_notify_url
     * @return string
     */
    public function singKey($_app_id, $_key, $_merch_code, $_merch_order_id, $_amount, $_nonce_str, $_timestamp, $_trade_type, $_notify_url)
    {

        $_data = $this->configVariable($_trade_type);

        $_sign_key = $this->getSignStr($_app_id, $_key, $_merch_code, $_merch_order_id, $_amount, $_nonce_str, $_timestamp, $_notify_url, $_data);

        return strtoupper($_sign_key);

    }

    /********** Generate Pre Create Request Array *********
     * @param $_merch_order_id
     * @param $_amount
     * @param $_app
     * @param $_trade_type
     * @return array
     */
    public function createRequestArray($_merch_order_id, $_amount, $_app, $_trade_type): array
    {
        $_timestamp = time();

        $_config_data = $this->configVariable($_trade_type);

        $_nonce_str = $this->nonce_str_generate();

        [$_notify_url, $_app_id, $_key, $_merch_code, $_refer_url] = $this->appConfig($_app, $_trade_type);

        $_sing = $this->singKey($_app_id, $_key, $_merch_code, $_merch_order_id, $_amount, $_nonce_str, $_timestamp, $_trade_type, $_notify_url);

        $_request_array = $this->getArrRequest($_timestamp, $_config_data, $_notify_url, $_nonce_str, $_sing);

        $_biz_content = $this->getBizContent($_merch_order_id, $_amount, $_merch_code, $_app_id, $_config_data);

        /** @var TYPE_NAME $_refer_url */
        $_biz_content = $this->bizReferUrl($_refer_url, $_biz_content);

        $_request_array['Request']['biz_content'] = $_biz_content;
        $_request_array['_request']['Request'] = $_request_array['Request'];
        unset($_request_array['Request']);

        return $_request_array;


    }

    /********* Return Config Variable *******
     * @param $_trade_type
     * @return mixed
     */
    public function configVariable($_trade_type)
    {
        $data['_method'] = Config::get('constants.kbz.pre_create_method');
        $data['_timeout_express'] = Config::get('constants.kbz.timeout_express');
        $data['_trade_type'] = Config::get('constants.kbz.trade_type_' . $_trade_type);
        $data['_trans_currency'] = Config::get('constants.kbz.trans_currency');

        return $data;
    }

    /********* Insert into Request Table *********/
    public function createRequest($_arr)
    {
        kbz_request::create([
            'trans_id' => $_arr['trans_id'],
            'bp_trans_id' => $_arr['bp_trans_id'],
            'app' => $_arr['app'],
            'amount' => $_arr['amount'],
            'timetick' => time()
        ]);
    }

    /********* Get Model *********/
    public function getModel($_app)
    {
        $_temp_model = '';
        switch ($_app) {
            case env('APP_MTK'):
                $_temp_model = new kbz_mtk_temp();
                break;
            case env('APP_WUNZINN'):
                $_temp_model = new kbz_wunzinn_temp();
                break;
            case env('APP_SERIES'):
                $_temp_model = new SeriesTemp();
                break;

        }

        return $_temp_model;
    }

    /*********** Insert into Temp ***********
     * @param $_model
     * @param $_arr
     */
    public function insertTemp($_model, $_arr)
    {
        $_model::create([
            'merch_order_id' => $_arr['trans_id'],
            'trans_id' => $_arr['bp_trans_id'],
            'amount' => $_arr['amount'],
            'timetick' => time()
        ]);
    }

    /**
     * @param int $_timestamp
     * @param $_config_data
     * @param $_notify_url
     * @param string $_nonce_str
     * @param string $_sing
     * @return array
     */
    private function getArrRequest(int $_timestamp, $_config_data, $_notify_url, string $_nonce_str, string $_sing): array
    {
        return [
            'Request' => [
                'timestamp' => (string)$_timestamp,
                'method' => $_config_data['_method'],
                'notify_url' => $_notify_url,
                'nonce_str' => $_nonce_str,
                'sign_type' => Config::get('constants.kbz.sign_type'),
                'sign' => $_sing,
                'version' => '1.0'
            ]
        ];
    }

    /**
     * @param $_app
     * @param $_trade_type
     * @return array
     */
    private function appConfig($_app, $_trade_type): array
    {
        switch ($_app) {
            case env('APP_WUNZINN'):
                $_app_config = 'wunzinn';
                break;
            case env('APP_MTK'):
                $_app_config = 'mtk';
                break;
            case env('APP_SERIES'):
                $_app_config = 'series';
        }

        $_notify_url = Config::get('constants.kbz.notify_url');
        $_app_id = Config::get('constants.kbz.' . $_app_config . '.app_id');
        $_key = Config::get('constants.kbz.' . $_app_config . '.key');
        $_merch_code = Config::get('constants.kbz.' . $_app_config . '.merch_code');

        switch ($_trade_type) {
            case 'app':
                $_refer_url = null;
                break;
            case 'web':
                $_refer_url = Config::get('constants.kbz.refer_url');
                break;
        }
        /** @var TYPE_NAME $_refer_url */
        return array($_notify_url, $_app_id, $_key, $_merch_code, $_refer_url);
    }

    /**
     * @param $_app_id
     * @param $_key
     * @param $_merch_code
     * @param $_merch_order_id
     * @param $_amount
     * @param $_nonce_str
     * @param $_timestamp
     * @param $_notify_url
     * @param $_data
     * @return string
     */
    private function getSignStr($_app_id, $_key, $_merch_code, $_merch_order_id, $_amount, $_nonce_str, $_timestamp, $_notify_url, $_data): string
    {
        return hash('sha256', 'appid=' . $_app_id
            . '&merch_code=' . $_merch_code
            . '&merch_order_id=' . $_merch_order_id
            . '&method=' . $_data['_method']
            . '&nonce_str=' . $_nonce_str
            . '&notify_url=' . $_notify_url
            . '&timeout_express=' . $_data['_timeout_express']
            . '&timestamp=' . (string)$_timestamp
            . '&total_amount=' . (string)$_amount
            . '&trade_type=' . $_data['_trade_type']
            . '&trans_currency=' . $_data['_trans_currency']
            . '&version=1.0'
            . '&key=' . $_key);
    }

    /**
     * @param $_merch_order_id
     * @param $_amount
     * @param $_merch_code
     * @param $_app_id
     * @param $_config_data
     * @return array
     */
    private function getBizContent($_merch_order_id, $_amount, $_merch_code, $_app_id, $_config_data): array
    {
        return [
            'merch_order_id' => $_merch_order_id,
            'merch_code' => (string)$_merch_code,
            'appid' => (string)$_app_id,
            'trade_type' => $_config_data['_trade_type'],
//        dd($_precreate_resp);

            'total_amount' => (string)$_amount,
            'trans_currency' => $_config_data['_trans_currency'],
            'timeout_express' => $_config_data['_timeout_express']
        ];
    }

    /**
     * @param TYPE_NAME $_refer_url
     * @param array $_biz_content
     * @return array
     */
    private function bizReferUrl($_refer_url, array $_biz_content): array
    {
        if ($_refer_url !== null) {
            $_biz_content['refer'] = $_refer_url;
        }
        return $_biz_content;
    }

}


