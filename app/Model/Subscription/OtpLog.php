<?php

namespace App\Model\Subscription;

use Illuminate\Database\Eloquent\Model;

class OtpLog extends Model
{
    protected $table='subscription_otps';
    protected $guarded=['id'];
}
