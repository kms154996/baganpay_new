<?php

namespace App\Model\Subscription\Telenor;

use Illuminate\Database\Eloquent\Model;

class TelenorMtkLog extends Model
{
    protected $guarded=['id'];
}
