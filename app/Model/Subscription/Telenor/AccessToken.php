<?php

namespace App\Model\Subscription\Telenor;

use Illuminate\Database\Eloquent\Model;

class AccessToken extends Model
{
    protected $guarded=['id'];
}
