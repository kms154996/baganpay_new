<?php

namespace App\Model\Wave2;

use Illuminate\Database\Eloquent\Model;

class Wave2Request extends Model
{
    protected $table='wave2_requests';
    protected $guarded=['id'];
}
