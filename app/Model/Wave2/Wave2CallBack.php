<?php

namespace App\Model\Wave2;

use Illuminate\Database\Eloquent\Model;

class Wave2CallBack extends Model
{
    protected $table='wave2_call_backs';
    protected $guarded=['id'];
}
