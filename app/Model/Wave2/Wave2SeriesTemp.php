<?php

namespace App\Model\Wave2;

use Illuminate\Database\Eloquent\Model;

class Wave2SeriesTemp extends Model
{
    protected $table='wave2_series_temps';
    protected $guarded=[
        'id'
    ];
}
