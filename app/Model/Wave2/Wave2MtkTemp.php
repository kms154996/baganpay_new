<?php

namespace App\Model\Wave2;

use Illuminate\Database\Eloquent\Model;

class Wave2MtkTemp extends Model
{
    protected $table='wave2_mtk_temps';
    protected $guarded=[
        'id'
    ];
}
