<?php

namespace App\Model\Aya;

use Illuminate\Database\Eloquent\Model;

class SeriesRedirect extends Model
{
    protected $table='aya_series_redirects';
    protected $guarded=[
        'id'
    ];
}
