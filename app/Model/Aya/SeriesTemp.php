<?php

namespace App\Model\Aya;

use Illuminate\Database\Eloquent\Model;

class SeriesTemp extends Model
{
    protected $table = 'aya_series_temps';
    protected $guarded = [
        'id'
    ];
}
