<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AppCallBackLog extends Model
{
    protected $table='app_call_back_logs';
    protected $guarded=[
        'id'
    ];
}
