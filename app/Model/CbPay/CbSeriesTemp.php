<?php

namespace App\Model\CbPay;

use Illuminate\Database\Eloquent\Model;

class CbSeriesTemp extends Model
{
    protected $table='cb_series_temp';
    protected $guarded=[
        'id'
    ];
}
