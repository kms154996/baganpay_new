<?php

namespace App\Model\Onepay;

use Illuminate\Database\Eloquent\Model;

class SeriesRedirect extends Model
{
    protected $table='onepay_series_redirect';
    protected $guarded=[
        'id'
    ];
}
