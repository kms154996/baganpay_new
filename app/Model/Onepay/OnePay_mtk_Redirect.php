<?php

namespace App\Model\Onepay;

use Illuminate\Database\Eloquent\Model;

class OnePay_mtk_Redirect extends Model
{
    protected $table = 'onepay_mtk_redirect';
    protected $guarded=[
        'id'
    ];
}
