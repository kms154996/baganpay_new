<?php

namespace App\Model\Onepay;

use Illuminate\Database\Eloquent\Model;

class WunzinnRedirect extends Model
{
    protected $table='onepay_wunzinn_redirect';
    protected $guarded=[
        'id'
    ];
}
