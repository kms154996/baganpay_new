<?php

namespace App\Model\Onepay;

use Illuminate\Database\Eloquent\Model;

class OnePayRequest extends Model
{
    protected $table = 'onepay_requests';
    protected $guarded=[
        'id'
    ];

}
