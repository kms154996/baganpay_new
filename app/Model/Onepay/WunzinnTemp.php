<?php

namespace App\Model\Onepay;

use Illuminate\Database\Eloquent\Model;

class WunzinnTemp extends Model
{
    protected $table='onepay_wunzinn_temps';
    protected $guarded=[
        'id'
    ];
}
