<?php

namespace App\Model\Onepay;

use Illuminate\Database\Eloquent\Model;

class SeriesTemp extends Model
{
    protected $table='onepay_series__temps';
    protected $guarded=[
        'id'
    ];
}
