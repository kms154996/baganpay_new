<?php

namespace App\Model\Onepay;

use Illuminate\Database\Eloquent\Model;

class OnePay_mtk_Temp extends Model
{
    protected $table = 'onepay_mtk__temps';
    protected $guarded=[
        'id'
    ];

}
