<?php

namespace App\Model\Kbz;

use Illuminate\Database\Eloquent\Model;

class SeriesTemp extends Model
{
    protected $table='kbz_series_temps';
    protected $guarded=['id'];
}
