<?php

namespace App\Model\Kbz;

use Illuminate\Database\Eloquent\Model;

class Olp extends Model
{
    protected $table='kbz_olp_temps';
    protected $guarded=[
        'id'
    ];
}
