<?php

namespace App\Model\Kbz\H5;

use Illuminate\Database\Eloquent\Model;

class OpenIdMtk extends Model
{
    protected $table = 'open_ids_mtk';
    protected $guarded = ['id'];
}
