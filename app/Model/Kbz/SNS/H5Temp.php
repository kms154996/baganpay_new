<?php

namespace App\Model\Kbz\SNS;

use Illuminate\Database\Eloquent\Model;

class H5Temp extends Model
{
    protected $table='kbz_sns_h5_temp';

    protected $guarded=['id'];
}
