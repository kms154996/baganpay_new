<?php

namespace App\Model\Kbz;

use Illuminate\Database\Eloquent\Model;

class CallBack extends Model
{
    protected $table = 'kbz_call_backs';
    protected $guarded = [
        'id'
    ];
}
