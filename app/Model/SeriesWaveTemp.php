<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SeriesWaveTemp extends Model
{
    protected $table = 'series_wave_temps';
    protected $guarded = ['id'];
}
