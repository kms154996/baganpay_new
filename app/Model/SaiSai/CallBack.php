<?php

namespace App\Model\SaiSai;

use Illuminate\Database\Eloquent\Model;

class CallBack extends Model
{
    protected $table = 'saisai_call_backs';
    protected $guarded = [
        'id'
    ];
}
