<?php

namespace App\Model\SaiSai;

use Illuminate\Database\Eloquent\Model;

class MtkH5Temp extends Model
{
    protected $table = 'saisai_mtk_h5_temps';
    protected $guarded = [
        'id'
    ];
}
