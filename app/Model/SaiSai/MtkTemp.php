<?php

namespace App\Model\SaiSai;

use Illuminate\Database\Eloquent\Model;

class MtkTemp extends Model
{
    protected $table='saisai_mtk_temps';
    protected $guarded=[
        'id'
    ];
}
