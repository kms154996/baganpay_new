<?php

namespace App\Model\SaiSai;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    protected $table='saisai_requests';
    protected $guarded=[
        'id'
    ];
}
