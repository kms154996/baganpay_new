<?php

namespace App\Model\SaiSai;

use Illuminate\Database\Eloquent\Model;

class SeriesTemp extends Model
{
    protected $table='saisai_series_temps';
    protected $guarded=[
        'id'
    ];
}
