<?php

namespace App\Model\RequestLog;

use Illuminate\Database\Eloquent\Model;

class OnepayRequestLog extends Model
{
    protected $guarded=['id'];
}
