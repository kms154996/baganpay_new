<?php

namespace App\Model\RequestLog;

use Illuminate\Database\Eloquent\Model;

class SaiSaipayRequestLog extends Model
{
    protected $guarded = ['id'];
}
