<?php

namespace App\Model\RequestLog;

use Illuminate\Database\Eloquent\Model;

class KbzRequestLog extends Model
{
    protected $guarded=['id'];
}
