<?php

namespace App\Model\CreditCode;

use Illuminate\Database\Eloquent\Model;

class CreditCodeLog extends Model
{
    protected $guarded=['id'];
}
