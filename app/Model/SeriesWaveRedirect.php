<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SeriesWaveRedirect extends Model
{
    protected $table = 'series_wave_redirects';
    protected $guarded = [
        'id'
    ];
}
