<?php

namespace App\Model\MyTel;

use Illuminate\Database\Eloquent\Model;

class MtkTemp extends Model
{
    protected $table='my_tel_mtk_temps';
    protected $guarded=[
        'id'
    ];
}
