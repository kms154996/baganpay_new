<?php

namespace App\Model\MyTel;

use Illuminate\Database\Eloquent\Model;

class SeriesTemp extends Model
{
    protected $table='my_tel_series_temps';
    protected $guarded=[
        'id'
    ];
}
