<?php

namespace App\Model\MyTel;

use Illuminate\Database\Eloquent\Model;

class KbzTemp extends Model
{
    protected $table='my_tel_kbz_temps';

    protected $guarded=[
        'id'
    ];
}
