<?php

namespace App\Model\MyTel;

use Illuminate\Database\Eloquent\Model;

class MyTelRequest extends Model
{
    protected $table = 'my_tel_requests';
    protected $guarded = [
        'id'
    ];
}
