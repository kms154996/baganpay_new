<?php

namespace App\Model\MyTel;

use Illuminate\Database\Eloquent\Model;

class CallBack extends Model
{
    protected $table='my_tel_call_backs';
    protected $guarded=[
        'id'
    ];
}
