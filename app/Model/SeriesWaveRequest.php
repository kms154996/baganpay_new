<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SeriesWaveRequest extends Model
{
    protected $table = 'series_wave_requests';
    protected $guarded = ['id'];
}
