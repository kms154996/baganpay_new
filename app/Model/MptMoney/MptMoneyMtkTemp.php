<?php

namespace App\Model\MptMoney;

use Illuminate\Database\Eloquent\Model;

class MptMoneyMtkTemp extends Model
{
    protected $guarded = [
        'id'
    ];
}
