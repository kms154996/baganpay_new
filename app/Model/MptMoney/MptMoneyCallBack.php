<?php

namespace App\Model\MptMoney;

use Illuminate\Database\Eloquent\Model;

class MptMoneyCallBack extends Model
{
    protected $guarded=[
        'id'
    ];
}
