<?php


namespace App;


class TransID
{
    private $_app;

    /* Generate Trans ID */
    public function generatedID($_app)
    {

        $trans_id = '';

        $this->_app = $_app;

        switch ($this->_app) {
            case env('APP_MTK'):
                $trans_id .= 'MTK_';
                break;
            case env('APP_WUNZINN'):
                $trans_id .= 'WUZ_';
                break;
            case env('APP_SERIES'):
                $trans_id .= 'SE_';
            default:
                $trans_id .= '';
        }

        $trans_id .= 'BP_';

        $trans_id .= round(microtime(true) * 1000);

        return $trans_id;
    }
}
