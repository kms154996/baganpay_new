!function (t) {
    var e = {};

    function o(n) {
        if (e[n]) return e[n].exports;
        var a = e[n] = {i: n, l: !1, exports: {}};
        return t[n].call(a.exports, a, a.exports, o), a.l = !0, a.exports
    }

    o.m = t, o.c = e, o.d = function (n, a, t) {
        o.o(n, a) || Object.defineProperty(n, a, {enumerable: !0, get: t})
    }, o.r = function (n) {
        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(n, Symbol.toStringTag, {value: "Module"}), Object.defineProperty(n, "__esModule", {value: !0})
    }, o.t = function (a, n) {
        if (1 & n && (a = o(a)), 8 & n) return a;
        if (4 & n && "object" == typeof a && a && a.__esModule) return a;
        var t = Object.create(null);
        if (o.r(t), Object.defineProperty(t, "default", {
            enumerable: !0,
            value: a
        }), 2 & n && "string" != typeof a) for (var e in a) o.d(t, e, function (n) {
            return a[n]
        }.bind(null, e));
        return t
    }, o.n = function (n) {
        var a = n && n.__esModule ? function () {
            return n.default
        } : function () {
            return n
        };
        return o.d(a, "a", a), a
    }, o.o = function (n, a) {
        return Object.prototype.hasOwnProperty.call(n, a)
    }, o.p = "", o(o.s = 0)
}([function (n, a, t) {
    "use strict";
    t.r(a);
    var e = {};
    t.r(e), t.d(e, "version", function () {
        return r
    }), t.d(e, "platform", function () {
        return c
    }), t.d(e, "language", function () {
        return u
    }), t.d(e, "supportUnicode", function () {
        return s
    });
    var o = {};
    t.r(o), t.d(o, "getAppPlatform", function () {
        return l
    }), t.d(o, "getAppVersion", function () {
        return b
    }), t.d(o, "getAppLanguage", function () {
        return f
    }), t.d(o, "getAppSupportUnicode", function () {
        return y
    }), t.d(o, "close", function () {
        return k
    }), t.d(o, "openBrowser", function () {
        return g
    }), t.d(o, "hideToolBar", function () {
        return d
    }), t.d(o, "showToolBar", function () {
        return z
    }), t.d(o, "gotoFunction", function () {
        return w
    });
    var i = {};
    t.r(i), t.d(i, "startPay", function () {
        return h
    }), t.d(i, "startPayCallback", function () {
        return m
    }), t.d(i, "startPayCallbackError", function () {
        return v
    });
    var p = {};
    t.r(p), t.d(p, "requestPin", function () {
        return P
    }), t.d(p, "getContactPhoneNumber", function () {
        return _
    }), t.d(p, "back", function () {
        return N
    }), t.d(p, "shareSocialNetwork", function () {
        return O
    }), t.d(p, "shareSocialNetworkCallback", function () {
        return S
    });
    var r = void 0, c = void 0, u = void 0, s = void 0;

    function l() {
        return this.platform ? this.platform : kbzpayapp.getPlatform instanceof Function ? (this.platform = kbzpayapp.getPlatform(), this.platform) : void (kbzpayapp.getPlatformAsync instanceof Function && kbzpayapp.getPlatformAsync(JSON.stringify({callback: "window.kbzpay.getAppPlatformCallback"})))
    }

    function b() {
        return this.version ? this.version : kbzpayapp.getVersion instanceof Function ? (this.version = kbzpayapp.getVersion, this.version) : void (kbzpayapp.getVersionAsync instanceof Function && kbzpayapp.getVersionAsync(JSON.stringify({callback: "window.kbzpay_getAppVersionCallback"})))
    }

    function f() {
        return this.language ? this.language : kbzpayapp.getLanguage instanceof Function ? (this.language = kbzpayapp.getLanguage(), this.language) : void (kbzpayapp.getLanguageAsync instanceof Function && kbzpayapp.getVersionAsync(JSON.stringify({callback: "window.kbzpay_getAppLanguageCallback"})))
    }

    function y() {
        return this.supportUnicode ? this.supportUnicode : kbzpayapp.getUnicode instanceof Function ? (this.supportUnicode = kbzpayapp.getUnicode(), this.supportUnicode) : void (kbzpayapp.getUnicodeAsync instanceof Function && kbzpayapp.getVersionAsync(JSON.stringify({callback: "window.kbzpay_getAppSupportUnicodeCallback"})))
    }

    function k() {
        window.kbzpayapp && (kbzpayapp.evaluate instanceof Function ? kbzpayapp.evaluate(JSON.stringify({functionName: "js_fun_closeWebview"})) : window.kbzpayapp.closeWebview ? window.kbzpayapp.closeWebview() : window.kbzpayapp.close())
    }

    function g(n) {
        n ? window.kbzpayapp && (kbzpayapp.evaluate instanceof Function ? kbzpayapp.evaluate(JSON.stringify({
            functionName: "js_fun_openNewWindow",
            params: {url: n}
        })) : window.kbzpayapp.openNewWindow && window.kbzpayapp.openNewWindow(n)) : console.log("url is empty")
    }

    function d() {
        kbzpayapp.evaluate instanceof Function ? kbzpayapp.evaluate(JSON.stringify({functionName: "js_fun_hideToolBar"})) : kbzpayapp.hideToolBar instanceof Function && kbzpayapp.hideToolBar()
    }

    function z() {
        kbzpayapp.evaluate instanceof Function ? kbzpayapp.evaluate(JSON.stringify({functionName: "js_fun_showToolBar"})) : kbzpayapp.showToolBar instanceof Function && kbzpayapp.showToolBar()
    }

    function w(n, a) {
        if (n || console.log("gotoFunction command is empty"), kbzpayapp.evaluate instanceof Function) {
            var t = {};
            if (!a) try {
                t = JSON.parse(a)
            } catch (n) {
                return void console.log("jsonParams is not json")
            }
            kbzpayapp.evaluate(JSON.stringify({
                functionName: "js_fun_gotoFunction",
                params: {command: n, commandParams: t}
            }))
        } else kbzpayapp.showToolBar instanceof Function && kbzpayapp.gotoFunction(n, a)
    }

    function h(n, a, t, e, o) {
        e instanceof Function ? this.startPay_callback = e : this.startPay_callback = void 0, o instanceof Function ? this.startPay_callback_error = o : this.startPay_callback_error = void 0, n && a && t ? kbzpayapp ? kbzpayapp.evaluate instanceof Function ? kbzpayapp.evaluate(JSON.stringify({
            functionName: "js_fun_startPay",
            params: {prepayId: n, orderInfo: a, sign: t}
        })) : kbzpayapp.startPay instanceof Function && kbzpayapp.startPay(n, a, t) : this.startPay_callback_error && this.startPay_callback_error(10, "not found kbzpay environment") : console.log("parameters have empty value")
    }

    function m(n) {
        if (this.startPay_callback instanceof Function) {
            n.startsWith("(") && 2 <= n.length && (n = n.substr(1, n.length - 2));
            var a = JSON.parse(n);
            this.startPay_callback(a)
        }
    }

    function v(n, a) {
        this.startPay_callback_error instanceof Function && this.startPay_callback_error({code: n, msg: a})
    }

    function P(n) {
        kbzpayapp.evaluate instanceof Function ? (this.requestPinCallback = n, kbzpayapp.evaluate(JSON.stringify({
            functionName: "js_fun_requestPin",
            params: {callback: "window.kbzpay.requestPinCallback"}
        }))) : kbzpayapp.requestPin instanceof Function ? (this.requestPinCallback = n, kbzpayapp.requestPin(JSON.stringify({callback: "window.kbzpay.requestPinCallback"}))) : console.log("kbzpayapp.requestPin is empty")
    }

    function _(n) {
        this.version ? parseFloat(this.version) <= 2.4 ? console.log("current version is not support") : kbzpayapp.evaluate instanceof Function ? (this.getContactPhoneNumberCallback = n, kbzpayapp.evaluate(JSON.stringify({
            functionName: "js_fun_getContactPhoneNumber",
            params: {callback: "window.kbzpay.getContactPhoneNumberCallback"}
        }))) : kbzpayapp.requestPin instanceof Function ? (this.getContactPhoneNumberCallback = n, kbzpayapp.getContactPhoneNumber(JSON.stringify({callback: "window.kbzpay.getContactPhoneNumberCallback"}))) : console.log("kbzpayapp.getContactPhoneNumber is empty") : console.log("current version is not support")
    }

    function N() {
        kbzpayapp.evaluate instanceof Function ? kbzpayapp.evaluate(JSON.stringify({functionName: "js_fun_back"})) : kbzpayapp.back instanceof Function && kbzpayapp.back()
    }

    function O(n, a, t) {
        n && "" !== n.trim() ? (t instanceof Function && (this.shareSocialNetworkCallback = t), kbzpayapp.evaluate instanceof Function ? kbzpayapp.evaluate(JSON.stringify({
            functionName: "js_fun_shareSocialNetwork",
            params: {shareUrl: n, appId: a}
        })) : kbzpayapp.shareSocialNetwork instanceof Function && (a ? kbzpayapp.shareSocialNetwork(n, a) : kbzpayapp.shareSocialNetwork(n))) : console.log("function shareSocialNetwork shareUrl is empty")
    }

    function S(n) {
    }

    function F(a, n) {
        var t = Object.keys(a);
        if (Object.getOwnPropertySymbols) {
            var e = Object.getOwnPropertySymbols(a);
            n && (e = e.filter(function (n) {
                return Object.getOwnPropertyDescriptor(a, n).enumerable
            })), t.push.apply(t, e)
        }
        return t
    }

    function A(n, a, t) {
        return a in n ? Object.defineProperty(n, a, {
            value: t,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : n[a] = t, n
    }

    window.kbzpay = function (a) {
        for (var n = 1; n < arguments.length; n++) {
            var t = null != arguments[n] ? arguments[n] : {};
            n % 2 ? F(Object(t), !0).forEach(function (n) {
                A(a, n, t[n])
            }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(a, Object.getOwnPropertyDescriptors(t)) : F(Object(t)).forEach(function (n) {
                Object.defineProperty(a, n, Object.getOwnPropertyDescriptor(t, n))
            })
        }
        return a
    }({}, e, {}, new function () {
        setTimeout(function () {
            kbzpayapp && kbzpayapp.getPlatformAsync instanceof Function && (window.kbzpay_getAppPlatformCallback = function (n) {
                window.kbzpay.platform = n
            }, kbzpayapp.getPlatformAsync(JSON.stringify({callback: "window.kbzpay_getAppPlatformCallback"}))), kbzpayapp && kbzpayapp.getVersionAsync instanceof Function && (window.kbzpay_getAppVersionCallback = function (n) {
                window.kbzpay.version = n
            }, kbzpayapp.getVersionAsync(JSON.stringify({callback: "window.kbzpay_getAppVersionCallback"}))), kbzpayapp && kbzpayapp.getLanguageAsync instanceof Function && (window.kbzpay_getAppLanguageCallback = function (n) {
                window.kbzpay.language = n
            }, kbzpayapp.getLanguageAsync(JSON.stringify({callback: "window.kbzpay_getAppLanguageCallback"}))), kbzpayapp && kbzpayapp.getUnicodeAsync instanceof Function && (window.kbzpay_getAppSupportUnicodeCallback = function (n) {
                window.kbzpay.supportUnicode = n
            }, kbzpayapp.getUnicodeAsync(JSON.stringify({callback: "window.kbzpay_getAppSupportUnicodeCallback"})))
        }, 100)
    }, {}, o, {}, i, {}, p)
}]);
